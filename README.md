# Syllable Embeddings Code + Data

This document contains some pointers as to how to use this code and data to reproduce the results described in the
paper "Compressing Word Embeddings Using Syllables".

Due to their collective size, we don't distribute the models that were used to obtain the results in the paper
in this package. Feel free to contact us should you wish to obtain them.

Questions/remarks can be addressed to Laurent Mertens:  
laurent.mertens@kuleuven.be  
https://github.com/LaurentMertens

## Getting Dutch syllabic decompositions
The "data/SyllabicDecompositions" folder contains a "nl\_syllables.txt" file that contains the Dutch syllabic
decompositions in raw text format, and a "nlwiktionary\_with\_syllables.dill" file, which is the file that has
been used for the paper, and that can be used with "syllables.nlwiktionary_syllables.NLWiktionarySyllables" and
"syllables.nlwiktionary_syllables2.NLWiktionarySyllables2". It contains the same info as the raw text file, but
with some extra tidbits, in a Python dill container. (See code for details.)

In case you want to start from scratch and recreate the dill file yourself, start by downloading a dump of the Dutch
Wiktionary from https://dumps.wikimedia.org/backup-index.html. For the paper, we used the November 1st, 2019 dump.
Then, update the "WIKTIONARY_NL_DUMP" variable in config.py, and run either NLWiktionarySyllables or
NLWiktionarySyllables2 (the data processing is the same for both). This will parse the Wiktionary dump, extract
the syllabic decompositions and create a new "nlwiktionary\_with\_syllables.dill" file.

## Getting English syllabic decompositions
The "data/SyllabicDecompositions" folder contains a "en_syllables.txt" file which contains the syllabic decompositions
used for the paper, as scraped by using "scrape.scrape\_both.ScrapeBoth". Either use this pre-existing file, or run the
scraper again to create a new file. Note that this scraper expect the FastText embeddings "wiki-news-300d-1M.vec" to
be present, as it uses the tokens from FastText as input to the scraped websites. These can be downloaded from:
https://fasttext.cc/docs/en/english-vectors.html.
The location of your local copy should be reflected by the "FASTTEXT\_VECS" variable in config.py.

## Getting the evaluation datasets
The paper uses the original English WordSim353 and SemEval2017 datasets, as well as Dutch translations made and/or
supervised by the authors.
The original WordSim353 data can be found at: http://www.cs.technion.ac.il/~gabr/resources/data/wordsim353/ (although
you might have to use WayBack Machine: https://web.archive.org/)
The original SemEval2017 data can be found at: https://alt.qcri.org/semeval2017/task2/

Our translations are contained in the "data/DutchEvaluationSets" folder.

## Train syllable splitter model
Use "models.nn.transformer.toy_trainer.py". As the name suggests, this was originally intended to only be a temporary
"toy" script, but eventually ended up being the one used for training the final models.

## Train syllable embeddings
### Vanilla model
Use "models.syllables.train_en_syllable_embeddings.py" for English and
"models.syllables.train_en_syllable_embeddings.py" for Dutch.

### "Attention 1" and "Attention 2" models
Use "models.nn.transformer.trans\_syll\_encoder\_2.py" for training what are know as "Attention 1" models in the paper,
and "models.nn.transformer.trans\_syll\_encoder\_4.py" for training "Attention 2" models.

Parameters (e.g., language and ratio of start/end tokens) should be set in the "main" clause at the bottom of each
script.

## Evaluate embeddings
Evaluation tools are contain in the "eval" package. In particular, "eval/semeval2017.py" and "eval/wordsim353.py" are
used to load the respective evaluation sets. "eval/eval\_wordsim\_generic.py" can be used to evaluate embeddings against
multiple reference datasets.
