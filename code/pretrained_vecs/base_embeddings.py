from abc import ABC, abstractmethod
import numpy as np


class BaseEmbeddings(ABC):
    @abstractmethod
    def get_words(self) -> set:
        pass

    @abstractmethod
    def get_vec_for_word(self, word) -> np.ndarray:
        pass
