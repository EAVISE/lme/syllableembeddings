"""
Extract wordvecs from SemEval pertaining to a particular language.
"""
import gzip
import os
from collections import Counter

from config import Config

file_in = Config.CONCEPTNET_VECS
# The following dictionary contains k, v pairs that encode the following:
#   k: target language
#   v: output file
files_out = {
    'nl': os.path.join(Config.PRETRAINED_VECS_DIR, "numberbatch_nl.txt.gz"),
    'en': os.path.join(Config.PRETRAINED_VECS_DIR, "numberbatch_en.txt.gz")
}

per_lang = Counter()
at_line = 0

# Delete output files if they already exist --> start fresh
for k, v in files_out.items():
    if os.path.isfile(v):
        os.remove(v)

with gzip.open(file_in, 'rb') as fin:
    for line in fin:
        at_line += 1
        if at_line == 1:
            print(line)
            continue
        elif at_line % 10000 == 0:
            print(f"\rAt line {at_line}...", end='')
        line_str = line.decode("utf-8")
        lan = line_str.split(" ")[0].split("/")[2]
        per_lang[lan] += 1
        # print(line)
        # Not the fastest way, but this is code that should only be ran once every so often, so not that critical
        if lan in files_out:
            with gzip.open(files_out[lan], 'ab') as fout:
                fout.write(line)
print(f"\rAt line {at_line}...")

for k in sorted(per_lang.keys()):
    print(f"{k:10s} - {per_lang[k]}")
