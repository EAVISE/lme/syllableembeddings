"""
Load pretrained vectors referenced in the Tulkens 2016 paper "Evaluating Unsupervised Dutch Word Embeddings as
 a Linguistic Resource".
"""
import gzip
import os
from enum import Enum

import numpy as np

from eval.semeval2017 import SemEval2017
from eval.wordsim353 import WordSim353
from pretrained_vecs.base_embeddings import BaseEmbeddings
from config import Config, Lang


class Embeddings(Enum):
    COW = os.path.join("cow-320", "320", "cow-320.txt.gz")
    SONAR500 = os.path.join("sonar-320", "320", "sonar-320.txt.gz")
    WIKIPEDIA = os.path.join("wikipedia-320", "320", "wikipedia-320.txt.gz")


class Tulkens2016(BaseEmbeddings):
    def __init__(self, embeddings: Embeddings, nb_to_load=-1):
        with gzip.open(os.path.join(Config.TULKENS2016_DIR, embeddings.value), 'rb') as fin:
            print(f"Loading Tulkens2016 embeddings {embeddings.name}...", end='', flush=True)
            self.n, self.d = map(int, fin.readline().decode("utf-8").split())
            data = {}

            nb_loaded = 0
            for line in fin:
                line = line.decode("utf-8")
                tokens = line.rstrip().split(' ')
                data[tokens[0]] = np.asarray([x for x in map(float, tokens[1:])])
                nb_loaded += 1
                if nb_loaded == nb_to_load:
                    break
            print(f" done! Loaded {nb_loaded} embeddings.")

        self.data = data

    def get_vec_for_word(self, word):
        if not word in self.data:
            return None

        else:
            return self.data[word]

    def get_words(self):
        return sorted(self.data.keys())


if __name__ == '__main__':
    # ws = WordSim353()
    # s_eval = SemEval2017(lang=Lang.NL)
    # for i in [1000, 10000, 25000, 50000, 100000, 250000]:
    #     embed = Tulkens2016(embeddings=Embeddings.COW, nb_to_load=i)
    #     print("------------------------------------------------------------")
    #     print(f"{i:6d}: {ws.compute_score(embed.data, b_ignore_case=True)}")
    #     print(f"{i:6d}: {s_eval.compute_score(embed.data, b_ignore_case=True)}")

    semeval = SemEval2017(lang=Lang.NL)
    embed = Tulkens2016(embeddings=Embeddings.WIKIPEDIA)
    res = semeval.compute_score(embed.data, b_ignore_case=True, b_print_not_found=False, b_replace_whitespace=False)
    print(res[0])
    print(res[1])

