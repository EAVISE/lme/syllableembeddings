import gzip
import os
from collections import Counter

import numpy as np

from pretrained_vecs.base_embeddings import BaseEmbeddings
from config import Config, Lang
from tools.tools import Tools


class ConceptNetVecs(BaseEmbeddings):
    def __init__(self, lang=Lang, nb_to_load=-1):
        print(f"Loading ConceptNet vectors for language '{lang.value}'...", end='', flush=True)
        embed_size = 0
        nb_loaded = 0
        with gzip.open(os.path.join(Config.PRETRAINED_VECS_DIR, f"numberbatch_{lang.value}.txt.gz"), 'rb') as fin:
            data = {}

            for line in fin:
                line = line.decode('utf-8')
                tokens = line.rstrip().split(' ')
                if not embed_size:
                    embed_size = len(tokens) - 1
                word = tokens[0].split('/')[-1]
                # print(word)
                data[word] = np.asarray([x for x in map(float, tokens[1:])])
                nb_loaded += 1
                if nb_loaded == nb_to_load:
                    break

        self.data = data
        self.n = nb_loaded
        self.embed_size = embed_size
        print(f" done! Loaded {nb_loaded} vectors; embedding size={self.embed_size}.")

    def get_words(self) -> set:
        return set(self.data.keys())

    def get_vec_for_word(self, word):
        if word not in self.data:
            return None

        else:
            return self.data[word]

    def get_most_common_for_word(self, word: str, n=10):
        if word not in self.data:
            return None

        syll_vec = self.data[word]

        cosim = Counter()
        for k, v in self.data.items():
            if k == word:
                continue
            cosim[k] = Tools.cosim(v, syll_vec)

        return cosim.most_common(n)

    def get_most_common_for_vec(self, vec: np.ndarray, n=10):
        cosim = Counter()
        for k, v in self.data.items():
            cosim[k] = Tools.cosim(v, vec)

        return cosim.most_common(n)


if __name__ == '__main__':
    cnnl = ConceptNetVecs(lang=Lang.NL, nb_to_load=-1)
    # wikt = NLWiktionarySyllables()
    # # sv = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "syll_vecs_best_147epochs.dill"))
    # sv = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "torch_syll_vecs_final.dill"))
    #
    # # Interessant: au to stra de
    # # dok ter
    # # pi loot
    # # kran ten kop
    #
    # print("Type 'EXIT' to stop.")
    # print("Enter a list of syllables, whitespace separated: ", end='')
    # tok = input()
    # while tok != "EXIT":
    #     vec = sv.get_sum_vec_for_sylls(tok.split())
    #     if vec is None:
    #         print("At least one of the syllables is unknown...")
    #     else:
    #         mc = cnnl.get_most_common_for_vec(vec)
    #         word = tok.replace(' ', '')
    #         print(f"Word: [{word}] -- ", end='')
    #         if word not in cnnl.data:
    #             print("!!! Not present in ConceptNet NumberBatch !!!")
    #         else:
    #             print(Tools.cosim(cnnl.get_vec_for_word(word), vec))
    #         if word not in wikt.data:
    #             print("!!! Not present in NLWiktionary !!!")
    #
    #         for k, v in mc:
    #             print(f"{k:10s}: {v}")
    #
    #     print("============================================================")
    #     print("Enter a list of syllables, whitespace separated: ", end='')
    #     tok = input()
    #
    # overlap = cnnl.get_words().intersection(wikt.get_words())
