"""Load embedding vectors for syllables, as extracted/trained from ConceptNet NumberBatch."""
import numpy as np
import os
from collections import Counter

import dill

from config import Config
from tools.tools import Tools


class SyllableVecs():
    def __init__(self, in_file: str, embed_size=300):
        """

        :param in_file: file to load containing the embeddings --> should be a dilled dictionary
        :param embed_size: size of the embeddings
        """
        self.syll_vecs = dill.load(open(in_file, 'rb'))  # k = syllable, v = vec
        self.sylls = sorted(self.syll_vecs.keys())  # Used for binary sort
        self.embed_size = embed_size
        print(f"Loaded SyllableVecs for {len(self.sylls)}@D={self.embed_size} syllables.")
        print(f"\tModel parameters: {len(self.sylls) * self.embed_size}")

    def get_vec_for_syllable(self, syll: str):
        if syll in self.syll_vecs:
            return self.syll_vecs[syll]
        else:
            return None

    def get_most_common_for_syllable(self, syll: str, n=10):
        if syll not in self.syll_vecs:
            return None

        syll_vec = self.syll_vecs[syll]

        cosim = Counter()
        for k, v in self.syll_vecs.items():
            if k == syll:
                continue
            cosim[k] = Tools.cosim(v, syll_vec)

        return cosim.most_common(n)

    def get_sum_vec_for_sylls(self, sylls: list, b_norm=True):
        """

        :param sylls: a list of lists, where each sublist contains a list of syllables, and is supposed to be
        representing one word
        :param b_norm: normalize sum vector?
        :return:
        """
        word_vec = np.zeros((self.embed_size,))
        for word in sylls:
            sum_vec = np.zeros((self.embed_size,))
            for syll in word:
                # If we don't have an embedding for at least one syllable, we can't compute the sum vector...
                if syll not in self.syll_vecs:
                    return None
                sum_vec += self.syll_vecs[syll]
            if b_norm:
                sum_vec /= np.linalg.norm(sum_vec)
            word_vec += sum_vec
        if b_norm and len(sylls) > 1:
            word_vec /= np.linalg.norm(word_vec)
        return word_vec


if __name__ == '__main__':
    nl_sv = SyllableVecs(in_file=os.path.join(Config.PRETRAINED_VECS_DIR, "torch_norm_weights_en_syll_vecs_final.dill"))

    print("Enter a syllable: ", end='')
    tok = input()
    while tok != "EXIT":
        mc = nl_sv.get_most_common_for_syllable(tok)
        if mc is None:
            print("Syllable unknown.")
        else:
            for k, v in nl_sv.get_most_common_for_syllable(tok):
                print(f"{k:10s}: {v}")

        print("============================================================")
        print("Enter a syllable: ", end='')
        tok = input()
