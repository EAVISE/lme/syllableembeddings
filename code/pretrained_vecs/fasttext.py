import numpy as np

from eval.wordsim353 import WordSim353
from pretrained_vecs.base_embeddings import BaseEmbeddings
from config import Config


class FastTextVecs(BaseEmbeddings):
    def __init__(self, fname=Config.FASTTEXT_VECS, nb_to_load=-1):
        # Load vectors, adapted from https://fasttext.cc/docs/en/english-vectors.html
        with open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore') as fin:
            self.n, self.d = map(int, fin.readline().split())
            data = {}

            nb_loaded = 0
            for line in fin:
                tokens = line.rstrip().split(' ')
                data[tokens[0]] = np.asarray([x for x in map(float, tokens[1:])])
                nb_loaded += 1
                if nb_loaded == nb_to_load:
                    break

        self.data = data

    def get_vec_for_word(self, word):
        if not word in self.data:
            return None

        else:
            return self.data[word]

    def get_words(self):
        return sorted(self.data.keys())


if __name__ == '__main__':
    eval = WordSim353()
    for i in [1000, 10000, 25000, 50000, 100000]:
        ftv = FastTextVecs(nb_to_load=i)
        print(f"{i:6d}: {eval.compute_score(ftv.data)}")
