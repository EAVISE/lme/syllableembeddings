"""
Load syllabic decompositions scraped form http://www.syllablecount.com/
"""
import math
from collections import Counter
from copy import deepcopy

from sklearn.preprocessing import normalize
from scipy.sparse import csr_matrix
import numpy as np
import dill
import os

from wikidump_reader.wikidump_reader import WikiDumpReader

# from pretrained_vecs.conceptnet_nl_vecs import ConceptNetNLVecs
from models.syllables.split_model_tf_loader import SplitModelTFLoader
from config import Config
from tools.tools import Tools


class ENSyllables:
    def __init__(self, data_file: str=None, split_model: str=None, b_start=False, b_end=False):
        """
        If initialized with b_start/b_end set to True, will hardcode starting and ending variants of appropriate
        syllables into memory.
        Alternatively, if set to False, one can call get_syllabic_decomposition_for_word with the same tags,
        and get starting/ending tokens generated on the fly.

        :param data_file:
        :param split_model:
        :param b_start:
        :param b_end:
        """
        if data_file is None:
            data_file = os.path.join(Config.DATA_DIR, 'en_syllables_BAK.txt')

        self.b_start, self.b_end = b_start, b_end

        print("Loading EN syllables...", end='', flush=True)
        data = {}
        uq_sylls = set()
        with open(data_file, 'r') as fin:
            for line in fin:
                parts = line.strip().split('\t')
                sylls = parts[1].split('-')
                if b_start:
                    sylls[0] = '#' + sylls[0]
                if b_end:
                    sylls[-1] = sylls[-1] + '$'
                data[parts[0]] = [sylls]
                uq_sylls.update(sylls)
        print(" done!")

        print(f"Loaded with syllabes: {len(data)}")
        self.data = data
        self.uq_sylls = sorted(uq_sylls)
        self.nb_uq_sylls = len(self.uq_sylls)
        self.vec_start, self.mtx_trans = self.compute_transition_probs(b_only_single_words=True)
        print(f"\tUnique syllables: {len(uq_sylls)}")

        self.split_model = None
        if split_model is not None:
            self.split_model = SplitModelLoader(path_to_model=split_model)

    def get_words(self) -> set:
        return set(self.data.keys())

    def get_syllabic_decomposition_for_word(self, word, b_start=False, b_end=False, b_ignore_plural=False):
        """
        Check if there is a known syllabic decomposition from Wiktionary for this word. If so, return it.

        :param word:
        :param b_start: prepend '#' in front of first syllable; ignored if self.b_start = True
        :param b_end: prepend '$' in front of last syllable; ignored if self.b_end = True
        :param b_ignore_plural: if True, and word is not known, will check if word ends with 's', and if so,
         if word without last 's' is known, return the decomposition for this word
        :return:
        """
        if word in self.data:
            res = deepcopy(self.data[word])
        # If word is multi-word expression, and is unknown as such, check if individual words are known
        elif ' ' in word:
            parts = word.split()
            res = []
            for part in parts:
                if part in self.data:
                    res.append(deepcopy(self.data[part])[0])
                else:
                    # try:
                    #     res.append(self.split_model.split_word(part))
                    # except KeyError as e:
                    #     return None
                    return None
            # print(f'Wazaa! : {res}')
        else:
            if b_ignore_plural and word[-1] == 's' and word[:-1] in self.data:
                res = deepcopy(self.data[word[:-1]])
            elif self.split_model is not None:
                try:
                    res = [self.split_model.split_word(word)]
                    # print(f"{word:20s} --> {res}")
                except KeyError as e:
                    return None
            else:
                return None

        if not self.b_start and b_start:
            res[0][0] = '#' + res[0][0]
        if not self.b_end and b_end:
            res[-1][-1] = res[-1][-1] + '$'

        return res

    def get_all_syllabic_decompositions_for_word(self, word):
        """
        Find all possible syllabic decompositions using the known syllables.
        A valid decomposition is a contiguous sequence of known syllables that makes up the word.

        :param word:
        :return:
        """
        decomps = []

        def recur(prev_decomp_idx, start):
            new_decomps_idx = []
            last_idx = []
            for i in range(start+1, len(word)+1):
                # From start
                if Tools.binary_search(self.uq_sylls, word[start:i]) >= 0:
                    decomps.append((prev_decomp_idx, word[start:i]))
                    last_idx.append(i)
                    new_decomps_idx.append(len(decomps)-1)

            if new_decomps_idx:
                for i, e in enumerate(new_decomps_idx):
                    if last_idx[i] < len(word):
                        recur(e, last_idx[i])
                    else:
                        decomps.append((e, '#END'))

            return new_decomps_idx

        # Run recursive algorithm
        recur(-1, 0)

        # Extract valid decompositions
        seqs = []
        for tpl in decomps[::-1]:
            if tpl[1] == "#END":
                seq = []
                link = tpl[0]
                while link > -1:
                    seq.append(decomps[link][1])
                    link = decomps[link][0]
                seqs.append(seq[::-1])

        # for seq in seqs:
        #     print(seq)

        return seqs

    def decomp_to_idx(self, decomp: list):
        """
        Transform a decomposition (=list of syllables) into a list of indices, where each index represents the
        position of the syllable in the list of unique syllables self.uq_sylls

        :param decomp:
        :return: list of same lenght as decomp, where each entry has been replaced by its index
        """
        res = [Tools.binary_search(self.uq_sylls, word) for word in decomp]

        return res

    def compute_transition_probs(self, b_only_single_words=True):
        """
        Compute the transition probabilities between syllables.

        :param b_only_single_words: only consider single words, not multi-word expressions.
        :return: (vec_start, mtx_trans), where vec_start is a numpy array, and mtx_trans is a CSR sparse matrix
        """
        vec_start = np.zeros((len(self.uq_sylls)), dtype=np.float32)
        mtx_trans = np.zeros((len(self.uq_sylls), len(self.uq_sylls)), dtype=np.float32)
        for word, decomps in self.data.items():
            if b_only_single_words and len(decomps) > 1:
                continue
            for decomp in decomps:
                decomp_idx = self.decomp_to_idx(decomp)
                # Update start probability
                vec_start[decomp_idx[0]] += 1
                # Update transition probabilities
                for i in range(1, len(decomp_idx)):
                    # print(f"Adding one to {decomp_idx[i-1]}-->{decomp_idx[i]}")
                    mtx_trans[decomp_idx[i-1], decomp_idx[i]] += 1
        # Row-normalize vector and matrix so each row sums to 1
        vec_start = vec_start/np.sum(vec_start)
        mtx_trans = normalize(mtx_trans, axis=1, norm='l1')
        # Transform to sparse matrix
        mtx_trans = csr_matrix(mtx_trans)

        return vec_start, mtx_trans

    def get_prob_for_decomp(self, decomps: list, zero_offset=1e-6):
        """
        Get probability for given decomposition

        :param decomps: list of lists, where each nested list contains the decomposition for a word; should only
        contain more than one sublist in case of multi-word expression
        :param zero_offset: value to use if transition probability is zero according to our stored probabilities
        :return: (log of probability, probability)
        """
        log_p = 0
        for decomp in decomps:
            decomp_idx = self.decomp_to_idx(decomp)
            log_p += math.log(self.vec_start[decomp_idx[0]])
            for i in range(1, len(decomp_idx)):
                trans_prob = self.mtx_trans[decomp_idx[i-1], decomp_idx[i]]
                if trans_prob == 0:
                    trans_prob = zero_offset
                log_p += math.log(trans_prob)

        return log_p, math.exp(log_p)


if __name__ == '__main__':
    wikt = ENSyllables()

    decomps = wikt.get_all_syllabic_decompositions_for_word('aansprakelijkheid')
    print(len(decomps))

    words_wikt = set(wikt.data.keys())

    print("Enter a word to get its syllabic decomposition (if known): ", end='')
    tok = input()
    while tok != "EXIT":
        sylls = wikt.get_syllabic_decomposition_for_word(tok)
        if sylls is None:
            print("Word not present in corpus.")
        else:
            print(f"{tok}\t: {sylls}")
            log_p, p = wikt.get_prob_for_decomp(sylls)
            print(f"p: {p:.6f} --> {log_p:.6f}")

        print("============================================================")
        print("Enter a word to get its syllabic decomposition (if known): ", end='')
        tok = input()
