import math
import os
from copy import deepcopy

import dill
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.preprocessing import normalize
from wikidump_reader.wikidump_reader import WikiDumpReader

# from pretrained_vecs.conceptnet_nl_vecs import ConceptNetNLVecs
from config import Config
from tools.tools import Tools


class NLWiktionarySyllables:
    def __init__(self, b_overwrite=False, split_model=None, b_start=False, b_end=False):
        fout = os.path.join(Config.WIKIPEDIA_DIR, 'nlwiktionary_with_syllables.dill')

        if not b_overwrite and os.path.isfile(fout):
            print("NLWiktionary: Loading pre-extracted syllables...")
            self.data, uq_sylls, self.vec_start, self.mtx_trans = dill.load(open(fout, 'rb'))
        else:
            print("Loading NLWiktionary...\nExtracting syllables from Wikimedia dump...", end='', flush=True)
            data = {}
            wdr = WikiDumpReader()

            with_sylls = 0
            no_sylls = 0
            uq_sylls = set()

            ALLOWED_CHARS = {c for c in "abcdefghijklmnopqrstuvwxyzäàáçëèéêïíîñöóüú' "}  # We don't allow hyphens!
            nb_articles = 0
            filtered = []
            filtered_on_sylls = 0
            for page_title, page_text in wdr.read_page(file=Config.WIKTIONARY_NL_DUMP):
                nb_articles += 1
                # Do some minor filtering
                if sum(not c in ALLOWED_CHARS for c in page_title.lower()):
                    filtered.append(page_title)
                    continue
                sylls = self.extract_syllables(page_text)
                if sylls is not None:
                    # First, also filter syllables --> many of them are bady formatted
                    b_filter = False
                    for e in [x for y in sylls for x in y]:
                        if sum(not c in ALLOWED_CHARS for c in e.lower()):
                            # print(f"Bad boy for [{page_title}]: {e}")
                            filtered_on_sylls += 1
                            b_filter = True
                            break
                    if b_filter:
                        continue

                    # print(page_title)
                    data[page_title] = sylls
                    uq_sylls.update([x for y in sylls for x in y])
                    with_sylls += 1
                else:
                    no_sylls += 1
            print(" done!")

            # print(f"Filtered the following {len(filtered)} out of {nb_articles} articles:")
            # for a in filtered:
            #     print(f"\t{a}")

            print(f"Filtered articles   : {len(filtered)}")
            print(f"Filtered on sylls   : {filtered_on_sylls}")
            print(f"Loaded with syllabes: {len(data)}")
            print(f"No syllables        : {no_sylls}")
            print("------------------------------")
            print(f"Total               : {nb_articles}")

            print(f"Extracting transition probabilities...", end='', flush=True)
            self.data = data
            self.uq_sylls = sorted(uq_sylls)
            self.vec_start, self.mtx_trans = self.compute_transition_probs(b_only_single_words=True)
            print(" done!")

            dill.dump((data, uq_sylls, self.vec_start, self.mtx_trans), open(fout, 'wb'))

        print(f"\tUnique syllables: {len(uq_sylls)}")

        # Add special start and end tokens
        self.b_start, self.b_end = b_start, b_end
        if b_start or b_end:
            # uq_sylls = set(self.uq_sylls)
            for k, v in self.data.items():
                for e in v:
                    if b_start:
                        strt_token = '#' + e[0]
                        e[0] = strt_token
                        uq_sylls.add(strt_token)
                    if b_end:
                        end_token = e[-1] + '$'
                        e[-1] = end_token
                        uq_sylls.add(end_token)

        self.uq_sylls = sorted(uq_sylls)
        self.nb_uq_sylls = len(self.uq_sylls)
        print(f"\tUnique syllables with/start/end: {len(uq_sylls)}")

        self.split_model = None
        if split_model is not None:
            self.split_model = SplitModelLoader(path_to_model=split_model)

    @classmethod
    def extract_syllables(cls, page_text):
        """
        Extract syllabic decomposition from Wiktionary page.

        :param page_text: the (possibly cleaned) Wiki code of the Wiktionary page to parse
        :return:
        """
        b_next = False
        b_nl = False

        page_text = page_text.split('\n')
        for line in page_text:
            line = line.strip()
            # Check language is Dutch
            if not b_nl and line == '{{=nld=}}':
                b_nl = True

            if b_nl and line.startswith("{{-syll-}}"):
                b_next = True
            elif b_next:
                line = line[1:].strip()
                # print(line)
                # Check for format of type '{{syll|zelf|stan|dig naam|woord}}
                if line.startswith('{{syll'):
                    line = line[7:-2]
                    parts = line.split(' ')
                    sylls = [part.split('|') for part in parts]
                else:
                    parts = line.split(' ')
                    sylls = [part.split('·') for part in parts]
                # print(sylls)
                return sylls

        return None

    def get_words(self) -> set:
        return set(self.data.keys())

    def get_syllabic_decomposition_for_word(self, word, b_start=False, b_end=False):
        """
        Check if there is a known syllabic decomposition from Wiktionary for this word. If so, return it.

        :param word:
        :param b_start: prepend '#' in front of first syllable; global settings take precedence
        :param b_end: prepend '$' in front of last syllable; global settings take precedence
        :return:
        """
        if word in self.data:
            res = deepcopy(self.data[word])
        # If word is multi-word expression, and is unknown as such, check if individual words are known
        elif ' ' in word:
            parts = word.split()
            res = []
            for part in parts:
                if part in self.data:
                    res.append(deepcopy(self.data[part])[0])
                else:
                    # try:
                    #     res.append(self.split_model.split_word(part))
                    # except KeyError as e:
                    #     return None
                    return None
            # print(f'Wazaa! : {res}')
        else:
            if self.split_model is not None:
                try:
                    res = [self.split_model.split_word(word)]
                    # print(f"{word:20s} --> {res}")
                except KeyError as e:
                    return None
            else:
                return None

        if not self.b_start and b_start:
            res[0][0] = '#' + res[0][0]
        if not self.b_end and b_end:
            res[-1][-1] = res[-1][-1] + '$'

        return res

    def get_all_syllabic_decompositions_for_word(self, word):
        """
        Find all possible syllabic decompositions using the known syllables.
        A valid decomposition is a contiguous sequence of known syllables that makes up the word.

        :param word:
        :return:
        """
        decomps = []

        def recur(prev_decomp_idx, start):
            new_decomps_idx = []
            last_idx = []
            for i in range(start+1, len(word)+1):
                # From start
                if Tools.binary_search(self.uq_sylls, word[start:i]) >= 0:
                    decomps.append((prev_decomp_idx, word[start:i]))
                    last_idx.append(i)
                    new_decomps_idx.append(len(decomps)-1)

            if new_decomps_idx:
                for i, e in enumerate(new_decomps_idx):
                    if last_idx[i] < len(word):
                        recur(e, last_idx[i])
                    else:
                        decomps.append((e, '#END'))

            return new_decomps_idx

        # Run recursive algorithm
        recur(-1, 0)

        # Extract valid decompositions
        seqs = []
        for tpl in decomps[::-1]:
            if tpl[1] == "#END":
                seq = []
                link = tpl[0]
                while link > -1:
                    seq.append(decomps[link][1])
                    link = decomps[link][0]
                seqs.append(seq[::-1])

        # for seq in seqs:
        #     print(seq)

        return seqs

    def decomp_to_idx(self, decomp: list):
        """
        Transform a decomposition (=list of syllables) into a list of indices, where each index represents the
        position of the syllable in the list of unique syllables self.uq_sylls

        :param decomp:
        :return: list of same lenght as decomp, where each entry has been replaced by its index
        """
        res = [Tools.binary_search(self.uq_sylls, word) for word in decomp]

        return res

    def compute_transition_probs(self, b_only_single_words=True):
        """
        Compute the transition probabilities between syllables.

        :param b_only_single_words: only consider single words, not multi-word expressions.
        :return: (vec_start, mtx_trans), where vec_start is a numpy array, and mtx_trans is a CSR sparse matrix
        """
        vec_start = np.zeros((len(self.uq_sylls)), dtype=np.float32)
        mtx_trans = np.zeros((len(self.uq_sylls), len(self.uq_sylls)), dtype=np.float32)
        for word, decomps in self.data.items():
            if b_only_single_words and len(decomps) > 1:
                continue
            for decomp in decomps:
                decomp_idx = self.decomp_to_idx(decomp)
                # Update start probability
                vec_start[decomp_idx[0]] += 1
                # Update transition probabilities
                for i in range(1, len(decomp_idx)):
                    # print(f"Adding one to {decomp_idx[i-1]}-->{decomp_idx[i]}")
                    mtx_trans[decomp_idx[i-1], decomp_idx[i]] += 1
        # Row-normalize vector and matrix so each row sums to 1
        vec_start = vec_start/np.sum(vec_start)
        mtx_trans = normalize(mtx_trans, axis=1, norm='l1')
        # Transform to sparse matrix
        mtx_trans = csr_matrix(mtx_trans)

        return vec_start, mtx_trans

    def get_prob_for_decomp(self, decomps: list, zero_offset=1e-6):
        """
        Get probability for given decomposition

        :param decomps: list of lists, where each nested list contains the decomposition for a word; should only
        contain more than one sublist in case of multi-word expression
        :param zero_offset: value to use if transition probability is zero according to our stored probabilities
        :return: (log of probability, probability)
        """
        log_p = 0
        for decomp in decomps:
            decomp_idx = self.decomp_to_idx(decomp)
            log_p += math.log(self.vec_start[decomp_idx[0]])
            for i in range(1, len(decomp_idx)):
                trans_prob = self.mtx_trans[decomp_idx[i-1], decomp_idx[i]]
                if trans_prob == 0:
                    trans_prob = zero_offset
                log_p += math.log(trans_prob)

        return log_p, math.exp(log_p)


if __name__ == '__main__':
    wikt = NLWiktionarySyllables(b_overwrite=True)
    # cnnl = ConceptNetNLVecs()

    # decomps = wikt.get_all_syllabic_decompositions_for_word('aansprakelijkheid')
    # print(len(decomps))

    words_wikt = set(wikt.data.keys())
    # words_cnnl = set(cnnl.get_words())

    print(f"#Words syllables: {len(words_wikt)}")

    # print(f"Words cnnl      : {len(words_cnnl)}")
    # print(f"Intersection    : {len(words_wikt.intersection(words_cnnl))}")

    print("Enter a word to get its syllabic decomposition (if known): ", end='')
    tok = input()
    while tok != "EXIT":
        sylls = wikt.get_syllabic_decomposition_for_word(tok)
        if sylls is None:
            print("Word not present in corpus.")
        else:
            print(f"{tok}\t: {sylls}")
            log_p, p = wikt.get_prob_for_decomp(sylls)
            print(f"p: {p:.6f} --> {log_p:.6f}")

        print("============================================================")
        print("Enter a word to get its syllabic decomposition (if known): ", end='')
        tok = input()
