"""
Extract some stats from syllable dataset.
"""
import math
from collections import Counter, defaultdict

from eval.semeval2017 import SemEval2017
from eval.wordsim353 import WordSim353
from config import Config, Lang
from syllables.en_syllables2 import ENSyllables2
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2

import matplotlib
matplotlib.use('Qt5Agg')
from matplotlib import pyplot as plt


# Get known syllables from evaluation set
def get_syllables_from_eval_set(eval_set, syll_set):
    syll_eval = set()
    for word in eval_set.words:
        if word in syll_set.data:
            for l in syll_set.data[word]:
                syll_eval.update(l)
    return syll_eval


def count_sylls(syll_set: ENSyllables2 or NLWiktionarySyllables2):
    """

    :param syll_set:
    :return: syll_all, syll_start, syll_no_start, single_syll
    """
    syll_all = Counter()
    syll_start = Counter()
    syll_no_start = Counter()
    single_syll = 0
    for k, v in syll_set.data.items():
        # print(k)
        for i in range(len(v)):
            syll_all.update(v[i])
            syll_start[v[i][0]] += 1
            if len(v[i]) == 1:
                single_syll += 1
            for j in range(1, len(v[i])):
                syll_no_start[v[i][j]] += 1

    print(f"Number of word containing only one syllable: {single_syll}/{len(syll_set.data)}")
    return syll_all, syll_start, syll_no_start, single_syll


# b_print_pc_ranges = False  # Print percent ranges
# if b_print_pc_ranges:
#     more_than_10, more_than_20, more_than_50, more_than_75, more_than_90 = 0, 0, 0, 0, 0
#     start_per_ct = Counter()
#     for k, v in syll_start.most_common():
#         start_per_ct[v] += 1
#         w = syll_no_start[k]
#         pc = v/(v+w)
#         # print(f"{k}: {v} -- {w} -- {100*pc:.1f}")
#         if pc > 0.1:
#             more_than_10 += 1
#         if pc > 0.2:
#             more_than_20 += 1
#         if pc > 0.5:
#             more_than_50 += 1
#         if pc > 0.75:
#             more_than_75 += 1
#         if pc > 0.90:
#             more_than_90 += 1
#
#     print(f"Single syllable words: {single_syll} of {len(sylls.data)}")
#     print(f"More than 10pc: {more_than_10} of {len(syll_start)}")
#     print(f"More than 20pc: {more_than_20} of {len(syll_start)}")
#     print(f"More than 50pc: {more_than_50} of {len(syll_start)}")
#     print(f"More than 75pc: {more_than_75} of {len(syll_start)}")
#     print(f"More than 90pc: {more_than_90} of {len(syll_start)}")

# print("Per count:")
# for k, v in start_per_ct.most_common():
#     print(f"{k} :: {v}")


# How many syllables appear how many times?
def get_sylls_per_ct(syll_counts):
    """
    Given a counter that contains "k: v" pairs of the form "syllable: # times syllable appears in words", flip it around
    to "k: v" of the form "number of appearances: number of syllables that appear this many times".

    :param syll_counts:
    :return: all_per_ct, list_per_ct --> list of syllables per count
    """
    all_per_ct = Counter()
    list_per_ct = defaultdict(set)
    for k, v in syll_counts.items():
        all_per_ct[v] += 1
        list_per_ct[v].add(k)

    return all_per_ct, list_per_ct


# b_print_per_ct = False
# if b_print_per_ct:
#     print(f"Nb. of syllables occuring only once (in any position): {all_per_ct[1]}")
#     for e in list_per_ct[1]:
#         if e not in syll_start:
#             print(e)

# Check how many syllables that occur in the WordSim dataset occur only once
def eval_sylls_per_ct(lang, syll_eval, syll_all):
    """
    For all the unique syllables occurring in the provided syll_eval counter,

    :param lang:
    :param syll_eval:
    :param syll_all:
    :return:
    """
    print(f"Number of unique syllables in evalset for lang={lang.value}: {len(syll_eval)}")
    only_once = 0
    syll_eval_per_ct = Counter()
    for syll in sorted(syll_eval):
        if syll_all[syll] == 1:
            only_once += 1
        syll_eval_per_ct[syll_all[syll]] += 1
        # print(f"{syll_all[syll]:3d}: {syll}")
    print("-" * 60 + f"\nOnly once: {only_once} of {len(syll_eval)}")
    return syll_eval_per_ct


def bin_sylls_per_ct(sylls_per_ct, hi_thres=1000, bin_width=20, b_normalize=False):
    """
    Bin counts.

    :param sylls_per_ct: Counter containing "k: v" = "number of occurrences: number of syllables".
    :param hi_thres: bin and plot up until this limit, and print to console how many syllables appear more times than this.
    :param b_normalize: normalize bin counts by dividing by total amount of counts.
    :return: Counter(bin: counts)
    """
    # We compute the bins ourselves, as otherwise the "dense=True" option to plt.hist() doesn't do what we want it to.
    ct_per_bin = Counter()

    max_k = 0
    exc_thres = 0
    blw_thres = 0
    for k, v in sylls_per_ct.most_common():
        if k > max_k:
            max_k = k
        # print(f"{k}: {v}")
        if k > hi_thres:
            exc_thres += v
        else:
            blw_thres += v
            in_bin = (k-1) // bin_width
            ct_per_bin[in_bin] += v
    print(f"Max k: {max_k}")  # max_k = 4399
    print(f"Below {hi_thres}    : {blw_thres}")
    print(f"Exceeding {hi_thres}: {exc_thres}")
    nb_sylls = blw_thres + exc_thres
    for i in range(5):
        print(f"bin[{i}]: {ct_per_bin[i]} --> {100*ct_per_bin[i]/nb_sylls:.1f}%")

    xs = list(range(0, hi_thres+1, bin_width))
    ys = [ct_per_bin[i // 20] for i in xs]
    if b_normalize:
        ys = [i/nb_sylls for i in ys]

    return xs, ys, exc_thres


def sylls_in_set(syllabic_decomp, syll_set):
    """
    Check whether all syllables in the syllabic decomposition provided appear in the set of syllables provided.

    :param syllabic_decomp:
    :param syll_set:
    :return:
    """
    check = [0 if s in syll_set else 1 for s in syllabic_decomp]
    return 1 if sum(check) == 0 else 0


def get_syllables_per_top_x(syll_ct: Counter, step=5):
    """
    Given a counter containing "k: v" = "syllable: nb. of counts", create a dictionary
    "k: v" = "top x%: set of syllables in top x%".

    :param syll_ct:
    :param step: stepsize to use
    :return: top_x
    """
    top_x = {i: set() for i in range(step, 101, step)}
    nb_sylls = len(syll_ct)

    at_syll = 0
    for k, v in syll_ct.most_common():
        at_syll += 1
        pc_thres = step + int(((100 * at_syll / nb_sylls) // step) * step)
        if pc_thres > 100:
            pc_thres = 100
        for t in range(pc_thres, 101, step):
            top_x[t].add(k)

    # Debug
    # for k, v in top_x.items():
    #     print(f"{k}: {len(v)}")

    return top_x


def get_xs_and_ys_for_top_plot(syll_splits, top_x, step=5, b_normalize=False):
    """
    Given a ENSyllabes/NLWiktionarySyllables object (syll_splits) and a dictionary {top x%: set of syllabes in top x%},
    count how many words you could still resolve by only keeping the top x% syllables.

    :param syll_splits:
    :param top_x:
    :param step: stepsize to use
    :param b_normalize: normalize ys?
    :return: xs, ys --> can be used for creating plots.
    """
    with_top_x = Counter()
    for word, word_sylls in syll_splits.data.items():
        flat_list = [s for p in word_sylls for s in p]
        for x in range(step, 101, step):
            with_top_x[x] += sylls_in_set(flat_list, top_x[x])

    xs = []
    ys = []
    for k in sorted(with_top_x.keys()):
        # print(f"{k}: {with_top_x[k]}")
        xs.append(k)
        ys.append(with_top_x[k])

    if b_normalize:
        nb_words = len(syll_splits.data)
        ys = [e*100/nb_words for e in ys]

    return xs, ys


if __name__ == '__main__':
    f_start, f_end = 0., 0.

    sylls_en = ENSyllables2(f_start=f_start, f_end=f_end)
    sylls_nl = NLWiktionarySyllables2(f_start=f_start, f_end=f_end)

    # Reference evaluation sets
    ws_en = WordSim353(ws_file=Config.EN_WORDSIM353_FILE)
    ws_nl = WordSim353(ws_file=Config.NL_WORDSIM353_FILE)
    semeval_en = SemEval2017(lang=Lang.EN)
    semeval_nl = SemEval2017(lang=Lang.NL)

    # Plot how many words you keep when only keeping the top x% syllables
    syll_ct_en = count_sylls(sylls_en)[0]
    syll_ct_nl = count_sylls(sylls_nl)[0]

    all_per_ct_en, list_per_ct_en = get_sylls_per_ct(syll_ct_en)
    all_per_ct_nl, list_per_ct_nl = get_sylls_per_ct(syll_ct_nl)

    # Plot histogram with bincounts
    hi_thres = 1000
    xs_en, ys_en, exc_thres_en = bin_sylls_per_ct(all_per_ct_en, hi_thres=hi_thres)
    xs_nl, ys_nl, exc_thres_nl = bin_sylls_per_ct(all_per_ct_nl, hi_thres=hi_thres)
    xs_nl = [i+10 for i in xs_nl]

    plt.figure(figsize=(12, 7))
    plt.bar(x=xs_en, height=ys_en, width=10, align='edge', color='blue', label='English')
    plt.bar(x=xs_nl, height=ys_nl, width=10, align='edge', color='red', label='Dutch')
    plt.xticks(ticks=list(range(0, hi_thres+1, 20)),
               labels=[e for l in [[f'{i}', '', ''] for i in range(0, hi_thres+1, 60)] for e in l])
    plt.xlabel("Number of occurrences", fontsize=12)
    plt.ylabel("Number of syllables", fontsize=12)
    plt.yscale("log")
    plt.legend()
    plt.tight_layout()
    plt.show()

    # Top X analysis
    step = 1
    top_x_en = get_syllables_per_top_x(syll_ct_en, step=step)
    top_x_nl = get_syllables_per_top_x(syll_ct_nl, step=step)

    nb_sylls_en, nb_sylls_nl = len(syll_ct_en), len(syll_ct_nl)
    nb_words_en, nb_words_nl = len(sylls_en.data), len(sylls_nl.data)
    print("="*60)
    print(f"Number of syllables: EN: {nb_sylls_en} - NL: {nb_sylls_nl}")
    print(f"Number of words    : EN: {nb_words_en} - NL: {nb_words_en}")

    xs_en, ys_en = get_xs_and_ys_for_top_plot(sylls_en, top_x_en, step=step, b_normalize=True)
    xs_nl, ys_nl = get_xs_and_ys_for_top_plot(sylls_nl, top_x_nl, step=step, b_normalize=True)

    plt.figure(figsize=(9, 6))
    plt.plot(xs_en, ys_en, color='blue', label='English')
    plt.plot(xs_nl, ys_nl, color='red', label='Dutch')
    plt.xlabel('Most often occurring syllables (%)', fontsize=12)
    plt.ylabel('Vocabulary formed (%)', fontsize=12)
    plt.legend()
    plt.show()
