"""
Same as NLWiktionarySyllables, except that this implementation allows to store hardcoded starting and ending tags for a
given fraction of syllables, rather than "all on" or "all off".
"""
import math
from collections import Counter
from copy import deepcopy

import torch
from sklearn.preprocessing import normalize
from scipy.sparse import csr_matrix
import numpy as np
import dill
import os

from wikidump_reader.wikidump_reader import WikiDumpReader

# from pretrained_vecs.conceptnet_nl_vecs import ConceptNetNLVecs
from models.syllables.split_model_tf_loader import SplitModelTFLoader
from config import Config, Lang
from tools.tools import Tools


class NLWiktionarySyllables2:
    def __init__(self, b_overwrite=False, split_model_base: str=None, split_model_version: str=None,
                 overlap_with_vecs=None, f_start=0., f_end=0., device=torch.device('cuda')):
        """

        :param b_overwrite:
        :param split_model:
        :param overlap_with_vecs: allows to provide a dictionary of loaded embeddings; only syllables will
        be kept for words from the Wiktionary dump that are also present in the provided overlap_with_vecs
        :param f_start: float in [0, 1] that indicates the ratio of starting syllables to hardcode, e.g., is set to 0.1,
        the top 10% most often occurring starting syllables will be hardcoded
        :param f_end: analogous to f_start
        """
        fout = os.path.join(Config.WIKIPEDIA_DIR, 'nlwiktionary_with_syllables.dill')

        if not b_overwrite and os.path.isfile(fout):
            print("NLWiktionary: Loading pre-extracted syllables...")
            self.data, uq_sylls, self.vec_start, self.mtx_trans = dill.load(open(fout, 'rb'))
        else:
            print("Loading NLWiktionary...\nExtracting syllables from Wikimedia dump...", end='', flush=True)
            data = {}
            wdr = WikiDumpReader()

            with_sylls = 0
            no_sylls = 0
            uq_sylls = set()
            for page_title, page_text in wdr.read_page(file=Config.WIKTIONARY_NL_DUMP):
                # print("============================================================")
                sylls = self.extract_syllables(page_text)
                if sylls is not None:
                    data[page_title] = sylls
                    uq_sylls.update([x for y in sylls for x in y])
                    with_sylls += 1
                else:
                    no_sylls += 1
            print(" done!")

            print(f"Loaded with syllabes: {len(data)}")
            print(f"No syllables        : {no_sylls}")

            print(f"Extracting transition probabilities...", end='', flush=True)
            self.data = data
            self.vec_start, self.mtx_trans = self.compute_transition_probs(b_only_single_words=True)
            print(" done!")

            dill.dump((data, uq_sylls, self.vec_start, self.mtx_trans), open(fout, 'wb'))

        print(f"\tSyllables loaded for {len(self.data)} words.")
        print(f"\tUnique syllables loaded from Wiktionary: {len(uq_sylls)}")

        # Take overlap, if necessary
        if overlap_with_vecs is not None:
            to_remove = set(self.data.keys()).difference(set(overlap_with_vecs.keys()))
            print(f"\tTo remove: {len(to_remove)} out of {len(self.data)} words ")
            for word in to_remove:
                del self.data[word]
            uq_sylls = set()
            for k, v in self.data.items():
                for e in v:
                    uq_sylls.update(e)
            print(f"\tNumber of kept words and syllables: {len(self.data)} / {len(uq_sylls)}")

        # Add special start and end tokens
        self.f_start, self.f_end = f_start, f_end
        if f_start or f_end:
            # Get set of start and end tokens + counts for each
            syll_start = Counter([v[i][0] for k, v in self.data.items() for i in range(len(v))])
            syll_end = Counter([v[i][-1] for k, v in self.data.items() for i in range(len(v))])
            # Get number of tokens to keep
            nb_start = int(len(syll_start) * f_start)
            nb_end = int(len(syll_end) * f_end)
            # Gather tokens to be hardcoded
            set_start = {k for k, v in syll_start.most_common(nb_start)}
            set_end = {k for k, v in syll_end.most_common(nb_end)}

            # Update syllabic decompositions
            # This loop will also (re)initialize uq_sylls
            uq_sylls = set()
            for k, v in self.data.items():
                update_start = v[0][0] in set_start
                update_end = v[0][-1] in set_end
                if update_start:
                    v[0][0] = '#' + v[0][0]
                if update_end:
                    v[0][-1] = v[0][-1] + '$'
                # It is "cheaper" to reassign, than to recheck for update_start/end to be true
                self.data[k] = v
                for e in v:
                    uq_sylls.update(e)
            print(f"\tUnique syllables after +start/end: {len(uq_sylls)}")

        self.uq_sylls = sorted(uq_sylls)
        self.nb_uq_sylls = len(self.uq_sylls)

        self.split_model = None
        if split_model_base is not None:
            self.split_model = SplitModelTFLoader(base_path_to_model=os.path.join(Config.MODELS_DIR,
                                                        'Paper', split_model_base),
                                                  model_version=split_model_version,
                                                  lang=Lang.NL,
                                                  device=device)

    @classmethod
    def extract_syllables(cls, page_text):
        """
        Extract syllabic decomposition from Wiktionary page.

        :param page_text: the (possibly cleaned) Wiki code of the Wiktionary page to parse
        :return:
        """
        b_next = False
        b_nl = False

        page_text = page_text.split('\n')
        for line in page_text:
            line = line.strip()
            # Check language is Dutch
            if not b_nl and line == '{{=nld=}}':
                b_nl = True

            if b_nl and line.startswith("{{-syll-}}"):
                b_next = True
            elif b_next:
                line = line[1:].strip()
                # print(line)
                # Check for format of type '{{syll|zelf|stan|dig naam|woord}}
                if line.startswith('{{syll'):
                    line = line[7:-2]
                    parts = line.split(' ')
                    sylls = [part.split('|') for part in parts]
                else:
                    parts = line.split(' ')
                    sylls = [part.split('·') for part in parts]
                # print(sylls)
                return sylls

        return None

    def check_model_split(self, word):
        if self.split_model is not None:
            try:
                res = self.split_model.split_word(word)
                # print(f"SyllSplitTF: {word:20s} --> {res}")
                if Tools.binary_search(self.uq_sylls, '#' + res[0]) > -1:
                    res[0] = '#' + res[0]
                if Tools.binary_search(self.uq_sylls, res[-1] + '$') > -1:
                    res[-1] = res[-1] + '$'
                # print(f"\tAfter: {res}")
                return res
            except KeyError as e:
                return None
        else:
            return None

    def get_words(self) -> set:
        return set(self.data.keys())

    def get_syllabic_decomposition_for_word(self, word, b_start=False, b_end=False, b_ignore_plural=False):
        """
        Check if there is a known syllabic decomposition from Wiktionary for this word. If so, return it.

        :param word:
        :param b_start: prepend '#' in front of first syllable; global settings take precedence
        :param b_end: prepend '$' in front of last syllable; global settings take precedence
        :param b_ignore_plural: just for compatibility with ENSyllables2
        :return:
        """
        if word in self.data:
            res = deepcopy(self.data[word])
        # If word is multi-word expression, and is unknown as such, check if individual words are known
        elif ' ' in word:
            parts = word.split()
            res = []
            for part in parts:
                if part in self.data:
                    res.append(deepcopy(self.data[part])[0])
                else:
                    # print(f"Splitter check: {word}-{part}")
                    temp_res = self.check_model_split(part)
                    if temp_res is None:
                        return None
                    else:
                        res.append(temp_res)
            # print(f'Wazaa! : {res}')
        else:
            res = self.check_model_split(word)
            if res is None:
                return None
            else:
                res = [res]

        if not self.f_start and b_start:
            res[0][0] = '#' + res[0][0]
        if not self.f_end and b_end:
            res[-1][-1] = res[-1][-1] + '$'

        return res

    def get_all_syllabic_decompositions_for_word(self, word):
        """
        Find all possible syllabic decompositions using the known syllables.
        A valid decomposition is a contiguous sequence of known syllables that makes up the word.

        :param word:
        :return:
        """
        decomps = []

        def recur(prev_decomp_idx, start):
            new_decomps_idx = []
            last_idx = []
            for i in range(start+1, len(word)+1):
                # From start
                if Tools.binary_search(self.uq_sylls, word[start:i]) >= 0:
                    decomps.append((prev_decomp_idx, word[start:i]))
                    last_idx.append(i)
                    new_decomps_idx.append(len(decomps)-1)

            if new_decomps_idx:
                for i, e in enumerate(new_decomps_idx):
                    if last_idx[i] < len(word):
                        recur(e, last_idx[i])
                    else:
                        decomps.append((e, '#END'))

            return new_decomps_idx

        # Run recursive algorithm
        recur(-1, 0)

        # Extract valid decompositions
        seqs = []
        for tpl in decomps[::-1]:
            if tpl[1] == "#END":
                seq = []
                link = tpl[0]
                while link > -1:
                    seq.append(decomps[link][1])
                    link = decomps[link][0]
                seqs.append(seq[::-1])

        # for seq in seqs:
        #     print(seq)

        return seqs

    def decomp_to_idx(self, decomp: list):
        """
        Transform a decomposition (=list of syllables) into a list of indices, where each index represents the
        position of the syllable in the list of unique syllables self.uq_sylls

        :param decomp:
        :return: list of same length as decomp, where each entry has been replaced by its index
        """
        res = [Tools.binary_search(self.uq_sylls, syll) for syll in decomp]

        return res

    def compute_transition_probs(self, b_only_single_words=True):
        """
        Compute the transition probabilities between syllables.

        :param b_only_single_words: only consider single words, not multi-word expressions.
        :return: (vec_start, mtx_trans), where vec_start is a numpy array, and mtx_trans is a CSR sparse matrix
        """
        vec_start = np.zeros((len(self.uq_sylls)), dtype=np.float32)
        mtx_trans = np.zeros((len(self.uq_sylls), len(self.uq_sylls)), dtype=np.float32)
        for word, decomps in self.data.items():
            if b_only_single_words and len(decomps) > 1:
                continue
            for decomp in decomps:
                decomp_idx = self.decomp_to_idx(decomp)
                # Update start probability
                vec_start[decomp_idx[0]] += 1
                # Update transition probabilities
                for i in range(1, len(decomp_idx)):
                    # print(f"Adding one to {decomp_idx[i-1]}-->{decomp_idx[i]}")
                    mtx_trans[decomp_idx[i-1], decomp_idx[i]] += 1
        # Row-normalize vector and matrix so each row sums to 1
        vec_start = vec_start/np.sum(vec_start)
        mtx_trans = normalize(mtx_trans, axis=1, norm='l1')
        # Transform to sparse matrix
        mtx_trans = csr_matrix(mtx_trans)

        return vec_start, mtx_trans

    def get_prob_for_decomp(self, decomps: list, zero_offset=1e-6):
        """
        Get probability for given decomposition

        :param decomps: list of lists, where each nested list contains the decomposition for a word; should only
        contain more than one sublist in case of multi-word expression
        :param zero_offset: value to use if transition probability is zero according to our stored probabilities
        :return: (log of probability, probability)
        """
        log_p = 0
        for decomp in decomps:
            decomp_idx = self.decomp_to_idx(decomp)
            log_p += math.log(self.vec_start[decomp_idx[0]])
            for i in range(1, len(decomp_idx)):
                trans_prob = self.mtx_trans[decomp_idx[i-1], decomp_idx[i]]
                if trans_prob == 0:
                    trans_prob = zero_offset
                log_p += math.log(trans_prob)

        return log_p, math.exp(log_p)


if __name__ == '__main__':
    wikt = NLWiktionarySyllables2(b_overwrite=False, f_start=0.1, f_end=0.1)
    # cnnl = ConceptNetNLVecs()

    # decomps = wikt.get_all_syllabic_decompositions_for_word('aansprakelijkheid')
    # print(len(decomps))

    words_wikt = set(wikt.data.keys())
    # words_cnnl = set(cnnl.get_words())

    print(f"#Words syllables: {len(words_wikt)}")

    # print(f"Words cnnl      : {len(words_cnnl)}")
    # print(f"Intersection    : {len(words_wikt.intersection(words_cnnl))}")

    print("Enter a word to get its syllabic decomposition (if known): ", end='')
    tok = input()
    while tok != "EXIT":
        sylls = wikt.get_syllabic_decomposition_for_word(tok)
        if sylls is None:
            print("Word not present in corpus.")
        else:
            print(f"{tok}\t: {sylls}")
            # log_p, p = wikt.get_prob_for_decomp(sylls)
            # print(f"p: {p:.6f} --> {log_p:.6f}")

        print("============================================================")
        print("Enter a word to get its syllabic decomposition (if known): ", end='')
        tok = input()
