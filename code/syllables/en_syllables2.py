"""
Load syllabic decompositions scraped form http://www.syllablecount.com/

Same as ENSyllables, except that this implementation allows to store hardcoded starting and ending tags for a
given fraction of syllables, rather than "all on" or "all off".
"""
import math
from collections import Counter
from copy import deepcopy

import torch
from sklearn.preprocessing import normalize
from scipy.sparse import csr_matrix
import numpy as np
import dill
import os

from wikidump_reader.wikidump_reader import WikiDumpReader

# from pretrained_vecs.conceptnet_nl_vecs import ConceptNetNLVecs
from models.syllables.split_model_tf_loader import SplitModelTFLoader
from config import Config, Lang
from tools.tools import Tools


class ENSyllables2:
    def __init__(self, data_file: str=None, split_model_base: str=None, split_model_version: str=None,
                 overlap_with_vecs=None, f_start=0., f_end=0., device=torch.device('cuda')):
        """
        If initialized with f_start/f_end set to >1, will hardcode starting and ending variants of appropriate
        syllables into memory.
        Alternatively, if set to 0, one can call get_syllabic_decomposition_for_word with the same tags,
        and get starting/ending tokens generated on the fly.

        :param data_file:
        :param split_model:
        :param overlap_with_vecs: allows to provide a dictionary of loaded embeddings; only syllables will
        be kept for words from the Wiktionary dump that are also present in the provided overlap_with_vecs
        :param f_start: float in [0, 1] that indicates the ratio of starting syllables to hardcode, e.g., is set to 0.1,
        the top 10% most often occurring starting syllables will be hardcoded
        :param f_end: analogous to f_start
        """
        if data_file is None:
            data_file = os.path.join(Config.DATA_DIR, 'en_syllables_BAK.txt')

        if f_start < 0 or f_start > 1:
            raise ValueError(f"Parameter f_start should be > 0 and < 1. Got value: {f_start}")
        if f_end < 0 or f_end > 1:
            raise ValueError(f"Parameter f_end should be > 0 and < 1. Got value: {f_end}")
        self.f_start, self.f_end = f_start, f_end

        print("Loading EN syllables...", end='', flush=True)
        data = {}
        syll_start = Counter()
        syll_end = Counter()
        with open(data_file, 'r') as fin:
            for line in fin:
                parts = line.strip().lower().split('\t')
                sylls = parts[1].split('-')
                if f_start:
                    # sylls[0] = '#' + sylls[0]
                    syll_start[sylls[0]] += 1
                if f_end:
                    # sylls[-1] = sylls[-1] + '$'
                    syll_end[sylls[-1]] += 1
                data[parts[0]] = [sylls]

        # Take overlap, if necessary
        if overlap_with_vecs is not None:
            to_remove = set(data.keys()).difference(set(overlap_with_vecs.keys()))
            print(f"\tTo remove: {len(to_remove)} words out of {len(data)}")
            for word in to_remove:
                del data[word]
            # uq_sylls = set()
            # for k, v in self.data.items():
            #     for e in v:
            #         uq_sylls.update(e)
            # print(f"\tNumber of kept syllables: {len(uq_sylls)}")
            print(f"\tNumber of kept words: {len(data)}")

        # Add explicit start and end tokens
        nb_start = int(len(syll_start) * f_start)
        nb_end = int(len(syll_end) * f_end)
        # Gather tokens to be hardcoded
        set_start = {k for k, v in syll_start.most_common(nb_start)}
        set_end = {k for k, v in syll_end.most_common(nb_end)}
        # Update syllabic decompositions
        # This loop will also initialize uq_sylls
        uq_sylls = set()
        for k, v in data.items():
            update_start = v[0][0] in set_start
            update_end = v[0][-1] in set_end
            if update_start:
                v[0][0] = '#' + v[0][0]
            if update_end:
                v[0][-1] = v[0][-1] + '$'
            # It is "cheaper" to reassign, than to recheck for update_start/end to be true
            data[k] = v
            for e in v:
                uq_sylls.update(e)
        print(" done!")

        print(f"Loaded with syllabes: {len(data)}")
        self.data = data
        self.uq_sylls = sorted(uq_sylls)
        self.nb_uq_sylls = len(self.uq_sylls)
        self.vec_start, self.mtx_trans = self.compute_transition_probs(b_only_single_words=True)
        print(f"\tUnique syllables: {len(uq_sylls)}")

        self.split_model = None
        if split_model_base is not None:
            self.split_model = SplitModelTFLoader(base_path_to_model=os.path.join(Config.MODELS_DIR,
                                                                                  'Paper', split_model_base),
                                                  model_version=split_model_version,
                                                  lang=Lang.EN,
                                                  device=device)

    def check_model_split(self, word):
        if self.split_model is not None:
            try:
                res = self.split_model.split_word(word)
                # print(f"SyllSplitTF: {word:20s} --> {res}")
                if Tools.binary_search(self.uq_sylls, '#' + res[0]) > -1:
                    res[0] = '#' + res[0]
                if Tools.binary_search(self.uq_sylls, res[-1] + '$') > -1:
                    res[-1] = res[-1] + '$'
                # print(f"\tAfter: {res}")
                return res
            except KeyError as e:
                return None
        else:
            return None

    def get_words(self) -> set:
        return set(self.data.keys())

    def get_syllabic_decomposition_for_word(self, word, b_start=False, b_end=False, b_ignore_plural=False):
        """
        Check if there is a known syllabic decomposition from Wiktionary for this word. If so, return it.

        :param word:
        :param b_start: prepend '#' in front of first syllable; ignored if self.f_start > 0
        :param b_end: prepend '$' in front of last syllable; ignored if self.f_end > 0
        :param b_ignore_plural: if True, and word is not known, will check if word ends with 's', and if so,
         if word without last 's' is known, return the decomposition for this word
        :return:
        """
        if word in self.data:  # Note that some expressions with whitespaces have an embedding as such
            res = deepcopy(self.data[word])
        # If word is multi-word expression, and is unknown as such, check if individual words are known
        elif ' ' in word:
            parts = word.split()
            res = []
            for part in parts:
                part_res = self._parse_token(part, b_start=b_start, b_end=b_end, b_ignore_plural=b_ignore_plural)
                # print(f"word: {word} --> part_res: {part} = {part_res}")
                if part_res is None:
                    return None
                else:
                    res.append(part_res)
        else:
            # parts = word.split("-")
            # ress = [self._parse_token(part, b_start=b_start, b_end=b_end, b_ignore_plural=b_ignore_plural) for part in parts]
            # if None in ress:
            #     print(f"'s Got None in it!: {word} -- > {ress}")
            #     return None
            #
            # res = []
            # for part in ress:
            #     res += part
            res = self._parse_token(word, b_start=b_start, b_end=b_end, b_ignore_plural=b_ignore_plural)
            if res is None:
                return None
            res = [res]

        return res

    def _parse_token(self, token, b_start=False, b_end=False, b_ignore_plural=False):
        """
        Parse a single token.

        :param token:
        :return:
        """
        res = None
        if token in self.data:
            res = deepcopy(self.data[token][0])
        else:
            b_check = True
            if b_ignore_plural:
                res = self._plural_check_token(token)
                b_check = (res is None)
            if b_check:
                res = self.check_model_split(token)
                if res is None:
                    return None
                # else:
                #     if b_ignore_plural:
                #         print(f"before: {res}")
                #         self._plural_check_decomp(res)
                #         print(f"after : {res}")

        self._add_start_end(res, b_start, b_end)

        return res

    def _add_start_end(self, decomp, b_start=False, b_end=False):
        """
        Modifies decomp in place.

        :param decomp:
        :param b_start:
        :param b_end:
        :return:
        """
        if decomp is not None:
            if not self.f_start and b_start:
                decomp[0][0] = '#' + decomp[0][0]
            if not self.f_end and b_end:
                decomp[-1][-1] = decomp[-1][-1] + '$'

    def _plural_check_token(self, token):
        res = None
        if token[-1] == 's' and token[:-1] in self.data:
            print(f"'s' hit for {token}")
            res = deepcopy(self.data[token[:-1]])
        elif len(token) > 2 and token[-2:] == 'es' and token[:-2] in self.data:
            print(f"'es' hit for {token}")
            res = deepcopy(self.data[token[:-2]])
        # elif len(token) > 3 and token[-3:] == 'ies' and token[:-3] + 'y' in self.data:
        #     token_singular = token[:-3] + 'y'
        #     print(f"'ies' hit for {token} --> {token_singular}")
        #     res = deepcopy(self.data[token_singular])

        return res[0] if res is not None else res

    def _plural_check_decomp(self, decomp):
        """
        Modifies decomp in place

        :param decomp: list containing syllabic decomposition
        :return:
        """
        # TODO: tidy this up so that length check is not necessary
        if decomp[-1] and decomp[-1][-1] == 's' and decomp[-1][:-1] in self.uq_sylls:
            print(f"Plural hit 1: {decomp}")
            decomp[-1] = decomp[-1][:-1]
        elif len(decomp[-1]) > 2 and decomp[-1][-2:] == 'es' and decomp[-1][:-2] in self.uq_sylls:
            print(f"Plural hit 2: {decomp}")
            decomp[-1] = decomp[-1][:-2]

    def get_all_syllabic_decompositions_for_word(self, word):
        """
        Find all possible syllabic decompositions using the known syllables.
        A valid decomposition is a contiguous sequence of known syllables that makes up the word.

        :param word:
        :return:
        """
        decomps = []

        def recur(prev_decomp_idx, start):
            new_decomps_idx = []
            last_idx = []
            for i in range(start+1, len(word)+1):
                # From start
                if Tools.binary_search(self.uq_sylls, word[start:i]) >= 0:
                    decomps.append((prev_decomp_idx, word[start:i]))
                    last_idx.append(i)
                    new_decomps_idx.append(len(decomps)-1)

            if new_decomps_idx:
                for i, e in enumerate(new_decomps_idx):
                    if last_idx[i] < len(word):
                        recur(e, last_idx[i])
                    else:
                        decomps.append((e, '#END'))

            return new_decomps_idx

        # Run recursive algorithm
        recur(-1, 0)

        # Extract valid decompositions
        seqs = []
        for tpl in decomps[::-1]:
            if tpl[1] == "#END":
                seq = []
                link = tpl[0]
                while link > -1:
                    seq.append(decomps[link][1])
                    link = decomps[link][0]
                seqs.append(seq[::-1])

        # for seq in seqs:
        #     print(seq)

        return seqs

    def decomp_to_idx(self, decomp: list):
        """
        Transform a decomposition (=list of syllables) into a list of indices, where each index represents the
        position of the syllable in the list of unique syllables self.uq_sylls

        :param decomp:
        :return: list of same lenght as decomp, where each entry has been replaced by its index
        """
        # print(f"decomp: {decomp}")
        res = [Tools.binary_search(self.uq_sylls, syll) for syll in decomp]

        # for i in res:
        #     if i < 0:
        #         raise ValueError(f"Decomp: {decomp}")

        return res

    def compute_transition_probs(self, b_only_single_words=True):
        """
        Compute the transition probabilities between syllables.

        :param b_only_single_words: only consider single words, not multi-word expressions.
        :return: (vec_start, mtx_trans), where vec_start is a numpy array, and mtx_trans is a CSR sparse matrix
        """
        vec_start = np.zeros((len(self.uq_sylls)), dtype=np.float32)
        mtx_trans = np.zeros((len(self.uq_sylls), len(self.uq_sylls)), dtype=np.float32)
        for word, decomps in self.data.items():
            if b_only_single_words and len(decomps) > 1:
                continue
            for decomp in decomps:
                decomp_idx = self.decomp_to_idx(decomp)
                # Update start probability
                vec_start[decomp_idx[0]] += 1
                # Update transition probabilities
                for i in range(1, len(decomp_idx)):
                    # print(f"Adding one to {decomp_idx[i-1]}-->{decomp_idx[i]}")
                    mtx_trans[decomp_idx[i-1], decomp_idx[i]] += 1
        # Row-normalize vector and matrix so each row sums to 1
        vec_start = vec_start/np.sum(vec_start)
        mtx_trans = normalize(mtx_trans, axis=1, norm='l1')
        # Transform to sparse matrix
        mtx_trans = csr_matrix(mtx_trans)

        return vec_start, mtx_trans

    def get_prob_for_decomp(self, decomps: list, zero_offset=1e-6):
        """
        Get probability for given decomposition

        :param decomps: list of lists, where each nested list contains the decomposition for a word; should only
        contain more than one sublist in case of multi-word expression
        :param zero_offset: value to use if transition probability is zero according to our stored probabilities
        :return: (log of probability, probability)
        """
        log_p = 0
        for decomp in decomps:
            decomp_idx = self.decomp_to_idx(decomp)
            log_p += math.log(self.vec_start[decomp_idx[0]])
            for i in range(1, len(decomp_idx)):
                trans_prob = self.mtx_trans[decomp_idx[i-1], decomp_idx[i]]
                if trans_prob == 0:
                    trans_prob = zero_offset
                log_p += math.log(trans_prob)

        return log_p, math.exp(log_p)


if __name__ == '__main__':
    wikt = ENSyllables2(f_start=.0, f_end=.0)
    # exit()

    decomps = wikt.get_all_syllabic_decompositions_for_word('decomposition')
    print(f"Number of possible decompositions for word `decomposition': {len(decomps)}")

    words_wikt = set(wikt.data.keys())

    print("Enter a word to get its syllabic decomposition (if known): ", end='')
    tok = input()
    while tok != "EXIT":
        sylls = wikt.get_syllabic_decomposition_for_word(tok)
        if sylls is None:
            print("Word not present in corpus.")
        else:
            print(f"{tok}\t: {sylls}")
            log_p, p = wikt.get_prob_for_decomp(sylls)
            print(f"p: {p:.6f} --> {log_p:.6f}")
            all_decomps = wikt.get_all_syllabic_decompositions_for_word(tok)
            print(f"Number of decompositions for word: {len(all_decomps)}")
            # for d in all_decomps:
            #     print(f"\t{d}")

        print("============================================================")
        print("Enter a word to get its syllabic decomposition (if known): ", end='')
        tok = input()
