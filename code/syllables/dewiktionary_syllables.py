import dill
import os

from wikidump_reader.wikidump_reader import WikiDumpReader

# from pretrained_vecs.conceptnet_nl_vecs import ConceptNetNLVecs
from config import Config


class DEWiktionarySyllables:
    def __init__(self, b_overwrite=False):
        fout = os.path.join(Config.WIKIPEDIA_DIR, 'dewiktionary_with_syllables.pkl')

        if not b_overwrite and os.path.isfile(fout):
            print("DEWiktionary: Loading pre-extracted syllables...")
            data = dill.load(open(fout, 'rb'))
        else:
            print("Loading DEWiktionary...", end='', flush=True)
            data = {}
            wdr = WikiDumpReader()

            with_sylls = 0
            no_sylls = 0
            for page_title, page_text in wdr.read_page(file=Config.WIKTIONARY_DE_DUMP):
                # print("============================================================")
                sylls = self.extract_syllables(page_text)
                # Not None and not empty
                if sylls is not None and sylls:
                    data[page_title] = sylls
                    with_sylls += 1
                else:
                    no_sylls += 1
            print(" done!")

            print(f"Loaded with syllabes: {len(data)}")
            print(f"No syllables        : {no_sylls}")

            dill.dump(data, open(fout, 'wb'))

        self.data = data

    @classmethod
    def extract_syllables(cls, page_text):
        b_first = True
        b_next = False

        page_text = page_text.split('\n')
        for line in page_text:
            line = line.strip()
            # print(line)
            if b_first and line.startswith('{{'):
                b_first = False

            if line.startswith("{{Worttrennung}}"):
                b_next = True
            elif b_next:
                line = line[1:].strip()
                # print(line)
                parts = line.split(',')
                sylls = []
                if parts[0]:
                    sylls = [parts[0].split('·')]
                    # print(sylls)
                return sylls

        return None

    def get_words(self) -> set:
        return set(self.data.keys())

    def get_syllables_for_word(self, word):
        if word in self.data:
            return self.data[word]
        else:
            return None


if __name__ == '__main__':
    wikt = DEWiktionarySyllables()
    # cnnl = ConceptNetNLVecs()

    words_wikt = set(wikt.data.keys())
    # words_cnnl = set(cnnl.get_words())

    print(f"Words wiktionary: {len(words_wikt)}")
    # print(f"Words cnnl      : {len(words_cnnl)}")
    # print(f"Intersection    : {len(words_wikt.intersection(words_cnnl))}")
