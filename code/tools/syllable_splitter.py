# Heuristic NL syllable splitter
# See:
# http://www.dutchgrammar.com/nl/?n=SpellingAndPronunciation.05
# https://kevinvermassen.be/2016/08/01/verdelen-in-lettergrepen/
# https://onzetaal.nl/taaladvies/afbreekregels
# https://www.taaltelefoon.be/afbreking-van-woorden-1-regels
from termcolor import cprint

from syllables.nlwiktionary_syllables import NLWiktionarySyllables


class FIFOStr:
    def __init__(self, size: int, init=''):
        if len(init) > size:
            raise ValueError("Length of provided intial value is larger than size of buffer.")
        self.size = 3
        self.buffer = init

    def append(self, c: str):
        if len(c) > 1:
            raise ValueError("Should only append characters.")
        self.buffer += c
        if len(self.buffer) > self.size:
            self.buffer = self.buffer[1:]

        return self.buffer


class SyllableSplitter:
    VOWEL = {'a', 'e', 'i', 'o', 'u', 'y'}  # Consider 'y' as equivalent to 'i'
    TWEEKLANKEN = {'aa', 'au', 'ee', 'ei', 'ie', 'ij', 'oe', 'oo', 'ou', 'ui', 'uu'}
    DOUBLE_V = {'aa', 'ee', 'oo', 'uu'}

    IJ_ATTACH = {'n'}

    B_DEBUG = False

    @classmethod
    def nl_split(cls, word: str):
        """
        Split a Dutch word into syllables according to the common rules for doing so

        :param word:
        :return: list of syllables composing the word
        """
        if len(word) <= 3:
            return [word]

        word = word.replace('sch', '♥')

        sylls = []

        fstr = FIFOStr(size=3)
        idx_split = 0
        # b_prev_ij = False
        b_prev_dbl_v = False
        for i, c in enumerate(word):
            sub = fstr.append('v' if c in cls.VOWEL else 'c')

            first_two = word[i - 2:i]
            last_two = word[i - 1:i + 1]
            # Je mag nooit één klinker alleen zetten aan het begin of het einde van een woord!

            # Als er één tussenmedeklinker is, dan gaat die naar de volgende lettergreep.
            if sub == 'vcv':
                if b_prev_dbl_v:
                    prev_split = idx_split
                    idx_split = i
                    fstr = FIFOStr(3, sub[2:])
                    sylls.append(word[prev_split:idx_split])
                    if cls.B_DEBUG:
                        print(f"Split vcv (a): {sylls[-1]}")
                else:
                    prev_split = idx_split
                    idx_split = i-1
                    fstr = FIFOStr(3, sub[1:])
                    sylls.append(word[prev_split:idx_split])
                    # print(f"Split vcv (b): {sylls[-1]}")
            # Twee tussenmedeklinkers worden van elkaar gescheiden.
            # -ch en –th worden niet van elkaar gescheiden en gaan allebei naar de volgende lettergreep.
            elif sub == 'vcc':
                if first_two == 'ij' and not word[i] in cls.IJ_ATTACH:
                    prev_split = idx_split
                    idx_split = i
                    fstr = FIFOStr(3, sub[2:])
                    sylls.append(word[prev_split:idx_split])
                    cprint(f"Split on ij: {word}", color='cyan')
                    if cls.B_DEBUG:
                        print(f"Split vcc - 'ij': {sylls[-1]}")
                elif last_two == 'ch' or last_two == 'th':
                    prev_split = idx_split
                    idx_split = i-1
                    fstr = FIFOStr(3, sub[1:])
                    sylls.append(word[prev_split:idx_split])
                    if cls.B_DEBUG:
                        print(f"Split vcc1: {sylls[-1]}")
                # elif last_two == 'sj':
                #     next_letter = word[i+1] if len(word) > i+1 else ''
                #     # 'sje' -> 'visje' --> split between 's' and 'j'
                #     if next_letter == 'e':
                #         prev_split = idx_split
                #         idx_split = i
                #         fstr = FIFOStr(3, sub[2:])
                #         sylls.append(word[prev_split:idx_split])
                #         print(f"Split vcc1: {sylls[-1]}")
                else:
                    prev_split = idx_split
                    idx_split = i
                    fstr = FIFOStr(3, sub[2:])
                    sylls.append(word[prev_split:idx_split])
                    if cls.B_DEBUG:
                        print(f"Split vcc2: {sylls[-1]}")
            # elif sub == 'ccc':
            #     if b_prev_ij:
            #         prev_split = idx_split
            #         idx_split = i
            #         fstr = FIFOStr(3, sub[2:])
            #         sylls.append(word[prev_split:idx_split])
            #         if cls.B_DEBUG:
            #             print(f"Split ccc: {sylls[-1]}")
            #     else:
            #         cprint(f"Wat nou Jose? {word} --> {word[:i+1]}", color='red')
            elif sub == 'cvv' or sub == 'vvv':
                if first_two in cls.TWEEKLANKEN:
                    prev_split = idx_split
                    idx_split = i
                    fstr = FIFOStr(3, sub[2:])
                    sylls.append(word[prev_split:idx_split])
                elif sub == 'cvv' and last_two in cls.TWEEKLANKEN:
                    # Don't split in this case --> 'koeien' -> 'koe' should be kept together
                    if cls.B_DEBUG:
                        print(f"Split cvv continue")
                    continue
                else:
                    prev_split = idx_split
                    idx_split = i
                    fstr = FIFOStr(3, sub[2:])
                    sylls.append(word[prev_split:idx_split])
                    if cls.B_DEBUG:
                        print(f"Split cvv/vvv: {sylls[-1]}")

            b_prev_dbl_v = True if first_two in cls.DOUBLE_V else False

        sylls.append(word[idx_split:])
        for i, e in enumerate(sylls):
            sylls[i] = e.replace('♥', 'sch')

        return sylls


if __name__ == '__main__':
    print(SyllableSplitter.nl_split('koopakte'))
    # print(SyllableSplitter.nl_split('visje'))
    # print(SyllableSplitter.nl_split('gekalibreerde'))

    b_test_against_wikt = True
    if b_test_against_wikt:
        wikt = NLWiktionarySyllables(b_overwrite=False)
        single, equal = 0, 0
        for word, decomps in wikt.data.items():
            splits = []
            parts = word.split()
            if len(parts) > 1:
                continue
            single += 1
            for part in parts:
                splits.append(SyllableSplitter.nl_split(part))
            if splits == decomps:
                equal += 1
            # else:
            #     print(f"{splits}\n{decomps}\n")

        print(f"Equal splits: {equal} out of {single}: {100*equal/single:.2f}%.")
