"""A collection of useful tools to process data"""
import heapq
from bisect import bisect_left
from operator import itemgetter

import numpy as np
import torch


class Tools:
    # Taken from: https://stackoverflow.com/questions/212358/binary-search-bisection-in-python
    @staticmethod
    def binary_search(a, x, lo=0, hi=None):  # can't use a to specify default for hi
        """

        :param a: list to search in
        :param x: item to search for
        :param lo: starting index
        :param hi: ending index
        :return:
        """
        hi = hi if hi is not None else len(a)  # hi defaults to len(a)
        pos = bisect_left(a, x, lo, hi)  # find insertion position
        return pos if pos != hi and a[pos] == x else -1  # don't walk off the end

    @staticmethod
    def to_one_hot(n: int, size: int):
        """
        Return a vector of zeros of size 'size', with the n-th position set to 1.

        :param n:
        :param size:
        :return:
        """
        if n >= size:
            raise ValueError(f"Position to be one-hot encoded can not exceed size of vector! Got n={n}, size={size}.")

        vec = np.zeros(size)
        vec[n] = 1

        return vec

    @staticmethod
    def to_n_hot(idxs, size: int):
        """
        Return a vector of zeros of size 'size', with the positions in idxs set to 1.

        :param idxs: iterable containing the positions of the 1s
        :param size:
        :return:
        """
        vec = np.zeros(size)
        for idx in idxs:
            if idx >= size:
                raise ValueError(f"Position to be one-hot encoded can not exceed size of vector! Got idx={idx}, size={size}.")
            vec[idx] = 1

        return vec

    # Taken from https://stackoverflow.com/questions/22227595/convert-integer-to-binary-array-with-suitable-padding
    @staticmethod
    def bin_array(num, m):
        """Convert a positive integer num into an m-bit bit vector"""
        return np.array(list(np.binary_repr(num).zfill(m))).astype(np.int8)

    # Taken from https://stackoverflow.com/questions/22227595/convert-integer-to-binary-array-with-suitable-padding
    def vec_bin_array(arr, m):
        """
        Arguments:
        arr: Numpy array of positive integers
        m: Number of bits of each integer to retain

        Returns a copy of arr with every element replaced with a bit vector.
        Bits encoded as int8's.
        """
        to_str_func = np.vectorize(lambda x: np.binary_repr(x).zfill(m))
        strs = to_str_func(arr)
        ret = np.zeros(list(arr.shape) + [m], dtype=np.int8)
        for bit_ix in range(0, m):
            fetch_bit_func = np.vectorize(lambda x: x[bit_ix] == '1')
            ret[...,bit_ix] = fetch_bit_func(strs).astype("int8")

        return ret

    @staticmethod
    def inc_avg(avg, item, iter: int):
        """
        Incrementally compute the average.

        !!! If the provided objects are lists, they will be updated in place. !!!

        :param avg: the average to update
        :param item: the item to update the average with
        :param iter: which element is this in the sequence? (start counting at 1, not at 0!)
        :return:
        """
        if not avg.__class__ == item.__class__:
            raise ValueError(f"'avg' and 'item' objects should be of same type, got avg={avg.__class__.__name__} vs."
                             f" item={item.__class__.__name__}")

        if isinstance(avg, list):
            for i, e in enumerate(avg):
                avg[i] = e + (item[i] - e)/iter

        else:
            avg = avg + (item - avg)/iter

        # Always return updated average, regardless of type, to keep things simple
        return avg

    @staticmethod
    def cosim(a, b):
        """
        Compute the cosine similarity between vectors a and b.

        :param a: Numpy 1-D array
        :param b: Numpy 1-D array
        :return: cosine similarity
        """
        return np.inner(a, b)/(np.linalg.norm(a)*np.linalg.norm(b))

    @staticmethod
    def least_common_values(counter, to_find=None):
        if to_find is None:
            return sorted(counter.items(), key=itemgetter(1), reverse=False)
        return heapq.nsmallest(to_find, counter.items(), key=itemgetter(1))

    @staticmethod
    def tensor_index_of(t: torch.Tensor, e):
        """
        Get the index of the first appearance of element 'e' in 1D Tensor 't'.

        :param t:
        :param e:
        :return: index of first appearance if element is present, else -1
        """
        bool_t = (t == e).nonzero()
        return bool_t[0].item() if len(bool_t) else -1

    @staticmethod
    def sum_vectors(vecs):
        if len(vecs) == 1:
            return vecs[0]

        s = np.sum(vecs, axis=0)
        s /= np.linalg.norm(s)

        return s
