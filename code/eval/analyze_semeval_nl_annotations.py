"""
Compare Dutch annotations between annotators, and combine them into one file.
"""
import itertools
import math
import os

import numpy as np
import pandas as pd
from scipy.stats import spearmanr, pearsonr

from config import Config

col_names = {0: "Word1", 1: "Word2", 2: "Score"}

laurent = pd.read_csv(os.path.join(Config.NL_SEMEVAL_DIR, "SemEval2017_NL_Annotations_Laurent.csv"),
                      header=None, dtype={2: int})
laurent.rename(columns=col_names, inplace=True)
matthias = pd.read_csv(os.path.join(Config.NL_SEMEVAL_DIR, "SemEval2017_NL_Annotations_Matthias.csv"),
                       header=None, dtype={2: int})
matthias.rename(columns=col_names, inplace=True)
simon = pd.read_csv(os.path.join(Config.NL_SEMEVAL_DIR, "SemEval2017_NL_Annotations_Simon.csv"),
                    header=None, dtype={2: int})
simon.rename(columns=col_names, inplace=True)


def compare_anotations():
    nb_equal = 0
    nb_within_1 = 0
    for i, row in laurent.iterrows():
        if row['Score'] == matthias.iloc[i]['Score'] == simon.iloc[i]['Score']:
            nb_equal += 1
        elif abs(row['Score'] - matthias.iloc[i]['Score']) <= 1\
                and abs((row['Score'] - simon.iloc[i]['Score'])) <= 1\
                and abs((simon.iloc[i]['Score'] - matthias.iloc[i]['Score'])):
            nb_within_1 += 1

    print(f"Equal between all 3: {nb_equal}")
    print(f"Not equal but only 1 difference: {nb_within_1}")

    s_corr = []
    p_corr = []
    at_comb = 0
    for a, b in itertools.combinations([laurent, matthias, simon], 2):
        at_comb += 1
        sp_r = spearmanr(a['Score'], b['Score'])
        pe_r = pearsonr(a['Score'], b['Score'])
        print(f'Comb{at_comb} Spearman: {sp_r[0]}')
        print(f'Comb{at_comb} Pearson : {pe_r[0]}')
        s_corr.append(sp_r[0])
        p_corr.append(pe_r[0])
    print("Average over all 3 combinations:")
    print(f"Spearman: {np.mean(s_corr)}"'')
    print(f"Pearson : {np.mean(p_corr)}"'')


def to_recheck(target: pd.DataFrame, baseline: tuple):
    """

    :param target: the target CSV/DataFrame from which to look
    :param baseline: the other two sets of annotations that are used as baseline
    :return: list of word pairs to be revisited
    """
    nb = 0
    tuples = []
    for i, row in target.iterrows():
        avg_other = (baseline[0].iloc[i][2] + baseline[1].iloc[i][2])/2
        diff = math.fabs(row[2] - avg_other)
        if diff > 1:
            nb += 1
            tuples.append((row[0], row[1], row[2]))
            print(f"{i:3d}: {row[0]} - {row[1]}")

    print(f"Total number of pairs to recheck: {nb}")

    return tuples


def write_recheck(tpls, name):
    with open(os.path.join(Config.NL_SEMEVAL_DIR, f"SemEval2017_NL_Annotations_{name}_RECHECK_LIST.csv"), "w") as fout:
        for tpl in tpls:
            fout.write(f"{tpl[0]},{tpl[1]},{tpl[2]}\n")


def load_recheck(df, name):
    df_re = pd.read_csv(os.path.join(Config.NL_SEMEVAL_DIR, f"SemEval2017_NL_Annotations_{name}_RECHECK.csv"), header=None)
    df_re.rename(columns=col_names, inplace=True)
    for i, row in df_re.iterrows():
        orig_row_idx = df[(df['Word1'] == row['Word1']) & (df['Word2'] == row['Word2'])].index[0]
        df.loc[orig_row_idx, 'Score'] = row['Score']

    return df


def combine_annotations(dfs, file_out=None):
    combined = []
    for i, row in dfs[0].iterrows():
        score = np.mean([dfs[j].iloc[i]['Score'] for j in range(len(dfs))])
        combined.append([row['Word1'], row['Word2'], score])

    df = pd.DataFrame(data=combined)
    if file_out is not None:
        df.to_csv(path_or_buf=file_out, header=False, index=False)
        print(f"Combined result written to {file_out}")

    return df


print("Before recheck:")
compare_anotations()

b_dump_recheck = False
b_combine = False
if b_dump_recheck:
    print("\nFor Laurent:")
    write_recheck(to_recheck(laurent, (matthias, simon)), "Laurent")

    print("\nFor Matthias:")
    write_recheck(to_recheck(matthias, (laurent, simon)), "Matthias")

    print("\nFor Simon:")
    write_recheck(to_recheck(simon, (laurent, matthias)), "Simon")
else:
    laurent = load_recheck(laurent, "Laurent")
    matthias = load_recheck(matthias, "Matthias")
    simon = load_recheck(simon, "Simon")
    print("============================================================")
    print("After recheck:")
    compare_anotations()

if b_combine:
    df_nl = combine_annotations([laurent, matthias, simon],
                                file_out=os.path.join(Config.SEMEVAL_COMB_DIR, "nl.csv") if b_combine else None)
    df_nl.rename(columns=col_names, inplace=True)
else:
    df_nl = pd.read_csv(os.path.join(Config.SEMEVAL_COMB_DIR, f"nl.csv"), header=None)
    df_nl.rename(columns=col_names, inplace=True)
# Compare Dutch annotations to English annotations
df_en = pd.read_csv(os.path.join(Config.SEMEVAL_COMB_DIR, f"en.csv"), header=None)
df_en.rename(columns=col_names, inplace=True)

print("\nSpearman rank correlation between Dutch and English annotations:")
print(spearmanr(df_nl['Score'], df_en['Score']))
print(pearsonr(df_nl['Score'], df_en['Score']))
