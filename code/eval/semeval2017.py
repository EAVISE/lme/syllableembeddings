import math
import os

import numpy as np
import pandas as pd
import torch
from scipy import stats

from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from pretrained_vecs.syllable_vecs import SyllableVecs
from config import Config, Lang
from tools.tools import Tools
from syllables.en_syllables2 import ENSyllables2
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2


class CombineSemEval2017:
    """
    A class to combine the official SemEval word pairs + scores into a single CSV for convenience, as the official
    distributions keeps these stored in separate files.
    """
    @staticmethod
    def parse_lang(lang: Lang):
        file_pairs = os.path.join(Config.EN_SEMEVAL_DIR, "test", "subtask1-monolingual", "data", f"{lang.value}.test.data.txt")
        file_scores = os.path.join(Config.EN_SEMEVAL_DIR, "test", "subtask1-monolingual", "keys", f"{lang.value}.test.gold.txt")

        combined = []
        with open(file_pairs, "r") as fin:
            for line in fin:
                pair = line.strip().split('\t')
                combined.append(pair)

        at_line = -1
        with open(file_scores, "r") as fin:
            for line in fin:
                at_line += 1
                score = float(line.strip())
                combined[at_line].append(score)

        df = pd.DataFrame(data=combined)
        file_out = os.path.join(Config.SEMEVAL_COMB_DIR, f"{lang.value}.csv")
        df.to_csv(path_or_buf=file_out, header=False, index=False)
        print(f"Combined result written to {file_out}")


class SemEval2017:
    def __init__(self, lang: Lang=Lang.EN):
        col_names = {0: "Word1", 1: "Word2", 2: "Score"}
        self.ref = pd.read_csv(os.path.join(Config.SEMEVAL_COMB_DIR, f"{lang.value}.csv"), header=None)
        self.ref.rename(columns=col_names, inplace=True)

        word1 = set(self.ref["Word1"].unique())
        word2 = set(self.ref["Word2"].unique())
        print(f"Loaded SemEval dataset for language '{lang.value}'...")
        self.words = word1.union(word2)  # Unique words

    def get_uq_words(self):
        return self.words

    def compute_score(self, wordvecs: dict, b_ignore_case=False, b_print_not_found=False, b_replace_whitespace=False):
        """
        For a given dictionary containing k, v pairs in the format: k = word, v = vector for word, compute the
        Spearman rank correlation between the human annotations contained in the SemEval dataset and the cosine
        similarities between the same word pairs as determined by the provided embeddings.

        The idea is of course that the provided dictionary contains embeddings for all words that are present in the
        SemEval dataset. Word pairs that can not be found in the dictionary will receive a cosine similarity score of 0.

        :param wordvecs:
        :param b_ignore_case:
        :param b_print_not_found: print to console those words present in SemEval for which no embeddings is present in
        the provided dictionary
        :param b_replace_whitespace: multi-word expressions are typically saved with whitespaces replaced by underscores
        for precomputed embeddings; if this boolean is set to "True", the scoring algorithm will replace whitespaces
        in reference multi-word expressions
        :return: tuple (Spearman correlation, Pearson correlation)
        """
        ref_vals = np.asarray(self.ref["Score"])

        pred_vals = []
        uq_words = set()
        not_found = set()
        for idx, row in self.ref.iterrows():
            word1 = row["Word1"]
            word2 = row["Word2"]
            uq_words.add(word1.lower())
            uq_words.add(word2.lower())
            if b_ignore_case:
                word1 = word1.lower()
                word2 = word2.lower()
            if b_replace_whitespace:
                word1 = word1.replace(' ', '_')
                word2 = word2.replace(' ', '_')
            if word1 in wordvecs and word2 in wordvecs:
                cs = Tools.cosim(wordvecs[word1], wordvecs[word2])
                if math.isnan(cs):
                    print(f"Nan value for: [{word1}] -- [{word2}]")
                    pred_vals.append(0.5)  # As suggested by SemEval2017 organization
                else:
                    pred_vals.append(cs)
            else:
                if word1 not in wordvecs:
                    not_found.add(word1)
                if word2 not in wordvecs:
                    not_found.add(word2)
                pred_vals.append(0.5)  # As suggested by SemEval2017 organization
        pred_vals = np.asarray(pred_vals)

        if b_print_not_found:
            for word in not_found:
                print(f"No vector for word: {word}")
        print(f"Missing words: {len(not_found)} of {len(uq_words)}.")

        s_corr = stats.spearmanr(ref_vals, pred_vals)
        # p_corr = 0  # stats.pearsonr(ref_vals, pred_vals)

        return s_corr, len(not_found)


if __name__ == '__main__':
    # Generate combined CSV files
    # for lang in Lang:
    #     if lang == Lang.NL:
    #         continue
    #     CombineSemEval2017.parse_lang(lang)
    lang = Lang.NL
    device = torch.device('cuda')

    sem = SemEval2017(lang=lang)
    for word in sorted(sem.words):
        print(word)
    exit()

    # Check ConceptNet performance on SemEval
    c_net = ConceptNetVecs(lang=lang)
    res = sem.compute_score(c_net.data, b_ignore_case=True, b_print_not_found=False, b_replace_whitespace=False)
    print(f"{lang.name} SemEval score for ConceptNet: {res[0]}")

    f_start_end = 0.
    if lang == Lang.EN:
        # sylls = ENSyllables(b_start=b_start, b_end=b_end)
        # sylls_split = ENSyllables2(overlap_with_vecs=c_net.data, f_start=f_start_end, f_end=f_start_end,
        #                            split_model_base='en_syll_splitter_transformer_em64_hid1x256_head8_full',
        #                            split_model_version='epoch4', device=device)
        sylls_no_split = ENSyllables2(overlap_with_vecs=c_net.data, f_start=f_start_end, f_end=f_start_end,
                                      split_model_base=None)
        syll_vecs = SyllableVecs(in_file=os.path.join(Config.PRETRAINED_VECS_DIR, "Paper",
                                                      "en2_vanilla_cptnet_final.dill"))
    elif lang == Lang.NL:
        # sylls = NLWiktionarySyllables(b_start=b_start, b_end=b_end)
        # sylls_split = NLWiktionarySyllables2(overlap_with_vecs=c_net.data, f_start=f_start_end, f_end=f_start_end,
        #                                      split_model_base='nl_syll_splitter_transformer_em64_hid1x256_head8_full',
        #                                      split_model_version='epoch4', device=device)
        sylls_no_split = NLWiktionarySyllables2(overlap_with_vecs=c_net.data, f_start=f_start_end, f_end=f_start_end,
                                                split_model_base=None)
        syll_vecs = SyllableVecs(in_file=os.path.join(Config.PRETRAINED_VECS_DIR, "Paper",
                                                      "nl2_vanilla_cptnet_start_end_final.dill"))

    # nl_syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "torch_norm_syll_vecs_final.dill"))
    # nl_syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "torch_norm_ext_syll_vecs_2epochs.dill"))
    # nl_syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "tulkens2016_wikipedia_1000000_final.dill"),
    #                             embed_size=320)

    syll_vecs_pred = {}  # Vectors from syllables
    for uq_word in sem.words:
        uq_word = uq_word.lower()

        sylls = sylls_no_split.get_syllabic_decomposition_for_word(uq_word)
        if sylls is None:
            print(f"No syllables for [{uq_word}]")
            continue

        # Check split doesn't contain weird shizzle
        concat_words = [''.join(sylls[i]) for i in range(len(sylls))]
        concat_word = ' '.join(concat_words).replace('#', '').replace('$', '')
        if concat_word != uq_word:
            print(f"!!! [{uq_word}] != [{concat_word}] !!!")
            continue

        vecs = []
        b_continue = False
        for e in sylls:
            vec = syll_vecs.get_sum_vec_for_sylls([e], b_norm=True)
            if vec is None:
                print(f"Vector is None for [{uq_word}] with syllabes: {sylls}")
                b_continue = True
                break
            vecs.append(vec)
        if b_continue:
            continue

        syll_vecs_pred[uq_word] = Tools.sum_vectors(vecs)

    res = sem.compute_score(syll_vecs_pred, b_ignore_case=True, b_print_not_found=True, b_replace_whitespace=False)
    print(res[0])
    print(res[1])
