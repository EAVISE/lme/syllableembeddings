# Compute Spearman rank correlation between human similarity annotations for pairs of words and
# the cosine similarities between word vectors
import math
from enum import Enum

from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from config import Config, Lang
from scipy import stats
import pandas as pd
import numpy as np

from tools.tools import Tools


class WS353Cols:
    SCORE = 'Human (mean)'
    WORD1 = 'Word 1'
    WORD2 = 'Word 2'


class WordSim353:
    def __init__(self, ws_file=Config.EN_WORDSIM353_FILE):
        self.ref = pd.read_csv(ws_file)
        word1 = set(self.ref[WS353Cols.WORD1].unique())
        word2 = set(self.ref[WS353Cols.WORD2].unique())
        print("Loaded WordSim dataset...")
        self.words = word1.union(word2)  # Unique words

        # print(f"Unique words: {len(words)}")
        # for word in sorted(words):
        #     print(word)

    def compute_score(self, wordvecs: dict, b_ignore_case=False, b_print_not_found=False):
        """
        For a given dictionary containing k, v pairs in the format: k = word, v = vector for word, compute the
        Spearman rank correlation between the human annotations contained in the WS-353 dataset and the cosine
        similarities between the same word pairs as determined by the provided embeddings.

        The idea is of course that the provided dictionary contains embeddings for all words that are present in the
        WS-353 dataset. Word pairs that can not be found in the dictionary will receive a cosine similarity score of 0.

        :param wordvecs:
        :param b_ignore_case:
        :param b_print_not_found: print to console those words present in WS353 for which no embeddings is present in
        the provided dictionary
        :return:
        """
        ref_vals = np.asarray(self.ref[WS353Cols.SCORE])

        pred_vals = []
        not_found = set()
        for idx, row in self.ref.iterrows():
            word1 = row[WS353Cols.WORD1]
            word2 = row[WS353Cols.WORD2]
            if b_ignore_case:
                word1 = word1.lower()
                word2 = word2.lower()
            if word1 in wordvecs and word2 in wordvecs:
                cs = Tools.cosim(wordvecs[word1], wordvecs[word2])
                if math.isnan(cs):
                    print(f"Nan value for: [{word1}] -- [{word2}]")
                    pred_vals.append(0.5)  # As suggested by SemEval2017 organization
                else:
                    pred_vals.append(cs)
            else:
                if word1 not in wordvecs:
                    not_found.add(word1)
                if word2 not in wordvecs:
                    not_found.add(word2)
                pred_vals.append(0.)
        pred_vals = np.asarray(pred_vals)

        if b_print_not_found:
            for word in not_found:
                print(f"No vector for word: {word}")
        print(f"Missing words: {len(not_found)}")

        return stats.spearmanr(ref_vals, pred_vals), len(not_found)


if __name__ == '__main__':
    eval_ws = WordSim353(ws_file=Config.NL_WORDSIM353_FILE)
    print(f"Number of unique words: {len(eval_ws.words)}")
    for word in sorted(eval_ws.words):
        print(word)
    exit()

    # c_net = ConceptNetVecs(lang=Lang.EN)
    # # syll_vecs = SyllableVecs()
    # res = eval_ws.compute_score(c_net.data, b_ignore_case=True, b_print_not_found=True)
    # print(res)
