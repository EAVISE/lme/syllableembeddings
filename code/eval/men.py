import os

import numpy as np
import pandas as pd
from scipy import stats

from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from pretrained_vecs.syllable_vecs import SyllableVecs
from config import Config, Lang
from syllables.en_syllables import ENSyllables
from tools.tools import Tools


class MEN:
    def __init__(self):
        col_names = {0: "Word1", 1: "Word2", 2: "Score"}
        self.ref = pd.read_csv(Config.EN_MEN_FILE, sep=' ', header=None)
        self.ref.rename(columns=col_names, inplace=True)

        word1 = set(self.ref["Word1"].unique())
        word2 = set(self.ref["Word2"].unique())
        print(f"Loaded MEN dataset for language 'en'...")
        self.words = word1.union(word2)  # Unique words

    def get_uq_words(self):
        return self.words

    def compute_score(self, wordvecs: dict, b_ignore_case=False, b_print_not_found=False, b_replace_whitespace=False):
        """
        For a given dictionary containing k, v pairs in the format: k = word, v = vector for word, compute the
        Spearman rank correlation between the human annotations contained in the SemEval dataset and the cosine
        similarities between the same word pairs as determined by the provided embeddings.

        The idea is of course that the provided dictionary contains embeddings for all words that are present in the
        SemEval dataset. Word pairs that can not be found in the dictionary will receive a cosine similarity score of 0.

        :param wordvecs:
        :param b_ignore_case:
        :param b_print_not_found: print to console those words present in SemEval for which no embeddings is present in
        the provided dictionary
        :param b_replace_whitespace: multi-word expressions are typically saved with whitespaces replaced by underscores
        for precomputed embeddings; if this boolean is set to "True", the scoring algorithm will replace whitespaces
        in reference multi-word expressions
        :return: tuple (Spearman correlation, Pearson correlation)
        """
        ref_vals = np.asarray(self.ref["Score"])

        pred_vals = []
        not_found = set()
        for idx, row in self.ref.iterrows():
            word1 = row["Word1"]
            word2 = row["Word2"]
            if b_ignore_case:
                word1 = word1.lower()
                word2 = word2.lower()
            if b_replace_whitespace:
                word1 = word1.replace(' ', '_')
                word2 = word2.replace(' ', '_')
            if word1 in wordvecs and word2 in wordvecs:
                pred_vals.append(Tools.cosim(wordvecs[word1], wordvecs[word2]))
            else:
                if word1 not in wordvecs:
                    not_found.add(word1)
                if word2 not in wordvecs:
                    not_found.add(word2)
                pred_vals.append(0.5)  # As suggested by SemEval2017 organization
        pred_vals = np.asarray(pred_vals)

        if b_print_not_found:
            for word in not_found:
                print(f"No vector for word: {word}")
        print(f"Missing words: {len(not_found)}")

        s_corr = stats.spearmanr(ref_vals, pred_vals)
        p_corr = 0  # stats.pearsonr(ref_vals, pred_vals)

        return s_corr, p_corr


if __name__ == '__main__':
    # Generate combined CSV files
    # for lang in Lang:
    #     if lang == Lang.NL:
    #         continue
    #     CombineSemEval2017.parse_lang(lang)

    ref = MEN()

    # Check ConceptNet performance on SemEval
    if True:
        c_net = ConceptNetVecs(lang=Lang.EN)
        print("EN MTurk score for ConceptNet:")
        res = ref.compute_score(c_net.data, b_ignore_case=True, b_print_not_found=False, b_replace_whitespace=False)
        print(res[0])
        print(res[1])

    en_sylls = ENSyllables()
    en_syll_vecs = SyllableVecs(in_file=os.path.join(Config.PRETRAINED_VECS_DIR,
                                                     "torch_norm_weights_en_syll_start_end_vecs_final.dill"))

    b_start = True  # Special token for staring syllable?
    b_end = True  # Special token for ending syllable?

    syll_vecs = {}  # Vectors from syllables
    no_vec = []
    for uq_word in ref.words:
        uq_word = uq_word.lower()

        sylls = en_sylls.get_syllabic_decomposition_for_word(uq_word, b_start=b_start, b_end=b_end,
                                                             b_ignore_plural=True)
        if sylls is None:
            print(f"No syllables for [{uq_word}]")
            no_vec.append(uq_word)
            continue
        vec = en_syll_vecs.get_sum_vec_for_sylls(sylls, b_norm=True)
        if vec is None:
            print(f"Vector is None for [{uq_word}] with syllabes: {sylls}")
            continue
        syll_vecs[uq_word] = vec

    res = ref.compute_score(syll_vecs, b_ignore_case=True, b_print_not_found=True, b_replace_whitespace=False)
    print(res[0])
    print(res[1])

    for word in no_vec:
        print(f"'{word}', ", end='')
