import numpy as np
import os
from sklearn.preprocessing import normalize

from eval.wordsim353 import WordSim353, WS353Cols
from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from pretrained_vecs.syllable_vecs import SyllableVecs
from pretrained_vecs.tulkens2016 import Tulkens2016, Embeddings
from config import Config, Lang
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2
from tools.tools import Tools
from syllables.nlwiktionary_syllables import NLWiktionarySyllables

f_start_end = 1.0
b_use_all_decomps = False  # Also compute score using all possible decompositions

nl_ws353 = WordSim353(Config().NL_WORDSIM353_FILE)
nl_vecs = ConceptNetVecs(lang=Lang.NL)
# nl_vecs = Tulkens2016(embeddings=Embeddings.WIKIPEDIA, nb_to_load=1000000)
nl_sylls = NLWiktionarySyllables2(overlap_with_vecs=nl_vecs.data, f_start=f_start_end, f_end=f_start_end)
# nl_syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "torch_norm_syll_vecs_final.dill"))
# nl_syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "torch_norm_ext_syll_vecs_2epochs.dill"))
# nl_syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR, "tulkens2016_wikipedia_1000000_final.dill"),
#                             embed_size=320)
nl_syll_vecs = SyllableVecs(in_file=os.path.join(Config.PRETRAINED_VECS_DIR, "Paper",
                                                 "nl2_vanilla_cptnet_start_end_final.dill"))

in_conceptnet = 0
in_wikt = 0
for uq_word in nl_ws353.words:
    if uq_word in nl_vecs.get_words():
        in_conceptnet += 1
    if uq_word in nl_sylls.get_words():
        in_wikt += 1
print(f"WS353 words found in ConceptNet : {in_conceptnet} out of {len(nl_ws353.words)}")
print(f"WS353 words found in NLWiktSylls: {in_wikt} out of {len(nl_ws353.words)}")

# scores = []
# for i, row in nl_ws353.ref.iterrows():
#     word1 = row[WS353Cols.WORD1]
#     word2 = row[WS353Cols.WORD2]
#     score = row[WS353Cols.SCORE]
#     w1_sylls = nl_sylls.get_syllables_for_word(word1)
#     w2_sylls = nl_sylls.get_syllables_for_word(word2)
#     if w1_sylls is None or w2_sylls is None:
#         scores.append(0)
#         continue
#     w1_sylls, w2_sylls = w1_sylls[0], w2_sylls[0]
#
#     w1_syll_vec = nl_syll_vecs.get_sum_vec_for_sylls(w1_sylls)
#     w2_syll_vec = nl_syll_vecs.get_sum_vec_for_sylls(w2_sylls)
#     print(Tools.cosim(w1_syll_vec, w2_syll_vec))

cn_vecs = {}  # ConceptNet vecs
syll_vecs = {}  # Vectors from syllables
for uq_word in nl_ws353.words:
    uq_word = uq_word.lower()

    cn_vec = nl_vecs.get_vec_for_word(uq_word)
    if cn_vec is not None:
        cn_vecs[uq_word] = cn_vec

    sylls = nl_sylls.get_syllabic_decomposition_for_word(uq_word)
    if sylls is None:
        print(f"No syllables for [{uq_word}]")
        continue
    vec = nl_syll_vecs.get_sum_vec_for_sylls(sylls, b_norm=True)
    if vec is None:
        print(f"Vector is None for [{uq_word}] with syllabes: {sylls}")
        continue
    syll_vecs[uq_word] = vec

print(nl_ws353.compute_score(syll_vecs, b_ignore_case=True))
print(nl_ws353.compute_score(cn_vecs, b_ignore_case=True))


# Using all found possible decompositions
if b_use_all_decomps:
    cn_vecs = {}  # ConceptNet vecs
    syll_vecs = {}  # Vectors from syllables
    for uq_word in nl_ws353.words:
        uq_word = uq_word.lower()

        cn_vec = nl_vecs.get_vec_for_word(uq_word)
        if cn_vec is not None:
            cn_vecs[uq_word] = cn_vec

        decomps = nl_sylls.get_all_syllabic_decompositions_for_word(uq_word)
        if decomps is None:
            print(f"No decompositions for [{uq_word}]")
            continue
        vecs = [nl_syll_vecs.get_sum_vec_for_sylls(decomp, b_norm=True) for decomp in decomps]

        if not vecs:
            print(f"Vector is None for [{uq_word}] with decomps\n{decomps}")
            continue

        vecs = [np.mean(vecs, axis=0)]
        vec = normalize(vecs, axis=1)

        syll_vecs[uq_word] = vec[0]

    print(nl_ws353.compute_score(syll_vecs, b_ignore_case=True))
    print(nl_ws353.compute_score(cn_vecs, b_ignore_case=True))
