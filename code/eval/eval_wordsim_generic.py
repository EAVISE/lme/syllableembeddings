import os

import numpy as np
import pandas as pd
import torch
from scipy import stats

from eval.men import MEN
from eval.mturk import MTurk
from eval.rare_words import RareWords
from eval.semeval2017 import SemEval2017
from eval.wordsim353 import WordSim353
from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from pretrained_vecs.syllable_vecs import SyllableVecs
from config import Lang, Config
from syllables.en_syllables import ENSyllables
from syllables.en_syllables2 import ENSyllables2
from syllables.nlwiktionary_syllables import NLWiktionarySyllables
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2
from tools.tools import Tools


def compute_score(ref: pd.DataFrame, wordvecs: dict, b_ignore_case=False, b_print_not_found=False,
                  b_replace_whitespace=False):
    """
    For a given dictionary containing k, v pairs in the format: k = word, v = vector for word, compute the
    Spearman rank correlation between the human annotations contained in the SemEval dataset and the cosine
    similarities between the same word pairs as determined by the provided embeddings.

    The idea is of course that the provided dictionary contains embeddings for all words that are present in the
    SemEval dataset. Word pairs that can not be found in the dictionary will receive a cosine similarity score of 0.

    :param ref:
    :param wordvecs:
    :param b_ignore_case:
    :param b_print_not_found: print to console those words present in SemEval for which no embeddings is present in
    the provided dictionary
    :param b_replace_whitespace: multi-word expressions are typically saved with whitespaces replaced by underscores
    for precomputed embeddings; if this boolean is set to "True", the scoring algorithm will replace whitespaces
    in reference multi-word expressions
    :return: tuple (Spearman correlation, Pearson correlation)
    """
    ref_vals = np.asarray(ref["Score"])

    pred_vals = []
    not_found = set()
    for idx, row in ref.iterrows():
        word1 = row["Word1"]
        word2 = row["Word2"]
        if b_ignore_case:
            word1 = word1.lower()
            word2 = word2.lower()
        if b_replace_whitespace:
            word1 = word1.replace(' ', '_')
            word2 = word2.replace(' ', '_')
        if word1 in wordvecs and word2 in wordvecs:
            pred_vals.append(Tools.cosim(wordvecs[word1], wordvecs[word2]))
        else:
            if word1 not in wordvecs:
                not_found.add(word1)
            if word2 not in wordvecs:
                not_found.add(word2)
            pred_vals.append(0.5)  # As suggested by SemEval2017 organization
    pred_vals = np.asarray(pred_vals)

    if b_print_not_found:
        for word in not_found:
            print(f"No vector for word: {word}")
        print(f"Missing words: {len(not_found)}")

    s_corr = stats.spearmanr(ref_vals, pred_vals)
    p_corr = stats.pearsonr(ref_vals, pred_vals)

    return s_corr, p_corr


lang = Lang.EN

# ref = MEN()
# ref = MTurk()
# ref = RareWords()
ref = SemEval2017(lang=lang)
# ref = WordSim353(ws_file=RadixAIConfig.EN_WORDSIM353_FILE if lang == Lang.EN else
#                  RadixAIConfig.NL_WORDSIM353_FILE)

# Reference
c_net = ConceptNetVecs(lang=lang)
print("Reference score for ConceptNet vectors:")
res = ref.compute_score(c_net.data, b_ignore_case=True, b_print_not_found=False)
print(res[0])
# print(res[1])

device = torch.device('cpu')
b_start = False  # Special token for starting syllable?
b_end = False  # Special token for ending syllable?
b_skip_space = False
f_start_end = .25
if lang == Lang.EN:
    # sylls = ENSyllables(b_start=b_start, b_end=b_end)
    sylls = ENSyllables2(overlap_with_vecs=c_net.data, f_start=f_start_end, f_end=f_start_end,
                         split_model_base=None)
                         # split_model_base='en_syll_splitter_transformer_em64_hid1x256_head8_full',
                         # split_model_version='epoch4', device=device)
elif lang == Lang.NL:
    # sylls = NLWiktionarySyllables(b_start=b_start, b_end=b_end)
    sylls = NLWiktionarySyllables2(overlap_with_vecs=c_net.data, f_start=f_start_end, f_end=f_start_end,
                                   split_model_base=None)
                                   # split_model_base='nl_syll_splitter_transformer_em64_hid1x256_head8_full',
                                   # split_model_version='epoch4', device=device)
# syll_vecs = SyllableVecs(in_file=os.path.join(RadixAIConfig.PRETRAINED_VECS_DIR,
#                                                  "torch_norm_weights_en_syll_start_end_vecs_final.dill"))

# Import is necessary for loading of model
# from models.nn.transformer.transformer_syllable_encoder import TransformerSyllableEncoder
from models.nn.transformer.trans_syll_encoder_2 import TransformerSyllableEncoder2
from models.nn.transformer.trans_syll_encoder_3 import TransformerSyllableEncoder3
from models.nn.transformer.trans_syll_encoder_4 import TransformerSyllableEncoder4
if lang == Lang.EN:
    # syll_compounder = torch.load(open(os.path.join(RadixAIConfig.MODELS_DIR, 'Paper',
    #                                                'en_att_cpnet_start_end_final.pt'), 'rb'))
    syll_compounder = torch.load(open(os.path.join(Config.MODELS_DIR, 'Paper',
                                                   'en_att4_cpnet_emd200_sten25pc_final.pt'), 'rb'),
                                 map_location=device)
elif lang == Lang.NL:
    # syll_compounder = torch.load(open(os.path.join(RadixAIConfig.MODELS_DIR, 'Paper',
    #                                                'nl_att_cpnet_start_end_final.pt'), 'rb'))
    syll_compounder = torch.load(open(os.path.join(Config.MODELS_DIR, 'Paper',
                                                   'nl_att2_cpnet_sten25pc_final.pt'), 'rb'),
                                 map_location=device)

syll_compounder.eval()
nb_params = sum(p.numel() for p in syll_compounder.parameters())
print(f"#Model parameters: {nb_params}")

syll_vecs = {}  # Vectors from syllables
no_vec = []
skip_space = 0
for uq_word in ref.words:
    uq_word = uq_word.lower()

    if b_skip_space and ' ' in uq_word:
        skip_space += 1
        continue

    syll_decomp = sylls.get_syllabic_decomposition_for_word(uq_word,  # .replace("-", " "),
                                                            b_start=b_start, b_end=b_end, b_ignore_plural=False)
    # print(syll_decomp)

    if syll_decomp is None:
        print(f"No syllables for [{uq_word}]")
        no_vec.append(uq_word)
        continue
    # vec = en_syll_vecs.get_sum_vec_for_sylls(sylls, b_norm=True)
    syll_decomp = [sylls.decomp_to_idx(s) for s in syll_decomp]
    if sum(-1 in e for e in syll_decomp):
        print(f"Word [{uq_word}]: Split contains unknown syllables")
        continue
    vecs = []
    b_continue = False
    for e in syll_decomp:
        src = torch.Tensor(e).unsqueeze(0).long().to(device)
        # print(f"src.shape: {src.shape}")
        # print(syll_compounder(src, b_debug=False).shape)
        vec = syll_compounder(src).view(-1).detach().cpu().numpy()
        if vec is None:
            print(f"Vector is None for [{uq_word}] with syllabes: {sylls}")
            b_continue = True
            break
        else:
            vecs.append(vec)
    if b_continue:
        continue

    syll_vecs[uq_word] = Tools.sum_vectors(vecs)

print(f"Skip space: {skip_space}")

res, nb_not_found = ref.compute_score(syll_vecs, b_ignore_case=True, b_print_not_found=True)  # , b_replace_whitespace=False)
print(f"Spearman rank: {res[0]}")
print(f"p-value: {res[1]}")

for word in no_vec:
    print(f"'{word}', ", end='')
