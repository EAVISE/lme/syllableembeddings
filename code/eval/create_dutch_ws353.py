""" Simple helper class that will create a Dutch version of the Sim353 dataset from the translations of the individual
words.
The problem is I translated the unique words in the WS-353 dataset, but hence lost the context because I didn't know
what pair the words appeared in. This makes that several words have several possible translations. This script simply
'translated the pairs' by using the mapping consisting of the unique word translations, to allow pruning of the
possible translations."""
import os

from config import Config
from eval.wordsim353 import WordSim353

ws353 = WordSim353()

uq_trans_file = os.path.join(Config().REFDATA_DIR, 'WordSimilarity', 'wordsim353-nl', 'WS353_UqWords_NL_Translation.csv')
uq_trans = {}
with open(uq_trans_file, 'r') as fin:
    fin.readline()  # Skip first line
    for line in fin:
        line = line.strip().split(',', 1)
        uq_trans[line[0]] = line[1].rstrip(',')

with open(os.path.join(Config().REFDATA_DIR, 'WordSimilarity', 'wordsim353-nl', 'WS353_EN_vs_NL.csv'), 'w') as fout:
    for i, row in ws353.ref.iterrows():
        word1 = row["Word 1"]
        word2 = row["Word 2"]
        fout.write(f'{row["Human (mean)"]},{word1},{word2},-->,{uq_trans[word1]},:,{uq_trans[word2]}\n')

