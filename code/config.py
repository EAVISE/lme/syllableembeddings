import os
from enum import Enum

import torch


class Lang(Enum):
    DE = "de"
    EN = "en"
    ES = "es"
    FA = "fa"
    IT = "it"
    NL = "nl"


class Config:
    USE_CUDA = torch.cuda.is_available()
    DEVICE = torch.device("cuda" if USE_CUDA else "cpu")
    HOME = os.path.expanduser("~")

    DATA_DIR = os.path.join(HOME, "Work", "Projects", "STDL", "Data", "RadixAI")
    MODELS_DIR = os.path.join(DATA_DIR, "Models")
    REFDATA_DIR = os.path.join(DATA_DIR, "ReferenceDataSets")
    TEMP_DIR = os.path.join(DATA_DIR, "Temp")

    # MEN
    EN_MEN_FILE = os.path.join(REFDATA_DIR, "WordSimilarity", "MEN", "MEN_dataset_natural_form_full")

    # MTurk
    EN_MTURK_FILE = os.path.join(REFDATA_DIR, "WordSimilarity", "MTurk", "Mturk.csv")

    # Rare Words
    EN_RW_FILE = os.path.join(REFDATA_DIR, "WordSimilarity", "RareWords", "rw.txt")

    # SemEval
    SEMEVAL_DIR = os.path.join(REFDATA_DIR, "SemEval2017")
    SEMEVAL_COMB_DIR = os.path.join(SEMEVAL_DIR, "Combined")
    EN_SEMEVAL_DIR = os.path.join(SEMEVAL_DIR, "Original")
    NL_SEMEVAL_DIR = os.path.join(SEMEVAL_DIR, "Dutch")

    # WordSim353
    EN_WORDSIM353_FILE = os.path.join(REFDATA_DIR, "WordSimilarity", "wordsim353", "combined.csv")
    NL_WORDSIM353_FILE = os.path.join(REFDATA_DIR, "WordSimilarity", "wordsim353-nl", "WS353_NL.csv")

    # Wikipedia dumps
    WIKIPEDIA_DIR = os.path.join(DATA_DIR, "WikiDump")
    WIKIPEDIA_EN_DUMP = os.path.join(WIKIPEDIA_DIR, "enwiki-20191020-pages-articles-multistream.xml.bz2")
    WIKI_PREPROCESS_DIR = os.path.join(DATA_DIR, "WikiPreProcess")
    WIKTIONARY_NL_DUMP = os.path.join(WIKIPEDIA_DIR, "nlwiktionary-20191101-pages-articles-multistream.xml.bz2")
    WIKTIONARY_DE_DUMP = os.path.join(WIKIPEDIA_DIR, "dewiktionary-20200101-pages-articles-multistream.xml.bz2")

    # Pretrained embeddings
    PRETRAINED_VECS_DIR = os.path.join(DATA_DIR, "PretrainedVecs")
    FASTTEXT_VECS = os.path.join(PRETRAINED_VECS_DIR, "wiki-news-300d-1M.vec")
    CONCEPTNET_VECS = os.path.join(PRETRAINED_VECS_DIR, "numberbatch-19.08.txt.gz")
    # CONCEPTNET_NL_VECS = os.path.join(PRETRAINED_VECS_DIR, "numberbatch_nl.txt.gz")
    TULKENS2016_DIR = os.path.join(PRETRAINED_VECS_DIR, "Tulkens2016")
