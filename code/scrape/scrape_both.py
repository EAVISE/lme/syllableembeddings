"""
A scraper that switches between http://www.syllablecount.com/ and https://www.howmanysyllables.com/ to allow faster
scraping.
"""
import os
from enum import Enum
from time import sleep
from urllib import request
from urllib.error import URLError

import numpy as np
from bs4 import BeautifulSoup as bs4

from config import Config


class BaseURL(Enum):
    SC = "http://www.syllablecount.com/syllables/"
    HMS = "https://www.howmanysyllables.com/words/"


class ScrapeBoth:
    def __init__(self):
        self.out_file = os.path.join(Config.DATA_DIR, "en_syllables.txt")
        self.pass_file = os.path.join(Config.DATA_DIR, "en_no_syllables.txt")
        self.parsed_words, self.pass_words = set(), set()
        # Check we're not starting with an emtpy file
        if os.path.isfile(self.out_file):
            with open(self.out_file, 'r') as fin:
                for line in fin:
                    if not line:
                        continue
                    parts = line.strip().split('\t')
                    self.parsed_words.add(parts[0])
        if os.path.isfile(self.pass_file):
            with open(self.pass_file, 'r') as fin:
                for line in fin:
                    if not line:
                        continue
                    self.pass_words.add(line.strip())

    def get_syllables_for_word(self, word: str, base_url: BaseURL):
        """

        :param word: the words to parse
        :param base_url
        :return:
        """
        if word in self.parsed_words:
            print(f"Word [{word}] has already been parsed, skipping...")
            return 0
        elif word in self.pass_words:
            print(f"Word [{word}] is not known by website, skipping...")
            return 0

        url = f"{base_url.value}{word}"
        print(f"Scraping [{url}]...", end='', flush=True)

        # <p id="ctl00_ContentPane_paragraphtext2" style="font-size: x-large">Divide afterwards into syllables: <b style='color: #008000'>af-ter-wards</b></p>
        req = request.Request(
            url,
            data=None,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        res = request.urlopen(req, timeout=10)
        soup = bs4(res.read(), features='lxml')
        try:
            if base_url == BaseURL.SC:
                syllables = soup.find('p', {'id': 'ctl00_ContentPane_paragraphtext2'}).b.contents[0]
            elif base_url == BaseURL.HMS:
                syllables = soup.find('p', {'id': 'SyllableContentContainer'}).span.contents[0]
            else:
                raise ValueError(f"Parsing not implemented for BaseURL {base_url}.")
            with open(self.out_file, 'a') as fout:
                fout.write(f"{word}\t{syllables}\n")

            print(" done.")
            return 1
        except AttributeError as e:
            self.pass_words.add(word)
            with open(self.pass_file, 'a') as fout:
                fout.write(f"{word}\n")
            print(f"No splits appear to be known for word [{word}]...")
            return -1


if __name__ == '__main__':
    # Load n most frequent words, according to FastText embeddings
    def load_words(fname=Config.FASTTEXT_VECS, nb_to_load=-1):
        # Load vectors, adapted from https://fasttext.cc/docs/en/english-vectors.html
        with open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore') as fin:
            n, d = map(int, fin.readline().split())
            words = []  # Track as a list to keep frequency information
            _words = set()  # But also keep a set, for efficient checking if lowercase version was already seen

            nb_loaded = 0
            for line in fin:
                tokens = line.rstrip().split(' ')
                # Skip non-alfa tokens
                if not str.isalpha(tokens[0]):
                    continue
                word = tokens[0].lower()
                if word in _words:
                    continue
                words.append(word)
                _words.add(word)
                nb_loaded += 1
                if nb_loaded == nb_to_load:
                    break

        return words

    words = load_words(nb_to_load=200000)

    time_between_calls = 5
    noise = 2.5
    bias = 1.25

    ignore = {'à', 'bin', 'josé', 'françois', 'andré', 'pokémon'}
    scraper = ScrapeBoth()
    at_word = 0
    new_words = 0
    for word in words:
        at_word += 1
        if word in ignore:
            continue
        try:
            word.encode(encoding='utf-8').decode('ascii')
        except UnicodeDecodeError as e:
            continue
        # Method returns 0 if word was already known, 1 if scrape was successfull, -1 otherwise
        try:
            if at_word % 2 == 1:
                _base_url = BaseURL.SC
            else:
                _base_url = BaseURL.HMS
            call_res = scraper.get_syllables_for_word(word, base_url=_base_url)
            if call_res != 0:
                if call_res == 1:
                    new_words += 1
                sleep_time = time_between_calls + (np.random.rand() * noise - bias)
                print(f"New words so far: {new_words}. Going to sleep for {sleep_time} seconds...")
                sleep(time_between_calls)
        except Exception as e:
            if isinstance(e, URLError):
                print(f"URLError:\n{e}")
                print(f"Going to sleep for {60} seconds...")
                sleep(60)
            else:
                print(f"Error class: {e.__class__.__name__}\n{e}")
                print(f"Going to sleep for {10} seconds...")
                sleep(10)
