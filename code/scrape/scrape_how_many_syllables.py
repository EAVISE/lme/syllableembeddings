"""
A scraper for the How Many Syllables website: https://www.howmanysyllables.com/
"""
import os
from time import sleep
from urllib import request
from urllib.error import URLError

import numpy as np
from bs4 import BeautifulSoup as bs4

from config import Config


class ScrapeHowManySyllables:
    def __init__(self):
        self.base_url = "https://www.howmanysyllables.com/words/"
        self.out_file = os.path.join(Config.DATA_DIR, "en_syllables.txt")
        self.pass_file = os.path.join(Config.DATA_DIR, "en_no_syllables.txt")
        self.parsed_words, self.pass_words = set(), set()
        # Check we're not starting with an emtpy file
        if os.path.isfile(self.out_file):
            with open(self.out_file, 'r') as fin:
                for line in fin:
                    if not line:
                        continue
                    parts = line.strip().split('\t')
                    self.parsed_words.add(parts[0])
        if os.path.isfile(self.pass_file):
            with open(self.pass_file, 'r') as fin:
                for line in fin:
                    if not line:
                        continue
                    self.pass_words.add(line.strip())

    def get_syllables_for_word(self, word: str):
        """

        :param word: the words to parse
        :return:
        """
        if word in self.parsed_words:
            print(f"Word [{word}] has arleady been parsed, skipping...")
            return 0
        # elif word in self.pass_words:
        #     print(f"Word [{word}] is not known by website, skipping...")
        #     return 0

        url = f"{self.base_url}{word}"
        print(f"Scraping [{url}]...", end='', flush=True)

        # <p id="ctl00_ContentPane_paragraphtext2" style="font-size: x-large">Divide afterwards into syllables: <b style='color: #008000'>af-ter-wards</b></p>
        req = request.Request(
            url,
            data=None,
            headers={
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
            }
        )
        res = request.urlopen(req, timeout=10)
        soup = bs4(res.read(), features='lxml')
        try:
            syllables = soup.find('p', {'id': 'SyllableContentContainer'}).span.contents[0]
            with open(self.out_file, 'a') as fout:
                fout.write(f"{word}\t{syllables}\n")

            print(" done.")
            return 1
        except AttributeError as e:
            self.pass_words.add(word)
            # with open(self.pass_file, 'a') as fout:
            #     fout.write(f"{word}\n")
            print(f"No splits appear to be known for word [{word}]...")
            return -1


if __name__ == '__main__':
    # Load n most frequent words, according to FastText embeddings
    def load_words(fname=Config.FASTTEXT_VECS, nb_to_load=-1):
        # Load vectors, adapted from https://fasttext.cc/docs/en/english-vectors.html
        with open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore') as fin:
            n, d = map(int, fin.readline().split())
            words = []  # Track as a list to keep frequency information
            _words = set()  # But also keep a set, for efficient checking if lowercase version was already seen

            nb_loaded = 0
            for line in fin:
                tokens = line.rstrip().split(' ')
                # Skip non-alfa tokens
                if not str.isalpha(tokens[0]):
                    continue
                word = tokens[0].lower()
                if word in _words:
                    continue
                words.append(word)
                _words.add(word)
                nb_loaded += 1
                if nb_loaded == nb_to_load:
                    break

        return words

    words = load_words(nb_to_load=100000)

    time_between_calls = 10
    noise = 5
    bias = 2.5

    ignore = {'à', 'bin', 'josé', 'françois', 'andré', 'pokémon'}
    scraper = ScrapeHowManySyllables()
    # for word in words:
    for word in ['slaughterers', 'picket', 'communicativeness', 'unionise', 'preconceptions', 'transmigrating', 'perceptible', 'canker', 'feminised', 'propagate', 'partible', 'splashy', 'humanness', 'inanimate', 'reelections', 'corpulence', 'concurrencies', 'ordain', 'piety', 'restrainer', 'disjoined', 'autocracy', 'pathfinders', 'discriminate', 'braid', 'shrewdness', 'princedom', 'deforming', 'fluidity', 'emotionalism', 'largeness', 'boastful', 'recourse', 'transgress', 'continence', 'globalise', 'vindictively', 'cuteness', 'needlepoint', 'coiled', 'scampering', 'brood', 'spaciousness', 'intermingles', 'exterminator', 'unimpressed', 'quadratics', 'repositions', 'soulfully', 'craftsman', 'enslaves', 'commingled', 'microcomputers', 'yellowish', 'naturalise', 'condensing', 'publicise', 'animalism', 'rompers', 'objector', 'gumption', 'snooper', 'swooshing', 'splice', 'caesarism', 'rematches', 'personifying', 'impermanent', 'deconstruct', 'submariners', 'containerful', 'forceps', 'microbiologist', 'convene', 'ceaseless', 'virologist', 'autobiographies', 'enlist', 'undefinable', 'docile', 'enthuse', 'blunders', 'spatiality', 'imperils', 'corruptive', 'circumnavigations', 'pervert', 'stump', 'calmness', 'plucked', 'princedoms', 'ringer', 'crosswise', 'lubricate', 'unmarketable', 'protesters', 'idiocy', 'mistrustful', 'solemnity', 'nurturance', 'enlarger', 'withhold', 'reassessments', 'autoimmune', 'spellers', 'inconsiderate', 'obstruct', 'flighted', 'severer', 'unaccessible', 'abstractionist', 'incommutable', 'benefactor', 'subtend', 'incurved', 'confide', 'embroideries', 'disgorge', 'blitzed', 'campfires', 'colonise', 'galvanic', 'rustic', 'imperfection', 'amounted', 'leagued', 'retrace', 'paparazzo', 'squishing', 'embellishment', 'quicken', 'untilled', 'sniffers', 'antedating', 'chairmanship', 'unskillfulness', 'employable', 'admittance', 'corral', 'nonviable', 'oppress', 'irremovable', 'clownish', 'cosponsoring', 'defiant', 'conflagration', 'galvanize', 'brightly', 'rehashing', 'lastingly', 'insurrectional', 'sophisticate', 'disinflation', 'tenderize', 'undisputable', 'extrasensory', 'translunar', 'houseful', 'self-discovery', 'insure', 'dispersive', 'earmuffs', 'naivety', 'macroeconomist', 'interspecies', 'consigning', 'protectiveness', 'autografts', 'jarringly', 'bewitchment', 'reordering', 'repress', 'improver', 'eventful', 'imitation', 'transmissible', 'subtropical', 'preheating', 'harden', 'representable', 'loveless', 'condescended', 'sidewinder', 'subjugate', 'autofocus', 'pittance', 'unicycling', 'typify', 'devalue', 'purchasable', 'impeded', 'exacted', 'algebraic', 'hydrochloride', 'illiberal', 'macroevolution', 'endurable', 'monotype', 'torment', 'prehistorical', 'sanctifying', 'directionless', 'abashed', 'antitoxic', 'knifing', 'newness', 'glittery', 'prudery', 'combust', 'microwaving', 'inessential', 'hospitalize', 'unrepeatable', 'elated', 'inheritor', 'populate', 'congruity', 'unintelligent', 'dooming', 'irritatingly', 'curvature', 'defeatist', 'impoliteness', 'infolding', 'corrode', 'antipsychotics', 'contravened', 'internee', 'immeasurable', 'recitalist', 'binging', 'classicist', 'heraldist', 'autoregulation', 'attested', 'rumbled', 'preposed', 'unmentionable', 'postboxes', 'disgruntle', 'guillotine', 'lavishness', 'kingship', 'imperceptible', 'malevolence', 'invigorating', 'frighten', 'enchantress', 'hover', 'sorcery', 'sociable', 'deceitful', 'narrow-minded', 'expressionless', 'carbonic', 'internationalisms', 'suggestible', 'defrayed', 'suppressor', 'inversions', 'hypertext', 'interplanetary', 'tailgate', 'unannounced', 'excitation', 'ripeness', 'monarchist', 'paradoxical', 'autopilots', 'anterooms', 'irrelevance', 'unheralded', 'crouch', 'unconcerned', 'pave', 'breather', 'bleach', 'confluent', 'rite', 'boldness', 'antechamber', 'inclosure', 'scandalize', 'eavesdropper', 'irreproducible', 'insecurities', 'titillated', 'acquisitive', 'excitedly', 'unshaped', 'shrieks', 'inapplicability', 'connoting', 'unapproachable', 'motherless', 'triclinic', 'envelop', 'discourteous', 'unarguable', 'shouter', 'facilitation', 'autoerotic', 'astronautical', 'immobilization', 'tolerable', 'monogenesis', 'expensiveness', 'gluttonous', 'seafaring', 'equatorial', 'subdivided', 'literalness', 'undatable', 'displeased', 'astringe', 'buggered', 'detestable', 'devilish', 'tricolours', 'reproducible', 'unostentatious', 'backwardness', 'implicational', 'concerto', 'omnipotence', 'exclaiming', 'mildness', 'containership', 'synthetical', 'internationaler', 'rhymers', 'traditionalism', 'balminess', 'unobjectionable', 'capitalised', 'carburettors', 'soulless', 'reprocessing', 'attractor', 'wingless', 'disapproving', 'unwaveringly', 'reclassifications', 'ungraceful', 'circumventing', 'masculinity', 'crispness', 'disinheritance', 'verbalize', 'tricolor', 'attributable', 'enunciated', 'mercifulness', 'immobile', 'defecation', 'composure', 'discolor', 'comportment', 'automate', 'sympathized', 'homoerotic', 'worsens', 'cofounder', 'manacles', 'uncommunicative', 'arouse', 'unspecialised', 'demureness', 'huffy', 'indifferently', 'undress', 'trilogies', 'scarcity', 'omniscience', 'internationality', 'roosted', 'interweaved', 'coerce', 'constancy', 'subarctic', 'autobiographer', 'venders', 'comprehensible', 'retraction', 'frivolously', 'heterosexism', 'portrayer', 'transubstantiate', 'cofounders', 'odorize', 'nonindulgent', 'choke', 'pedaler', 'psychodynamics', 'impossibilities', 'clamorous', 'civilise', 'intragroup', 'inadvertence', 'cosigned', 'transfuse', 'laud', 'muscularity', 'unbroken', 'unilateralist', 'imprecise', 'ceremonious', 'adhesion', 'crudeness', 'loveable', 'sociability', 'tasteless', 'brandish', 'codefendants', 'hallucinating', 'intertwining', 'sponge', 'impurity', 'autoimmunity', 'stockers', 'undated', 'penitent', 'hilariously', 'nanometer', 'deface', 'viscometry', 'inflection', 'microbalance', 'unformed', 'rearrangements', 'reprehensible', 'supplanting', 'regionalisms', 'postponements', 'rattlesnake', 'smallish', 'pretense', 'laureate', 'unquenchable', 'antonymous', 'concordance', 'primacy', 'nonverbally', 'cardinality', 'subdivide', 'extort', 'underprivileged', 'incommensurable', 'assimilate', 'circumference', 'ascendence', 'blackmailed', 'blithering', 'impregnate', 'cynically', 'parallelize', 'nonpartisan', 'hyperlinks', 'extraterrestrials', 'crier', 'apparition', 'tripods', 'hush', 'experimenter', 'entreaty', 'flightless', 'insecureness', 'postposition', 'interconnectedness', 'cross-index', 'bootless', 'inelegance', 'unenthusiastic', 'untracked', 'internationalize', 'lightship', 'whizzed', 'circumscribed', 'append', 'knightly', 'incalculable', 'deprive', 'detectable', 'evangelize', 'microvolts', 'spacewalker', 'liveable', 'bastardize', 'canonize', 'inquisitive', 'obviousness', 'constrict', 'recycle', 'socialites', 'postcode', 'transvestitism', 'marginality', 'color-blind', 'preordained', 'contraries', 'pressurise', 'virility', 'procreated', 'comfortless', 'intramuscular', 'postmodernist', 'roadless', 'halfhearted', 'inflicted', 'careerism', 'multidimensional', 'dysentery', 'carbonate', 'companionships', 'criticality', 'headship', 'randomize', 'embody', 'mercantile', 'greenness', 'revivalism', 'incombustible', 'ropewalker', 'expressible', 'entrapping', 'preschoolers', 'antifeminist', 'impressionable', 'glistens', 'maildrop', 'entraps', 'pathfinder', 'assay', 'immigrate', 'protraction', 'commodes', 'extraterrestrial', 'brainless', 'yodeling', 'immoderate', 'microflora', 'deregulating', 'regretful', 'transducers', 'tricycle', 'combatted', 'incongruous', 'commingle', 'inelasticity', 'scowl', 'squirt', 'autograft', 'unforeseen', 'negociate', 'reviles', 'besieging', 'nonpolitical', 'vocalism', 'exterminated', 'contortionists', 'heiress', 'instructorship', 'valorous', 'reprints', 'interceptor', 'belligerence', 'translocating', 'implantations', 'chromatic', 'conformism', 'intercession', 'posthole', 'revolutionise', 'communicator', 'stroked', 'flavourful', 'undissolved', 're-create', 'converse', 'impassively', 'federalize', 'intermarry', 'uncreative', 'cuddle', 'objectifying', 'inarticulate', 'americanize', 'inabilities', 'indirectness', 'homophobia', 'heedless', 'expound', 'opalescence', 'worsen', 'unceremonious', 'disorderly', 'wrestle', 'deserters', 'bellowing', 'objectify', 'rectorate', 'antifeminism', 'businessperson', 'impotently', 'ulcerate', 'individualize', 'fortitude', 'trioxide', 'seductive', 'excommunicate', 'unloving', 'repel', 'brace', 'royalist', 'protrusion', 'coeducation', 'hyperextension', 'thinness', 'pessimist', 'skater', 'slurred', 'enunciates', 'seasonable', 'untroubled', 'rejoinders', 'memorialize', 'prideful', 'religiousness', 'entrust', 'edgeless', 'dissociable', 'immoveable', 'cosigns', 'freshen', 'conversely', 'summonings', 'circumvent', 'nondescripts', 'disengages', 'bounced', 'monograms', 'copulate', 'combusting', 'precociously', 'wheaten', 'immortalize', 'virginals', 'incontrovertible', 'scar', 'subgroup', 'stoical', 'irregardless', 'gelatinous', 'refered', 'improvise', 'clamor', 'witch-hunt', 'incubate', 'dematerialised', 'anamorphosis', 'estrogenic', 'discipleship', 'overlying', 'puritanism', 'unforgiving', 'deceiver', 'continuance', 'exchangeable', 'exclamation', 'obstructive', 'deposes', 'specialism', 'sandbag', 'evangelistic', 'innocuous', 'unreal', 'headless', 'convocation', 'circumscribes', 'extinguish', 'unreserved', 'nonstandard', 'distributive', 'cognizance', 'destabilization', 'embroiderer', 'bunking', 'workman', 'personify', 'perfectible', 'decapitated', 'enliven', 'circumvents', 'suspiciousness', 'monopolist', 'asphaltic', 'inducement', 'protestantism', 'aspirate', 'chronologize', 'fictitiously', 'cooperator', 'portioned', 'caliper', 'outlawed', 'monoclinic', 'vegetational', 'freighter', 'macroeconomists', 'promotive', 'representational', 'disembodied', 'winking', 'repulses', 'passable', 'normalise', 'transfigure', 'directional', 'repurchases', 'disassociates', 'interned', 'parallelism', 'homogenized', 'embracement', 'conjurors', 'submerging', 'wreathe', 'covariant', 'cooperators', 'transalpine', 'slanderous', 'extravert', 'cylindrical', 'nonperformance', 'inscribe', 'uproarious', 'methodically', 'lordship', 'delimited', 'preadolescent', 'deduce', 'tumble', 'contagious', 'amethysts', 'gracefulness', 'incoordination', 'airship', 'digitise', 'skateboarders', 'transposable', 'unfit', 'totalism', 'steepen', 'procurator', 'uncomprehending', 'expel', 'separationist', 'unvariedness', 'reenactor', 'fabricate', 'behavioural', 'parasitical', 'postdates', 'dissenters', 'goldplated', 'refurbishments', 'militarize', 'instruct', 'interlingua', 'sermonize', 'hypercoaster', 'bendability', 'conscripting', 'organismal', 'autographic', 'seeders', 'ganging', 'widen', 'cockerel', 'corroding', 'subsurface', 'photocopy', 'nonconformist', 'up-to-date', 'hard-and-fast', 'trichloride', 'unmanned', 'preassembled', 'anticancer', 'trespass', 'spiritize', 'arousal', 'prayerful', 'utterance', 'fascinate', 'hypersensitivity', 'equip', 'scarceness', 'flaunt', 'burying', 'papered', 'heater', 'walloper', 'omnipotent', 'puffery', 'baptistic', 'gringo', 'boisterously', 'dissatisfying', 'interjection', 'archery', 'contrive', 'freakishly', 'voraciously', 'animality', 'talkativeness', 'predominance', 'suppleness', 'delimitations', 'preheated', 'asteroidal', 'transfused', 'lenience', 'calcify', 'kindergarteners', 'symmetrical', 'schemer', 're-argue', 'absconding', 'undeviating', 'discharged', 'languishing', 'microorganism', 'latinist', 'bluejacket', 'provisionally', 'warmness', 'democratize', 'deformity', 'circumnavigate', 'extraversion', 'purgatory', 'inquisitor', 'spiritualist', 'deadness', 'congeniality', 'unknowing', 'extractor', 'malfeasance', 'foreclosed', 'despoil', 'manifestation', 'discernment', 'incensing', 'postglacial', 'standardize', 'unloved', 'concurrence', 'dispossess', 'dimensional', 'contrastive', 'contravene', 'rubberstamp', 'cliffhanger', 'tenured', 'snickering', 'anaesthetics', 'unchaste', 'migrational', 'self-improvement', 'catalogued', 'abnormality', 'disfigure', 'extrapolations', 'undefined', 'disprove', 'synchronic', 'outfoxed', 'intramolecular', 'dissonance', 'annoy', 'illimitable', 'excrete', 'unrewarding', 'algebraist', 'deceive', 'traversals', 'merchantable', 'sheikhdoms', 'circularize', 'sufferance', 'depressor', 'communistic', 'extroversive', 'dreadnought', 'finality', 'comport', 'householders', 'trilateral', 'unmolested', 'encamping', 'circumcising', 'depictive', 'bestowals', 'foreigner', 'griping', 'intraspecific', 'abbreviate', 'subserving', 'unintelligible', 'synthesize', 'mushroomed', 'intoxication', 'teaspoonful', 'wrongdoer', 'transponder', 'protectorship', 'stormy', 'trusteeship', 'retrials', 'intensions', 'instrumentality', 'postmodernism', 'imposition', 'sprinkle', 'postdated', 'perished', 'modesty', 'depopulate', 'hypervelocity', 'moderatorship', 'transfusing', 'immigrating', 'misbehave', 'acronymic', 'receiverships', 'unploughed', 'inharmonious', 'unconcern', 'rascality', 'prostatic', 'cofactors', 'kazakhstani', 'prolapse', 'allurement', 'extendible', 'afghani', 'intersected', 'relinquishment', 'christianise', 'subfamily', 'refresher', 'leisured', 'highlanders', 'disquieting', 'reckoner', 'guardedly', 'monocultures', 'postmarks', 'hybridise', 'regenerate', 'interlink', 'grassland', 'enshrouded', 'undetectable', 'rudderless', 'itch', 'dissimulate', 'intending', 'fruiterer', 'reorientate', 'encapsulate', 'syntactic', 'sportive', 'noncitizens', 'subordination', 'acrobat', 'nerveless', 'unsettled', 'unfavourable', 'actuator', 'exceedance', 'unsalable', 'speculativeness', 'devilishly', 'repositioned', 'prophetic', 'fiddled', 'combusted', 'sexless', 'subsequences', 'irreligious', 'interrelate', 'morph', 'anticyclones', 'resistive', 'fasten', 'ravenous', 'amorphous', 'clergyman', 'irresolution', 'predetermination', 'fireproof', 'defrauding', 'adjournment', 'orientate', 'badness', 'reenact', 'decorate', 'florescence', 'gaiety', 'discounters', 'blurting', 'concavity', 'inquirer', 'moisten', 'brigadier', 'know-how', 'disabused', 'fundamentalism', 'eruptive', 'sulfide', 'nonconscious', 'reassess', 'nonrepresentational', 'paving', 'undiscerning', 'stressor', 'microfossils', 'luxuriance', 'institutionalize', 'reproachful', 'apprenticeship', 'ruralist', 'nonfunctional', 'indelicate', 'academicism', 'flicker', 'short-change', 'intercommunicate', 'stitch', 'scornful', 'courteous', 'disfavor', 'transmuted', 'clericalism', 'embroiderers', 'discontinuous', 'painkillers', 'harpsichord', 'inbreeding', 'antisubmarine', 'pedicab', 'negotiable', 'preschooler', 'rainless', 'analogize', 'scheduled', 'unworthiness', 'vindictiveness', 'monotony', 'quitter', 'monoatomic', 'debarred', 'formalisms', 'septic', 'purveying', 'deciphering', 'annihilator', 'ejector', 'combusts', 'managership', 'descend', 'transmigrated', 'spirited', 'unsighted', 'syntaxes', 'greengrocery', 'commandership', 'exploitive', 'thoughtless', 'helm', 'inexpert', 'cylindric', 'sensualist', 'urbanize', 'nonnative', 'unfeathered', 'subserve', 'fret', 'desertion', 'inexplicable', 'interlinks', 'salable', 'faze', 'defame', 'alleviated', 'fording', 'disassembled', 'adversely', 'protractors', 'unilluminated', 'naughtiness', 'insubordinate', 'indicted', 'resigning', 'extrajudicial', 'inducted', 'utilitarianism', 'unparented', 'deflowering', 'microcircuit', 'cultist', 'insufficiency', 'ceramicist', 'traitorous', 'engorge', 'disinvestment', 'nanosecond', 'evaporate', 'endangerment', 'monoplanes', 'cozy', 'antagonize', 'hypermarkets', 'spoonfuls', 'rarity', 'surpass', 'circumvented', 'battened', 'automates', 'purposeless', 'emulsify', 'carefreeness', 'interlace', 'baggers', 'microseconds', 'terrorize', 'translocate', 'conformations', 'entrench', 'macrocosmic', 'dazzle', 'unsexy', 'pastorship', 'aerialist', 'unaffected', 'demerit', 'unemotional', 'quieten', 'opportune', 'splitter', 'helplessness', 'confine', 'approachable', 'conscientious', 'copilots', 'undefended', 'monsignori', 'angrier', 'unsubdivided', 'eldership', 'postcodes', 'emerald', 'reinsured', 'standoffish', 'inventively', 'concerti', 'fractionate', 'flatulence', 'confirmable', 'fastness', 'circumpolar', 'weaponize', 'inoffensive', 'condescend', 'procreating', 'initialise', 'soloist', 'philosophic', 'unwrap', 'preteens', 'unfavorable', 'bibliographies', 'disbelieving', 'exacerbated', 'extraterritorial', 'moralist', 'shanked', 'bronchus', 'longing', 'caramelize', 'practicality', 'traverse', 'hankering', 'prophetical', 'unicyclist', 'ideality', 'incorrupt', 'conductive', 'mayoralty', 'insidiously', 'untruth', 'serenaded', 'magnetize', 'astonish', 'injure', 'coinsurance', 'unconsciousness', 'disarranged', 'imbedding', 'radiators', 'changeableness', 'acknowledgement', 'energized', 'infectiously', 'conductance', 'duplicable', 'secularist', 'transmutes', 'epicure', 'heartlessness', 'deflate', 'cession', 'gravitated', 'semiconducting', 'mutinied', 'conspicuousness', 'nontoxic', 'lustrate', 'prospector', 'unrealizable', 'survivalist', 'copilot', 'repressing', 'castled', 'premisses', 'enjoins', 'provincialism', 'ingroup', 'elector', 'tasteful', 'lusterware', 'transshipped', 'unzipping', 'unwed', 'contrabands', 'secluding', 'differentia', 'resurfacing', 'autopilot', 'auditive', 'causative', 'synchronized', 'interstellar', 'mogul', 'principality', 'ecclesiastic', 'normalize', 'consubstantial', 'consumptive', 'evangelicalism', 'exterminate', 'enfolding', 'commode', 'postholes', 'copartnership', 'libelous', 'retarding', 'inflame', 'toppled', 'unclog', 'encroachments', 'subhead', 'banished', 'rhythmicity', 'incontestable', 'deviationism', 'narrow-mindedness', 'lilt', 'monoculture', 'interrelated', 'pestilence', 'predetermine', 'rooters', 'indoctrinate', 'disfavoring', 'pretenders', 'subjoined', 'characterless', 'nonobservant', 'adjustor', 'compartmentalization', 'fieldworker', 'unisexual', 'topically', 'inheritable', 'migrate', 'nonprofessional', 'regularize', 'abductor', 'interchanging', 'entombment', 'absorbance', 'incommensurate', 'demoralise', 'indecent', 'spoonful', 'whimsically', 'reinterpret', 'brisker', 'gladness', 'hilarity', 'practicable', 'bestowal', 'cosponsors', 'primer', 'presuppose', 'engrave', 'possessor', 'sorrowful', 'unfledged', 'antitumor', 'amateurish', 'nosiness', 'profusion', 'healthful', 'sanctify', 'oust', 'mingles', 'rededicated', 'piquancy', 'shortish', 'regularise', 'retrying', 'disloyal', 'associational', 'interpenetrate', 'footballers', 'imitate', 'refuels', 'sublieutenant', 'riskless', 'habitable', 'grassroots', 'undemocratic', 'highjacking', 'friendliness', 'sourdough', 'incised', 'subspaces', 'conjoins', 'freshness', 'prejudging', 'molar', 'corespondent', 'helical', 'constitutive', 'outperforming', 'germinate', 'encrust', 'feudalism', 'irreverence', 'transact', 'connectedness', 'ennobled', 'reburial', 'necessitate', 'microfilm', 'conscientiousness', 'postmark', 'pathless', 'fetishism', 'allergic', 'fantasist', 'cocoon', 'lushness', 'reformations', 'remounted', 'convergent', 'interlinking', 'prejudge', 'unmentionables', 'exemplify', 'denominate', 'premeditation', 'compel', 'mimicked', 'apocalyptical', 'microphallus', 'hyperlink', 'asexual', 'syphons', 'unassertiveness', 'unicycles', 'unisons', 'self-discipline', 'grievous', 'artlessness', 'unstuff', 'standardise', 'overshoe', 'distressful', 'vanish', 'entwined', 'adventism', 'undeniable', 'insurrectionist', 'villainous', 'autobuses', 'tripod', 'cheapen', 'procreation', 'immobilizing', 'intracerebral', 'encrusted', 'discordance', 'insatiate', 'disavowed', 'reclaim', 'hydrolysed', 'starkness', 'reassuringly', 'affectional', 'momentousness', 'nomad', 'punctuate', 'positioners', 'exaction', 'crusaders', 'maleness', 'uproariously', 'placidity', 'concreteness', 'eroticism', 'divisible', 'insurgent', 'unicycle', 'unconvincing', 'discontinuance', 'religionist', 'electioneering', 'snookered', 'nakedness', 'dwarfish', 'innovativeness', 'unequivocal', 'alarmism', 'spiritless', 'homophony', 'partiality', 'interwove', 'romanic', 'sightedness', 'therapeutical', 'intercede', 'monarchic', 'uninterested', 'venomous', 'behaviorist', 'foresters', 'transsexual', 'hypermarket', 'implausible', 'stand-in', 'circumferential', 'recast', 'microfiche', 'cofactor', 'unquestioned', 'circumspect', 'mitigated', 'sweetish', 'subdividing', 'indexical', 'grinder', 'intramural', 'needleworker', 'deviously', 'radiance', 'dynastic', 'bobbers', 'ebb', 'tricolour', 'schnauzer', 'nonpublic', 'regimental', 'hotness', 'subletting', 'impolitic', 'marginalize', 'clairvoyant', 'undisclosed', 'pinpointed', 'nobelist', 'provisionary', 'sulfuric', 'preservers', 'irredeemable', 'medicate', 'commutation', 'promiscuous', 'plundered', 'excommunicated', 'incredulous', 'tunneled', 'marinate', 'blacken', 'impermissible', 'flattery', 'rediscovery', 'weirdly', 'reformism', 'subroutines', 'hypersensitive', 'incomprehension', 'preempt', 'monarchical', 'legalism', 'occlusion', 'discountenance', 'displeases', 'distillate', 'apolitical', 'dissociations', 'flim-flam', 'cross-link', 'excitations', 'rotational']:
        if word in ignore:
            continue
        b_ok = False
        try:
            word.encode(encoding='utf-8').decode('ascii')
            b_ok = True
        except UnicodeDecodeError as e:
            continue
        # Method returns 0 if word was already known, 1 if scrape was successfull, -1 otherwise
        try:
            if scraper.get_syllables_for_word(word) != 0:
                sleep_time = time_between_calls + (np.random.rand() * noise - bias)
                print(f"Going to sleep for {sleep_time} seconds...")
                sleep(time_between_calls)
        except Exception as e:
            if isinstance(e, URLError):
                print(f"URLError:\n{e}")
                print(f"Going to sleep for {5 * 60} seconds...")
                sleep(5 * 60)
            else:
                print(f"Error class: {e.__class__.__name__}\n{e}")
                print(f"Going to sleep for {10} seconds...")
                sleep(10)
