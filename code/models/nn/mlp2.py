"""
The idea of this model is to provide four binary representations of (positions of) tokens (in a sorted list) at the
input, and then expand each of these binary representations before merging them and presenting them as input to the
embedding layer.
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
from config import Config


class MLP2(nn.Module):
    def __init__(self, nb_input: int, nb_output: int, expand_size=64, embed_size=300):
        super(MLP2, self).__init__()
        if nb_input % 4 != 0:
            raise ValueError(f"Input dimension is expected to be divisible by 4, which {nb_input} isn't.")

        self.token_dim = nb_input//4

        self.softmax = nn.Softmax(dim=1).to(Config.DEVICE)

        self.l_tokens = [nn.Linear(self.token_dim, expand_size).to(Config.DEVICE) for _ in range(4)]

        self.l_embed = nn.Linear(4 * expand_size, embed_size).to(Config.DEVICE)
        self.out = nn.Linear(embed_size, nb_output).to(Config.DEVICE)

    def forward(self, input: torch.Tensor, b_train=False):
        output = self.decode(self.encode(input), b_train)

        return output

    def encode(self, input: torch.Tensor):
        dim = len(input.shape)-1
        ins = torch.chunk(input, chunks=4, dim=dim)

        outs = [self.l_tokens[i](ins[i]) for i in range(4)]

        t = torch.cat(outs, dim=dim)

        # Normalize embeddings

        return F.normalize(self.l_embed(torch.tanh(t)), dim=1)

    def decode(self, input, b_train=False):
        out = self.out(input)

        return out if b_train else self.softmax(out)
