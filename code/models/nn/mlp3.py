"""
Same as MLP2 with an extra layer.
"""
import torch
import torch.nn as nn
from config import Config


class MLP3(nn.Module):
    def __init__(self, nb_input: int, nb_output: int, expand_size=64, embed_size=300):
        super(MLP3, self).__init__()
        if nb_input % 4 != 0:
            raise ValueError(f"Input dimension is expected to be divisible by 4, which {nb_input} isn't.")

        self.token_dim = nb_input//4

        self.relu = nn.ReLU().to(Config.DEVICE)
        self.elu = nn.ELU().to(Config.DEVICE)
        # self.sig = nn.Sigmoid().to(RadixAIConfig.DEVICE)
        self.softmax = nn.Softmax(dim=1).to(Config.DEVICE)

        self.l_tokens = [nn.Linear(self.token_dim, expand_size).to(Config.DEVICE) for _ in range(4)]

        self.l_embed_1 = nn.Linear(4 * expand_size, 4 * expand_size).to(Config.DEVICE)
        self.l_embed_2 = nn.Linear(4 * expand_size, embed_size).to(Config.DEVICE)
        self.out = nn.Linear(embed_size, nb_output).to(Config.DEVICE)

    def forward(self, input: torch.Tensor, b_train=False):
        output = self.decode(self.encode(input), b_train)

        return output

    def encode(self, input: torch.Tensor):
        dim = len(input.shape)-1
        ins = torch.chunk(input, chunks=4, dim=dim)

        outs = [self.l_tokens[i](ins[i]) for i in range(4)]

        t = torch.cat(outs, dim=dim)

        return self.elu(self.l_embed_2(self.relu(self.l_embed_1(self.relu(t)))))

    def decode(self, input, b_train=False):
        out = self.out(input)

        return out if b_train else self.softmax(out)
