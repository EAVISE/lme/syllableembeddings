import torch.nn as nn
from config import Config


class MLP(nn.Module):
    def __init__(self, nb_input: int, nb_output: int, embed_size=300):
        super(MLP, self).__init__()
        self.relu = nn.ReLU().to(Config.DEVICE)
        self.elu = nn.ELU().to(Config.DEVICE)
        self.sig = nn.Sigmoid().to(Config.DEVICE)
        self.softmax = nn.Softmax(dim=1).to(Config.DEVICE)

        self.l1 = nn.Linear(nb_input, embed_size).to(Config.DEVICE)
        self.out = nn.Linear(embed_size, nb_output).to(Config.DEVICE)

    def forward(self, input, b_train=False):
        output = self.decode(self.encode(input), b_train)

        return output

    def encode(self, input):
        return self.elu(self.l1(input))

    def decode(self, input, b_train=False):
        out = self.out(input)

        return out if b_train else self.softmax(out)
