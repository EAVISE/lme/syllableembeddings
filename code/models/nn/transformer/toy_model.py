import math
import torch
import torch.nn as nn

device = torch.device('cpu')  # torch.device("cuda" if torch.cuda.is_available() else "cpu")


class PositionalEncoding(nn.Module):
    def __init__(self, d_model, dropout=0.1, max_len=5000):
        super(PositionalEncoding, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, d_model)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)

        # print(f"pe.shape: {pe.shape}")
        self.register_buffer('pe', pe)

    def forward(self, x):
        # print(f"x.shape: {x.shape}")
        x = x + self.pe[:x.size(1), :]
        return self.dropout(x)


class ToyModel(nn.Module):
    def __init__(self, nb_tokens: int, emb_size: int, nb_layers=2, nb_heads=4, hid_size=512, dropout=0.5, max_len=10,
                 padding_idx=None):
        super(ToyModel, self).__init__()
        from torch.nn import TransformerEncoder, TransformerEncoderLayer, TransformerDecoder, TransformerDecoderLayer
        self.emb_size = emb_size

        self.pos_encoder = PositionalEncoding(emb_size, dropout=dropout, max_len=max_len)
        self.embedder = nn.Embedding(nb_tokens, emb_size, padding_idx=padding_idx)

        encoder_layer = TransformerEncoderLayer(d_model=emb_size, nhead=nb_heads,
                                                dim_feedforward=hid_size, dropout=dropout)
        self.encoder = TransformerEncoder(encoder_layer=encoder_layer, num_layers=nb_layers)

        decoder_layer = TransformerDecoderLayer(d_model=emb_size, nhead=nb_heads,
                                                dim_feedforward=hid_size, dropout=dropout)
        self.decoder = TransformerDecoder(decoder_layer=decoder_layer, num_layers=nb_layers)

        self.out_lin = nn.Linear(in_features=emb_size, out_features=nb_tokens)
        self.soft_max = nn.Softmax(dim=2)

        self.tgt_mask = None

    def _generate_square_subsequent_mask(self, sz):
        mask = torch.triu(torch.ones(sz, sz), diagonal=1).to(device)
        mask = mask.masked_fill(mask == 1, float('-inf'))
        return mask

    def init_weights(self):
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def enc_forward(self, src):
        # print(f"src.shape: {src.shape}")
        # print(f"src.device: {src.device}")
        # print(f"self.src_mask.shape: {self.src_mask.shape}")

        src = self.embedder(src) * math.sqrt(self.emb_size)
        # print(f"embed src.shape: {src.shape}")
        src = self.pos_encoder(src.view(src.shape[0], 1, src.shape[1]))
        # print(f"pos   src.shape: {src.shape}")
        output = self.encoder(src, mask=None)

        return output

    def dec_forward(self, memory, tgt):
        if self.tgt_mask is None or self.tgt_mask.size(0) != len(tgt):
            mask = self._generate_square_subsequent_mask(len(tgt)).to(device)
            self.tgt_mask = mask

        tgt = self.embedder(tgt) * math.sqrt(self.emb_size)
        tgt = tgt.view(tgt.shape[0], 1, tgt.shape[1])
        output = self.soft_max(self.out_lin(self.decoder(memory=memory, tgt=tgt, tgt_mask=self.tgt_mask)))

        return output

    def forward(self, src, tgt):
        if isinstance(tgt, int):
            tgt = torch.Tensor([tgt] + [0]*30).long.to(device)
        memory = self.enc_forward(src=src)
        output = self.dec_forward(memory=memory, tgt=tgt)

        return output


class ToyModelSimple(nn.Module):
    def __init__(self, nb_tokens: int, emb_size: int, nb_layers=2, nb_heads=4, hid_size=512, dropout=0.25, max_len=10,
                 padding_idx=None):
        super(ToyModelSimple, self).__init__()
        from torch.nn import Transformer
        self.emb_size = emb_size

        self.pos_encoder = PositionalEncoding(emb_size, dropout=dropout, max_len=max_len)
        self.embedder = nn.Embedding(nb_tokens, emb_size, padding_idx=padding_idx)

        self.transformer = Transformer(d_model=emb_size, nhead=nb_heads, num_encoder_layers=nb_layers,
                                       num_decoder_layers=nb_layers, dim_feedforward=hid_size, dropout=dropout)

        self.out_lin = nn.Linear(in_features=emb_size, out_features=nb_tokens)
        self.soft_max = nn.Softmax(dim=0)

        self.tgt_mask = None

    def _generate_square_subsequent_mask(self, sz):
        mask = torch.triu(torch.ones(sz, sz), diagonal=1).to(device)
        mask = mask.masked_fill(mask == 1, float('-inf'))
        return mask

    def init_weights(self):
        initrange = 0.1
        self.encoder.weight.data.uniform_(-initrange, initrange)
        self.decoder.bias.data.zero_()
        self.decoder.weight.data.uniform_(-initrange, initrange)

    def enc_forward(self, src, src_key_padding_mask=None):
        src = self.embedder(src) * math.sqrt(self.emb_size)
        src = self.pos_encoder(src)
        src = src.transpose(0, 1)
        # print(f"src: {src}")
        # print(f"src_key_padding_mask: {src_key_padding_mask}")
        # If single element is passed
        # src = self.pos_encoder(src.view(src.shape[0], 1, src.shape[1]))

        output = self.transformer.encoder(src, src_key_padding_mask=src_key_padding_mask)
        # output = self.transformer.encoder(src)
        # print(output)

        return output

    def dec_forward(self, memory, tgt, tgt_key_padding_mask=None):
        # if isinstance(tgt, int):
        #     tgt = torch.Tensor([tgt] + [0]*29).long().to(device)

        if self.tgt_mask is None or self.tgt_mask.size(0) != tgt.shape[1]:
            mask = self._generate_square_subsequent_mask(tgt.shape[1]).to(device)
            self.tgt_mask = mask

        tgt = self.embedder(tgt) * math.sqrt(self.emb_size)
        tgt = self.pos_encoder(tgt)
        tgt = tgt.transpose(0, 1)

        # print(f"tgt.shape: {tgt.shape}")
        # print(f"tgt_mask.shape: {self.tgt_mask.shape}")
        # print(f"tgt_key_padding_mask.shape: {tgt_key_padding_mask.shape}")

        # print(f"memory: \n{memory}")

        output = self.out_lin(self.transformer.decoder(memory=memory, tgt=tgt, tgt_mask=self.tgt_mask,
                                                       tgt_key_padding_mask=tgt_key_padding_mask))
        if not self.training:
            output = torch.nn.functional.softmax(output, 2)

        return output

    def forward(self, src, tgt, src_key_padding_mask=None, tgt_key_padding_mask=None):
        memory = self.enc_forward(src, src_key_padding_mask)
        output = self.dec_forward(memory, tgt, tgt_key_padding_mask)

        return output


if __name__ == '__main__':
    model = ToyModelSimple(emb_size=100, nb_tokens=100)
    vec_in = torch.LongTensor([50, 6, 7, 1])
    print(model.forward(vec_in, tgt=2).shape)

# Check for batch processing stuff:
# https://towardsdatascience.com/how-to-code-the-transformer-in-pytorch-24db27c8f9ec#1b3f
