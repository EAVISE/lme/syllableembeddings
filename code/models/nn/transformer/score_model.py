"""
Load a pretrained syllable splitter model, and check how many words it gets right.
"""
import os

import numpy as np

from models.syllables.splitter_dataset import Seq2SeqSplitterDataset
from config import Config, Lang
from syllables.en_syllables import ENSyllables
import torch

from syllables.nlwiktionary_syllables import NLWiktionarySyllables

device = torch.device('cuda')


def greedy_decode(model, src, max_len=4, start_symbol=0, stop_symbol=None):
    # print(f"src.shape: {src.shape}")
    memory = model.enc_forward(src)
    # print(f"memory.shape: {memory.shape}")
    idxs = [start_symbol]
    next_char = start_symbol
    while next_char != stop_symbol:
    # for i in range(max_len-1):
        if len(idxs) == max_len:
            # print("Breaking due to max_len")
            break
        ys = torch.LongTensor(idxs).unsqueeze(0).to(device)
        # print(f"ys.shape: {ys.shape}")
        out = model.dec_forward(memory=memory, tgt=ys)
        # print(f"out.shape: {out.shape}")
        # print(f"out[-1,:,:]: {out[-1,:,:]}")
        # print(torch.sum(out[-1,:,:], dim=1))
        # print(f"torch.max(out[-1,:,:], dim=1): {torch.max(out[-1,:,:], dim=1)}")
        _, next_char = torch.max(out[-1, :, :], dim=1)
        next_char = next_char.item()
        # print(f"next_word: {next_word}")
        idxs.append(next_char)
        # print(f"ys: {ys}")

    return idxs

model = torch.load(open(os.path.join(Config.MODELS_DIR,
                                     "nl_syll_splitter_transformer_em64_hid1x256_head8_epoch17.torch"), 'rb'),
                   map_location='cuda')
lang = Lang.NL
# data_loader = torch.utils.data.DataLoader(dataset=ToyData(n_elems=2500, shift=2), batch_size=1)
if lang == Lang.EN:
    sylls = ENSyllables()
elif lang == Lang.NL:
    sylls = NLWiktionarySyllables()
else:
    raise ValueError(f"Language not supported: {lang.value}")

uq_words = sorted(sylls.data.keys())
idxs = np.random.choice(len(uq_words), 70000 if lang == Lang.EN else 200000, False)
train_words = {uq_words[i]: sylls.data[uq_words[i]] for i in idxs}
max_len = 100
train_data = Seq2SeqSplitterDataset(data=train_words, max_len_out=max_len, b_pad=False, b_return_one_hot=False)
start_token = train_data.abc_map[train_data.START_CHAR]
stop_token = train_data.abc_map[train_data.STOP_CHAR]

correct, wrong, total = 0, 0, 0
for word in sylls.data.keys():
    if ' ' in word:
        continue
    word_sylls = sylls.get_syllabic_decomposition_for_word(word)[0]
    word_sylls = [x.lower() for x in word_sylls]
    word_target = train_data.START_CHAR + '/'.join(word_sylls) + train_data.STOP_CHAR
    try:
        eval_data = train_data.word_to_torch(word.lower(), b_as_one_hot=False)
    except KeyError:
        continue

    eval_data = eval_data.unsqueeze(0)
    greedy = greedy_decode(model=model, src=eval_data.long().to(device),
                           max_len=len(word) + 8, start_symbol=start_token, stop_symbol=stop_token)
    target_pred = train_data.idxs_to_word(greedy)
    # print(f"{word:20s} :: [{word_target}] vs [{target_pred}]")
    total += 1
    if word_target == target_pred:
        correct += 1
    else:
        wrong += 1
    if total % 250 == 0:
        print(f"\rScore: {correct}/{total} [{100*correct/total:.1f}]", end='', flush=True)
print(f"\rScore: {correct}/{total} [{100 * correct / total:.1f}]")
