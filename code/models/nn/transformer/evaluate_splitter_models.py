"""
Script that aims to do what it says on the tin: evaluate splitter models trained using toy_trainer.py.
"""

# Load training data
import itertools
import os

import dill
import torch

from config import Config, Lang
from syllables.en_syllables import ENSyllables
from syllables.nlwiktionary_syllables import NLWiktionarySyllables


def greedy_decode(model, src, src_key_padding_mask=None, max_len=4, start_symbol=0, stop_symbol=None):
    # print(f"src.shape: {src.shape}")
    memory = model.enc_forward(src, src_key_padding_mask=src_key_padding_mask)
    # print(f"memory.shape: {memory.shape}")
    idxs = [start_symbol]
    next_char = start_symbol
    while next_char != stop_symbol:
    # for i in range(max_len-1):
        if len(idxs) == max_len:
            # print("Breaking due to max_len")
            break
        ys = torch.LongTensor(idxs).unsqueeze(0).to(device)
        # print(f"ys.shape: {ys.shape}")
        out = model.dec_forward(memory=memory, tgt=ys)
        # print(f"out.shape: {out.shape}")
        # print(f"out[-1,:,:]: {out[-1,:,:]}")
        # print(torch.sum(out[-1,:,:], dim=1))
        # print(f"torch.max(out[-1,:,:], dim=1): {torch.max(out[-1,:,:], dim=1)}")
        _, next_char = torch.max(out[-1, :, :], dim=1)
        next_char = next_char.item()
        # print(f"next_word: {next_word}")
        idxs.append(next_char)
        # print(f"ys: {ys}")

    return idxs


lang = Lang.EN

# Load train data
train_data = dill.load(open(os.path.join(Config.MODELS_DIR, 'Paper',
                                         'nl_syll_splitter_train_data_250000.dill' if lang == Lang.NL else
                                         'en_syll_splitter_train_data_55000.dill'), 'rb'))
start_token = train_data.abc_map[train_data.START_CHAR]
stop_token = train_data.abc_map[train_data.STOP_CHAR]
# Load eval data, based on train data
if lang == Lang.EN:
    sylls = ENSyllables()
elif lang == Lang.NL:
    sylls = NLWiktionarySyllables()

words_eval = {k: v for k, v in sylls.data.items() if k not in train_data.data.keys()}
# Remove words that don't pass the check
to_remove = set()
for k, v in words_eval.items():
    if not train_data.check(k, v):
        to_remove.add(k)
for k in to_remove:
    del words_eval[k]
print(f"Size of evaluation set: {len(words_eval)}")

# Iterate over models and check performance over eval data
device = torch.device('cpu')
epoch = "epoch4"  # Which epoch to check?
for emsize, nlayers, nhid, nhead in itertools.product([32],
                                                      [2],
                                                      [256],  # , 512],
                                                      [8]):

    model_name = f"{lang.value}_syll_splitter_transformer_em{emsize}_hid{nlayers}x{nhid}_head{nhead}_{epoch}"
    model_file = os.path.join(Config.MODELS_DIR, 'Paper', f"{model_name}.torch")
    if not os.path.isfile(model_file):
        # print(f"Model could not be found: {model_file}")
        continue

    print(f"Evaluating model: {model_name}")
    model = torch.load(open(model_file, 'rb'), map_location=device)

    correct, wrong, total = 0, 0, 0
    for word, word_sylls in words_eval.items():
        if ' ' in word:
            continue
        word_sylls = [x.lower() for x in word_sylls[0]]
        word_target = train_data.START_CHAR + '/'.join(word_sylls) + train_data.STOP_CHAR
        try:
            next_eval_data = train_data.word_to_torch(word.lower(), b_as_one_hot=False, pad_len=0)
        except KeyError:
            continue

        eval_data = next_eval_data.unsqueeze(0)
        data_mask = None
        # data_mask = (eval_data == train_data.abc_map[train_data.PAD_CHAR]).to(device)
        # print(f"eval_data.shape: {eval_data.shape}")
        greedy = greedy_decode(model=model, src=eval_data.long().to(device), src_key_padding_mask=data_mask,
                               max_len=len(word) + 8, start_symbol=start_token, stop_symbol=stop_token)
        target_pred = train_data.idxs_to_word(greedy)
        # print(f"{word:20s} :: [{word_target}] vs [{target_pred}]")
        total += 1
        if word_target == target_pred:
            correct += 1
        else:
            # print(f"{word:20s} :: [{word_target}] vs [{target_pred}]")
            wrong += 1
        if total % 250 == 0:
            print(f"\rScore: {correct}/{total} [{100*correct/total:.1f}]", end='', flush=True)

        # if total == 1000:
        #     break
    print(f"\rScore: {correct}/{total} [{100 * correct / total:.1f}]")

