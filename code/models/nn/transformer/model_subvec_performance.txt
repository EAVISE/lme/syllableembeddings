Model: TransformerSyllableEncoder2
EN Syllables
Loaded with syllabes: 55262
	Unique syllables: 24788
1. ------------------------------------------------------------
batch_size = 250
train_data = TransformerSyllableDataset(lang=Lang.EN, nb_to_load=-1, b_pad=True, b_start=True, b_end=True)
data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

model = TransformerSyllableEncoder2(voc_size=train_data.nb_sylls, emb_dim=300).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.05)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.5)

epochs = 25
Epoch 24 :: batch 200: total_loss = 0.50157
	Avg. loss per item: 0.00001
WS353: 0.45758963668853114
2. ------------------------------------------------------------
Same as 1., but without normalization of output in network
batch_size = 250
train_data = TransformerSyllableDataset(lang=Lang.EN, nb_to_load=-1, b_pad=True, b_start=True, b_end=True)
data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

model = TransformerSyllableEncoder2(voc_size=train_data.nb_sylls, emb_dim=300).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.05)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.5)

epochs = 25
Epoch 24 :: batch 200: total_loss = 0.3974143
	Avg. loss per item: 0.0000079
WS353: 0.3399054928278712
3. ------------------------------------------------------------
Same as 1., but altered model to include positional encoding to Q and K matrices
batch_size = 250
train_data = TransformerSyllableDataset(lang=Lang.EN, nb_to_load=-1, b_pad=True, b_start=True, b_end=True)
data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

model = TransformerSyllableEncoder2(voc_size=train_data.nb_sylls, emb_dim=300).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.05)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.5)

epochs = 25
Epoch 24 :: batch 200: total_loss = 0.6090712
	Avg. loss per item: 0.0000122
WS353: 0.4294540635436294
4. ------------------------------------------------------------
No positional encoding, no passing v through a linear layer before multiplying with activations
No start/end tags!
batch_size = 250
train_data = TransformerSyllableDataset(lang=Lang.EN, nb_to_load=-1, b_pad=True, b_start=False, b_end=False)
data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

model = TransformerSyllableEncoder2(voc_size=train_data.nb_sylls, emb_dim=300).to(device)

lr = 0.05
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=lr, betas=(0.90, 0.995), eps=1e-8, amsgrad=True)
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=optimizer,
                                              lr_lambda=[lambda epoch: lr*(0.85 ** (epoch+1)/2)])

Epoch 31 :: batch 200: total_loss = 0.5684271
	Avg. loss per item: 0.0000114
WS353: 0.4681816669594555
5. ------------------------------------------------------------
Same network as 4., but with start/end tags.
Note the altered eps value for the Adam optimizer
batch_size = 250
train_data = TransformerSyllableDataset(lang=Lang.EN, nb_to_load=-1, b_pad=True, b_start=True, b_end=True)
data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

model = TransformerSyllableEncoder2(voc_size=train_data.nb_sylls, emb_dim=300).to(device)

lr = 0.05
criterion = nn.MSELoss()
optimizer = torch.optim.Adam(model.parameters(), lr=lr, betas=(0.90, 0.999), eps=1e-9, amsgrad=False)
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=optimizer,
                                              lr_lambda=[lambda epoch: lr/math.sqrt(epoch + 1)])

Epoch 14 :: batch 200: total_loss = 0.3470782
	Avg. loss per item: 0.0000069
WS353: 0.6313814474209981
