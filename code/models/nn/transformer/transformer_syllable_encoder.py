"""
An attempt at learning syllable embeddings through the use of a Transformer Encoder network.

For some reason, this produces very bad results.
Also, using batches everything gets even worse. No idea why.
"""
import math
import os

import torch
import torch.nn as nn
from torch.nn.modules import TransformerEncoder, TransformerEncoderLayer

from models.nn.transformer.positional_encoding import PositionalEncoder
from models.syllables.transformer_syllable_dataset import TransformerSyllableDataset
from config import Lang, Config

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class TransformerSyllableEncoder(nn.Module):
    def __init__(self, voc_size, emb_dim, pad_idx=None, num_layers=2, nb_heads=4, hid_size=512, dropout=0.2):
        super(TransformerSyllableEncoder, self).__init__()
        self.emb_dim = emb_dim
        self.embedder = nn.Embedding(num_embeddings=voc_size, embedding_dim=emb_dim,
                                     max_norm=1., norm_type=2., padding_idx=pad_idx)
        self.pos_encoder = PositionalEncoder(emb_dim=emb_dim, dropout=dropout, max_len=100)
        encoder_layer = TransformerEncoderLayer(d_model=emb_dim, nhead=nb_heads,
                                                dim_feedforward=hid_size, dropout=dropout)
        self.encoder = TransformerEncoder(encoder_layer=encoder_layer, num_layers=num_layers)
        # self.self_attn = nn.MultiheadAttention(embed_dim=emb_dim, num_heads=nb_heads, dropout=dropout)

        # self.lin_out = nn.Linear(in_features=emb_dim, out_features=emb_dim)

    def forward(self, src, src_mask=None, b_debug=False):
        """
        N = batch size
        S = sequence length
        E = embedding dim

        :param src: Tensor(N,S,E)
        :param src_mask: Tensor(N,S)
        :param b_debug:
        :return:
        """
        # Embed src
        in_src = self.embedder(src) * math.sqrt(self.emb_dim)
        if b_debug:
            print(f"in_src.shape: {in_src.shape}")
        # Add position encoding
        in_src = self.pos_encoder(in_src, dim=1)
        if b_debug:
            print(f"in_src.shape + pos: {in_src.shape}")

        in_src = torch.einsum('ijk->jik', in_src)  # Permute
        if b_debug:
            print(f"einsum(in_src.shape): {in_src.shape}")
            print(f"src_mask.shape: {src_mask.shape}")
        # Encode and sum
        output = self.encoder(src=in_src, src_key_padding_mask=src_mask)
        if b_debug:
            print(f"output.shape: {in_src.shape}")
        # print(f"pre attn : {in_src.shape}")
        # output = self.self_attn(in_src, in_src, in_src)[0]
        # print(f"post attn: {output.shape}")
        output = torch.sum(output, dim=0)
        if b_debug:
            print(f"sum(output).shape: {output.shape}")
        # Normalize
        # output = torch.nn.functional.normalize(output, p=2, dim=1)

        # output = self.lin_out(output)

        return output


if __name__ == '__main__':
    # cnet = ConceptNetVecs(lang=Lang.EN, nb_to_load=500000)
    # en_sylls = ENSyllables()
    # Get overlap between ConceptNet embeddings and words for which we have syllables
    # overlap = list(set(cnet.data.keys()).intersection(set(en_sylls.data.keys())))
    # print(f"Overlap: {len(overlap)}")

    lang = Lang.EN
    max_len = 20
    b_start, b_end = True, True
    batch_size = 1

    train_data = TransformerSyllableDataset(lang=lang, max_len=max_len, nb_to_load=-1,
                                            b_start=b_start, b_end=b_end, b_pad=(batch_size > 1))
    data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

    padding_idx = train_data.nb_sylls
    model = TransformerSyllableEncoder(voc_size=padding_idx+1, num_layers=1, nb_heads=6,
                                       emb_dim=300, hid_size=512, pad_idx=padding_idx).to(device)

    criterion = nn.MSELoss()
    lr = 0.01
    # optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.5, nesterov=True)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.9)

    optimizer = torch.optim.Adam(model.parameters(), lr=lr, betas=(0.90, 0.999), eps=1e-12, amsgrad=True)
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=optimizer,
                                                  lr_lambda=[lambda epoch: lr/math.sqrt(epoch + 1)])

    def train(model, nb_epochs=5, print_every=100, b_debug=False, model_name=None):
        model.train()  # Turn on the train mode
        for at_epoch in range(nb_epochs):
            total_loss = 0.
            for at_sample, (data, target) in enumerate(data_loader):
                data = data.to(device)
                data_mask = (data == padding_idx)
                tgt = target.to(device)
                # print(f"tgt: {tgt}")
                optimizer.zero_grad()
                output = model(data, src_mask=data_mask, b_debug=b_debug)
                # print(f"output: {output}")
                if b_debug:
                    # print(f"data: {data}")
                    # print(f"target: {target}")
                    print(f"data.shape: {data.shape}")
                    print(f"target.shape: {target.shape}")
                    print(f"output.shape: {output.shape}")
                    # print(f"output.view(-1, ntokens): {output.view(-1, ntokens)}")
                    # print(f"{torch.max(output.view(-1, ntokens), dim=1)}")
                loss = criterion(output.double(), tgt.double())
                loss.backward()
                # torch.nn.utils.clip_grad_norm_(model.parameters(), 1.)
                optimizer.step()

                total_loss += loss.item()

                if (at_sample + 1) % print_every == 0:
                    print("-" * 60)
                    print(f"Epoch {at_epoch} :: sample {at_sample + 1}: total_loss = {total_loss:7.5f}")
                    print(f"\tAvg. loss per item: {total_loss / (at_sample + 1):7.5f}")

                    # print(f"data: {data}")
                    # print(f"target: {target}")
                    # print(f"data.shape: {data.shape}")
                    # print(f"target.shape: {target.shape}")
                    # print(f"output.shape: {output.shape}")
                    # print(f"output idxs: {torch.max(output.view(-1, ntokens), dim=1)[1]}")
                    model.eval()
                    eval_loss = 0
                    eval_words = ['cat', 'power', 'parabola', 'parliament']
                    # eval_words = ['kat', 'paraplu', 'parlement', 'autostrade']
                    for w in eval_words:
                        eval_src, eval_tgt = train_data.get_word(w)
                        eval_src, eval_tgt = eval_src.unsqueeze(0).cuda(), eval_tgt.unsqueeze(0).cuda()
                        eval_res = model(eval_src)
                        # print(f"{w}\n\t{eval_res[:10]}\n\t{eval_tgt[:10]}")
                        eval_loss += criterion(eval_res, eval_tgt)
                    print(f"\tEval loss: {eval_loss/len(eval_words)}")
                    model.train()

                # log_interval = 200
                # if batch % log_interval == 0 and batch > 0:
                #     cur_loss = total_loss / log_interval
                #     elapsed = time.time() - start_time
                #     print('| epoch {:3d} | {:5d}/{:5d} batches | '
                #           'lr {:02.2f} | ms/batch {:5.2f} | '
                #           'loss {:5.2f} | ppl {:8.2f}'.format(
                #             epoch, batch, len(data_loader) // bptt, scheduler.get_lr()[0],
                #             elapsed * 1000 / log_interval,
                #             cur_loss, math.exp(cur_loss)))
                #     total_loss = 0
                #     start_time = time.time()
            scheduler.step()
            print("*" * 60)

            if model_name is not None:
                torch.save(model, os.path.join(Config.MODELS_DIR, "Paper",
                                               lang.value + '_' + model_name + f"_{at_epoch}.pt"))

        if model_name is not None:
            torch.save(model, os.path.join(Config.MODELS_DIR, "Paper",
                                           lang.value + '_' + model_name + "_final.pt"))
            # torch.save(model.state_dict(), os.path.join(RadixAIConfig.MODELS_DIR, model_name + "_state_dict.pt"))

    train(model=model, nb_epochs=50, print_every=100, model_name="tfmer_cpnet_start_end", b_debug=False)
