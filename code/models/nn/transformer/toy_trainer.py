"""
Train a Transformer network to split words into syllables. Called "ToyTrainer" for historical reasons.
"""
import itertools
import math
import os
import time

import dill
import numpy as np

import torch
import torch.nn as nn
from torch.autograd import Variable

from models.nn.transformer.toy_data import ToyData
from models.nn.transformer.toy_model import ToyModel, ToyModelSimple
from models.syllables.splitter_dataset import Seq2SeqSplitterDataset
from config import Config, Lang
from syllables.en_syllables import ENSyllables
from syllables.nlwiktionary_syllables import NLWiktionarySyllables
from tools.tools import Tools

device = torch.device('cpu')  # torch.device("cuda" if torch.cuda.is_available() else "cpu")

lang = Lang.EN
# data_loader = torch.utils.data.DataLoader(dataset=ToyData(n_elems=2500, shift=2), batch_size=1)
if lang == Lang.EN:
    sylls = ENSyllables()
elif lang == Lang.NL:
    sylls = NLWiktionarySyllables()
else:
    raise ValueError(f"Language not supported: {lang.value}")
uq_words = sorted(sylls.data.keys())
print(f"Unique words: {len(uq_words)}")
idxs = np.random.choice(len(uq_words), 55000 if lang == Lang.EN else 250000, False)
train_words = {uq_words[i]: sylls.data[uq_words[i]] for i in idxs}
max_len = 100
# print("!!! ALLOW TO LOAD PREVIOUSLY GENERATED TRAIN_DATA !!!")
train_data = Seq2SeqSplitterDataset(data=train_words, max_len_out=max_len, b_pad=False, b_return_one_hot=False)
exit()
# dill.dump(train_data, open(os.path.join(RadixAIConfig.MODELS_DIR, 'Paper', 'en_syll_splitter_train_data_full.dill'), 'wb'))
train_data = dill.load(open(os.path.join(Config.MODELS_DIR, 'Paper',
                                         'nl_syll_splitter_train_data_full.dill' if lang == Lang.NL else
                                         'en_syll_splitter_train_data_full.dill'), 'rb'))
print(f"Training set size: {len(train_data.data)}")
data_loader = torch.utils.data.DataLoader(train_data, batch_size=1, shuffle=True, drop_last=True)
padding_idx = train_data.get_padding_idx()

ntokens = data_loader.dataset.nb_chars  # the size of vocabulary
start_token = train_data.abc_map[train_data.START_CHAR]
stop_token = train_data.abc_map[train_data.STOP_CHAR]


def train(nb_epochs=5, b_debug=False, print_every=-1, model_name=None,
          emsize=64, nhid=256, nlayers=1, nhead=8, dropout=0.0):
    """

    :param nb_epochs: number of epochs to train
    :param b_debug: print extra debug info
    :param print_every: print loss every x batches iterations
    :param model_name: name of the file the model will be saved to, leave to None if you don't want to save
    :param emsize: embedding dimension
    :param nhid: the dimension of the feedforward network model in nn.TransformerEncoder
    :param nlayers: the number of nn.TransformerEncoderLayer in nn.TransformerEncoder
    :param nhead: the number of heads in the multiheadattention models
    :param dropout: the dropout value
    :return:
    """
    model = ToyModelSimple(nb_tokens=ntokens, emb_size=emsize, nb_layers=nlayers, nb_heads=nhead, hid_size=nhid,
                           dropout=dropout, max_len=max_len, padding_idx=padding_idx).to(device)
    # model = torch.load(open(os.path.join(RadixAIConfig.MODELS_DIR,
    #                                      "nl_syll_splitter_transformer_em64_hid2x512_head8_epoch4.torch"), 'rb'),
    #                    map_location='cuda')

    if model_name is not None:
        base_name = os.path.join(Config.MODELS_DIR, 'Paper', model_name)
        f_out = base_name + "_out.txt"

    # #####
    # TRAIN
    # #####
    criterion = nn.CrossEntropyLoss(ignore_index=padding_idx)
    lr = 1  # learning rate
    # optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.2, nesterov=True)
    # optimizer = torch.optim.Adam(model.parameters(), lr=lr, weight_decay=1e-6, amsgrad=True)
    optimizer = torch.optim.ASGD(model.parameters(), lr=lr)  # , lambd=1e-6, alpha=0.9, t0=1e5)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=1, gamma=0.2)
    # scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer,
    #                                               lr_lambda=[lambda epoch: lr/math.sqrt(epoch + 1)])
    model.train()  # Turn on the train mode

    best_loss = math.inf
    best_epoch = 0

    for at_epoch in range(nb_epochs):
        total_loss = 0.
        for at_batch, (data, target) in enumerate(data_loader):
            data = data.to(device)
            # Non-zero/True positions are ignored
            data_mask = (data == padding_idx)
            tgt = target[:, :-1].to(device)
            tgt_mask = (tgt == padding_idx)
            tgt_y = target[:, 1:].to(device)
            optimizer.zero_grad()
            output = model(data, tgt, src_key_padding_mask=data_mask, tgt_key_padding_mask=tgt_mask)

            if b_debug:
                print(f"data: {data}")
                print(f"target: {target}")
                print(f"data.shape: {data.shape}")
                print(f"target.shape: {target.shape}")
                print(f"output.shape: {output.shape}")
                print(f"output.view(-1, ntokens): {output.view(-1, ntokens)}")
                # print(f"{torch.max(output.view(-1, ntokens), dim=1)}")

            # print(output)

            preds = output.transpose(0, 1).transpose(1, 2)
            loss = criterion(preds, tgt_y)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), 1.)
            optimizer.step()

            total_loss += loss.item()

            if (at_batch+1) % print_every == 0:
                line = "-"*60 + f"\nEpoch {at_epoch} :: batch {at_batch+1}: total_loss = {total_loss:7.5f}"\
                       + f"\n\tAvg. loss per item: {total_loss / (at_batch + 1):7.5f}"
                print(line)
                if model_name is not None:
                    with open(f_out, 'a') as fout:
                        fout.write(line + '\n')

                # print(f"data: {data}")
                # print(f"target: {target}")
                # print(f"data.shape: {data.shape}")
                # print(f"target.shape: {target.shape}")
                # print(f"output.shape: {output.shape}")
                # print(f"output idxs: {torch.max(output.view(-1, ntokens), dim=1)[1]}")
                model.eval()
                # print(f"Eval output: {torch.max(model(src=data,tgt=start_token).view(-1, ntokens), dim=1)[1]}")
                if lang == Lang.EN:
                    demo_words = ['hallo', 'appetite', 'corona', 'bike', 'parliament', 'bourgeoisie', 'bizarrely',
                                  'painting', 'mother', 'circumference']
                elif lang == Lang.NL:
                    demo_words = ['hallo', 'dokter', 'parlement', 'corona', 'asfalt', 'garage', 'circulatie',
                                  'autostrade']
                for w in demo_words:
                    eval_data = train_data.word_to_torch(w, b_as_one_hot=False)
                    eval_data = eval_data.unsqueeze(0)
                    greedy = greedy_decode(model=model, src=eval_data.long().to(device),
                                           max_len=len(w) + 8, start_symbol=start_token, stop_symbol=stop_token)
                    print(f"{w:13s}: {train_data.idxs_to_word(greedy)}")
                model.train()

            # log_interval = 200
            # if batch % log_interval == 0 and batch > 0:
            #     cur_loss = total_loss / log_interval
            #     elapsed = time.time() - start_time
            #     print('| epoch {:3d} | {:5d}/{:5d} batches | '
            #           'lr {:02.2f} | ms/batch {:5.2f} | '
            #           'loss {:5.2f} | ppl {:8.2f}'.format(
            #             epoch, batch, len(data_loader) // bptt, scheduler.get_lr()[0],
            #             elapsed * 1000 / log_interval,
            #             cur_loss, math.exp(cur_loss)))
            #     total_loss = 0
            #     start_time = time.time()
        scheduler.step(epoch=at_epoch)

        if total_loss < best_loss:
            best_loss = total_loss
            best_epoch = at_epoch
        line = f"Best epoch so far: {best_epoch} with loss {best_loss}"
        print(line)
        if model_name is not None:
            with open(f_out, 'a') as fout:
                fout.write(line + '\n')

        if model_name is not None:
            torch.save(model, open(base_name + f"_epoch{at_epoch}.torch", 'wb'))
            # dill.dump(train_data, open(base_name + "_train_data.dill", "wb"))

    line = f"Best epoch: {best_epoch} with loss {best_loss}"
    print(line)
    if model_name is not None:
        with open(f_out, 'a') as fout:
            fout.write(line + '\n')

    if model_name is not None:
        torch.save(model, open(base_name + "_final.torch", 'wb'))
        # dill.dump(train_data, open(base_name + "_train_data.dill", "wb"))
        print(f"Saved final model to: {base_name + '_final.torch'}")


def greedy_decode(model, src, max_len=4, start_symbol=0, stop_symbol=None):
    # print(f"src.shape: {src.shape}")
    with torch.no_grad():
        memory = model.enc_forward(src)
    # print(f"memory.shape: {memory.shape}")
    idxs = [start_symbol]
    next_char = start_symbol
    while next_char != stop_symbol:
    # for i in range(max_len-1):
        if len(idxs) == max_len:
            # print("Breaking due to max_len")
            break
        ys = torch.LongTensor(idxs).unsqueeze(0).to(device)
        # print(f"ys.shape: {ys.shape}")
        with torch.no_grad():
            out = model.dec_forward(memory=memory, tgt=ys)
        # print(f"out.shape: {out.shape}")
        # print(f"out[-1,:,:]: {out[-1,:,:]}")
        # print(torch.sum(out[-1,:,:], dim=1))
        # print(f"torch.max(out[-1,:,:], dim=1): {torch.max(out[-1,:,:], dim=1)}")
        _, next_char = torch.max(out[-1, :, :], dim=1)
        next_char = next_char.item()
        # print(f"next_word: {next_word}")
        idxs.append(next_char)
        # print(f"ys: {ys}")

    return idxs


# def evaluate(eval_model, data_source):
#     eval_model.eval() # Turn on the evaluation mode
#     total_loss = 0.
#     ntokens = len(TEXT.vocab.stoi)
#     with torch.no_grad():
#         for i in range(0, data_source.size(0) - 1, bptt):
#             data, targets = get_batch(data_source, i)
#             output = eval_model(data)
#             output_flat = output.view(-1, ntokens)
#             total_loss += len(data) * criterion(output_flat, targets).item()
#     return total_loss / (len(data_source) - 1)

if __name__ == '__main__':
    # for emsize, nlayers, nhid, nhead in itertools.product([16, 32, 64],
    #                                                       [1, 2],
    #                                                       [64, 128, 256],  #, 512],
    #                                                       [4, 8, 16]):
    for emsize, nlayers, nhid, nhead in itertools.product([64],
                                                          [1],
                                                          [256],  #, 512],
                                                          [8]):
        if os.path.isfile(os.path.join(Config.MODELS_DIR, 'Paper',
                                       f"{lang.value}_syll_splitter_transformer_em{emsize}_hid{nlayers}x{nhid}_head{nhead}_full_out.txt")):
            print(f"Skipping model for emsize={emsize}, nlayers={nlayers}, nhid={nhid}, nhead={nhead}.")
            continue
        elif nhead > emsize//4:
            print(f"nhead > emsize//4 for nhead={nhead} and emsize={emsize}")
            continue
        print(f"Training model for emsize={emsize}, nlayers={nlayers}, nhid={nhid}, nhead={nhead}.")
        train(nb_epochs=5, print_every=250,
              #  model_name=f"{lang.value}_syll_splitter_transformer_em{emsize}_hid{nlayers}x{nhid}_head{nhead}_full",
              emsize=emsize, nlayers=nlayers, nhid=nhid, nhead=nhead)
        print("*" * 60)
        print("*" * 60)
        print("*" * 60)
