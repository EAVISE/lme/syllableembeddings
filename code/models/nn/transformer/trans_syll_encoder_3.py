"""
TransSyllableEncoder2 + added idea where we try to add a linear layer for "begin" and "end" tokens.
The hope is that these layers can replace explicit "begin" and "end" tokens.

...

Only minorly successful.
"""
import math
import os

import torch
import torch.nn as nn
from torch.nn.modules import TransformerEncoder, TransformerEncoderLayer

from models.nn.transformer.positional_encoding import PositionalEncoder
from models.syllables.transformer_syllable_dataset import TransformerSyllableDataset
from config import Lang, Config

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class TransformerSyllableEncoder3(nn.Module):
    """
    This actually isn't a real transformer model; it simply uses an attention mechanism.
    """
    def __init__(self, voc_size, emb_dim, max_len=10, dropout=0.0):
        super(TransformerSyllableEncoder3, self).__init__()
        self.emb_dim = emb_dim
        self.embedder = nn.Embedding(num_embeddings=voc_size+1,
                                     embedding_dim=emb_dim,
                                     max_norm=1., norm_type=2.,
                                     padding_idx=voc_size)

        # self.lin_v = nn.Linear(emb_dim, emb_dim)
        self.lin_k = nn.Linear(emb_dim, emb_dim, bias=False)
        self.lin_q = nn.Linear(emb_dim, emb_dim, bias=False)

        self.lin_start = nn.Linear(emb_dim, emb_dim)
        # self.lin_end = nn.Linear(emb_dim, emb_dim)

        self.pos_encoder = PositionalEncoder(emb_dim=emb_dim, dropout=dropout, max_len=max_len)

    def forward(self, src):
        """

        :param src: (N, S) hot-encoded tensor containing 1s in places corresponding to indices of embeddings to take,
        with N = batch size, S = sequence length
        :return:
        """
        # print(f"src.shape  : {src.shape}")
        v = self.embedder(src)

        # v_first = self.lin_start(v[:, 0, :])
        # print(f"v.shape: {v.shape}")
        # print(f"v_first.shape: {v_first.shape}")
        # v = torch.cat((v_first.unsqueeze(1), v[:, 1:, :]), dim=1)

        # print(f"v.mean(): {v.mean()}")
        # print(f"v.shape: {v.shape}")
        # print(v[:,:,:5])
        # v_pos = self.pos_encoder(v, dim=1)  # Add positional encoding
        v_pos = v
        # print(f"(v + pos).shape: {v.shape}")
        # print(f"(v+pos).mean(): {v.mean()}")
        # !!! This doesnt work:
        # v_pos = self.pos_encoder(v * math.sqrt(self.emb_dim), dim=1)  # Add positional encoding

        q_hat = torch.mean(self.lin_q(v_pos), dim=1).unsqueeze(1)
        k_t = self.lin_k(torch.zeros(v.shape).copy_(v_pos).to(device)).transpose(2, 1)
        # print(f"q_hat.shape: {q_hat.shape}")
        # print(f"k_t.shape  : {k_t.shape}")
        # print(k_t[:,:5,:])

        a = torch.matmul(q_hat, k_t)
        # print(f"pre  a.shape    : {a.shape}")
        # print(f"pre a: {a}")
        a = a.masked_fill(a == 0, -1e9)
        # print(f"fill a: {a}")
        a = nn.functional.softmax(a, dim=2)
        # print(f"post a.shape    : {a.shape}")
        # print(f"a: {a}")

        # v_hat = self.lin_v(v)
        # print(f"v_hat.shape: {v_hat.shape}")
        # output = torch.matmul(a, v_hat).squeeze(1)
        # print(f"output.shape: {output.shape}")
        output = torch.matmul(a, v).squeeze(1)

        # Normalize
        output = torch.nn.functional.normalize(output, p=2, dim=1)

        # output = self.lin_out(output)

        return output


if __name__ == '__main__':
    # TODO: redo training with StepLR, and adam lr=0.05, 25 epochs, something like that
    # TODO: Check out RayTune! https://stackoverflow.com/questions/44260217/hyperparameter-optimization-for-pytorch-model
    batch_size = 250
    max_len = 20
    lang = Lang.EN
    train_data = TransformerSyllableDataset(lang=lang, max_len=max_len, nb_to_load=-1,
                                            b_pad=(batch_size > 1), b_start=False, b_end=False)
    data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, drop_last=True)

    model = TransformerSyllableEncoder3(voc_size=train_data.nb_sylls, emb_dim=300, max_len=max_len).float().to(device)
    # model = torch.load(open(os.path.join(RadixAIConfig.MODELS_DIR, 'tfmer_sylls2_start_end_final.pt'), 'rb'))
    nb_params = sum(p.numel() for p in model.parameters())
    print(f"#Model parameters: {nb_params}")
    for p in model.parameters():
        print(p.shape)

    criterion = nn.MSELoss()
    lr = 0.05
    optimizer = torch.optim.Adam(model.parameters(), lr=lr, betas=(0.90, 0.999), eps=1e-12, amsgrad=True)
    # optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.5, nesterov=True)
    # optimizer = torch.optim.ASGD(model.parameters(), lr=0.01)
    # scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.5)
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=optimizer,
                                                  lr_lambda=[lambda epoch: lr/math.sqrt(epoch + 1)])

    def train(model, nb_epochs=5, print_every=100, b_debug=False, model_name=None, b_save_intermediate=False):
        model.train()  # Turn on the train mode
        last_avg = 1e2
        best_avg = 1e2
        best_epoch = 0
        for at_epoch in range(nb_epochs):
            total_loss = 0.
            avg_loss = 0.
            for at_batch, (data, target) in enumerate(data_loader):
                data = data.to(device)
                tgt = target.to(device)
                # print(f"tgt: {tgt}")
                optimizer.zero_grad()
                output = model(data)
                if b_debug:
                    print(f"data: {data}")
                    print(f"target: {target}")
                    print(f"data.shape: {data.shape}")
                    print(f"target.shape: {target.shape}")
                    print(f"output.shape: {output.shape}")
                    # print(f"output.view(-1, ntokens): {output.view(-1, ntokens)}")
                    # print(f"{torch.max(output.view(-1, ntokens), dim=1)}")
                loss = criterion(output.double(), tgt.double())
                loss.backward()

                # torch.nn.utils.clip_grad_norm_(model.parameters(), 1.)
                optimizer.step()

                total_loss += loss.item()
                avg_loss = total_loss / (batch_size * (at_batch + 1))

                if (at_batch + 1) % print_every == 0:
                    print("-" * 60)
                    print(f"Epoch {at_epoch} :: batch {at_batch + 1}: total_loss = {total_loss:9.7f}")
                    print(f"\tAvg. loss per item: {avg_loss:9.7f}")

                    # print(f"data: {data}")
                    # print(f"target: {target}")
                    # print(f"data.shape: {data.shape}")
                    # print(f"target.shape: {target.shape}")
                    # print(f"output.shape: {output.shape}")
                    # print(f"output idxs: {torch.max(output.view(-1, ntokens), dim=1)[1]}")
                    model.eval()
                    for w in ['cat', 'power', 'parabola', 'parliament']:
                    # for w in ['kat', 'paraplu', 'parlement', 'autostrade']:
                        decomp = train_data.sylls.get_syllabic_decomposition_for_word(w)
                        eval_src, eval_tgt = train_data.get_word(w)
                        eval_src, eval_tgt = eval_src.unsqueeze(0).cuda(), eval_tgt.unsqueeze(0).cuda()
                        eval_res = model(eval_src)
                        print(f"{w:10s} --> Eval loss: {criterion(eval_res, eval_tgt):9.7f} :: {decomp}: ")
                        #print(f"\t{eval_res[0,:10]}\n\t{eval_tgt[0,:10]}")
                    model.train()

                # log_interval = 200
                # if batch % log_interval == 0 and batch > 0:
                #     cur_loss = total_loss / log_interval
                #     elapsed = time.time() - start_time
                #     print('| epoch {:3d} | {:5d}/{:5d} batches | '
                #           'lr {:02.2f} | ms/batch {:5.2f} | '
                #           'loss {:5.2f} | ppl {:8.2f}'.format(
                #             epoch, batch, len(data_loader) // bptt, scheduler.get_lr()[0],
                #             elapsed * 1000 / log_interval,
                #             cur_loss, math.exp(cur_loss)))
                #     total_loss = 0
                #     start_time = time.time()
            scheduler.step()

            diff = last_avg - avg_loss
            print(f"Current avg. loss: {avg_loss:9.7f}")
            print(f"Last avg. loss   : {last_avg:9.7f}")
            print(f"Difference       : {-diff:9.7f}")
            if diff > 1e-7:
                best_avg = avg_loss
                best_epoch = at_epoch
            elif at_epoch - best_epoch > 3:
                print(f"Not doing better for 3 epochs in a row. Stopping training.")
                break
            last_avg = avg_loss

            if b_save_intermediate and model_name is not None:
                torch.save(model, os.path.join(Config.MODELS_DIR, lang.value + '_' + model_name
                                               + f"_{at_epoch}.pt"))
                torch.save(model.state_dict(), os.path.join(Config.MODELS_DIR,
                                                            model_name + f"_{at_epoch}_state_dict.pt"))

        print("-"*60)
        print(f"Best average loss: {best_avg:9.7f} @ epoch {best_epoch}\n")

        if model_name is not None:
            model_path = os.path.join(Config.MODELS_DIR, "Paper", lang.value + '_' + model_name + "_final.pt")
            torch.save(model, model_path)
            # torch.save(model.state_dict(), os.path.join(RadixAIConfig.MODELS_DIR, "Paper", lang.value + '_' + model_name
            #                                             + "_final_state_dict.pt"))
            print(f"Saved model to: {model_path}")

    train(model=model, nb_epochs=100, print_every=25, model_name="att3_cpnet_baseline")
