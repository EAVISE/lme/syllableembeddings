"""
Positional Encoding network module to be used with Transformer networks, as taken from
http://nlp.seas.harvard.edu/2018/04/03/attention.html
"""
import torch
import torch.nn as nn
import math


class PositionalEncoder(nn.Module):
    def __init__(self, emb_dim, dropout=0.1, max_len=5000):
        super(PositionalEncoder, self).__init__()
        self.dropout = nn.Dropout(p=dropout)

        pe = torch.zeros(max_len, emb_dim)
        position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
        div_term = torch.exp(torch.arange(0, emb_dim, 2).float() * (-math.log(10000.0) / emb_dim))
        pe[:, 0::2] = torch.sin(position * div_term)
        pe[:, 1::2] = torch.cos(position * div_term)
        # pe = pe.unsqueeze(0).transpose(0, 1)
        # print(f"pe.shape: {pe.shape}")

        pe = nn.functional.normalize(pe, p=2, dim=1)

        self.register_buffer('pe', pe)

    def forward(self, x, dim=0):
        """

        :param x: input tensor
        :param dim: dimension in tensor representing the sequence length
        :return:
        """
        # print(f"x.size({dim}) = {x.size(dim)}")
        # print(f"self.pe[:x.size(dim), :].size: {self.pe[:x.size(dim), :].size()}")
        x = x + self.pe[:x.size(dim), :]/x.size(dim)
        return self.dropout(x)
