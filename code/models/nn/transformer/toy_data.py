"""
ToyData set to test Transformer model.
"""
import numpy as np
import torch
from torch.utils.data import Dataset


class ToyData(Dataset):
    def __init__(self, n_elems=1000, min=0, max=9, shift=1):
        # Generate n_elems source-target pairs, where each source consists of a sequence of 3
        # consecutive numbers between 0 and 'max' (roundabout), and the target is the same sequence
        # shifted by 'shift'
        source = []
        targets = []
        for _ in range(n_elems):
            start = np.random.randint(min, max)
            s = torch.Tensor([(start+i) % max for i in range(3)]).long()
            t = torch.Tensor([max] + [(start+shift+i) % max for i in range(3)]).long()
            source.append(s)
            targets.append(t)

        self.source = source
        self.targets = targets
        self.sos = max  # 'start-of-sequence' character
        self.nb_chars = (max - min) + 1  # '+1' for 'start-of-sequence' character

    def __getitem__(self, item: int) -> (torch.Tensor, torch.Tensor):
        """
        Retrieves an item for training.
        :param item: Item index
        :return: Return item in the form (x, y). Where x is a vector of features, and y are the ground truth values to
        be predicted.
        """

        return self.source[item], self.targets[item]

    def __len__(self) -> int:
        """
        Return the length of the dataset.
        :return: The length of the dataset.
        """
        assert len(self.source) == len(self.targets)
        return len(self.source)
