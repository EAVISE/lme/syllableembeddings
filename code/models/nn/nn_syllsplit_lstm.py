import torch
import torch.nn as nn
import torch.nn.functional as F

from config import Config


class EncoderRNN(nn.Module):
    def __init__(self, input_size, emb_dim, hidden_size, nb_layers=1, bidirectional=False, dropout=0.25):
        super(EncoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.nb_layers = nb_layers
        self.bidirectional = bidirectional

        self.embedding = nn.Embedding(input_size, emb_dim)
        self.do_bridge = (emb_dim != hidden_size)
        self.bridge = nn.Linear(emb_dim, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=nb_layers, bidirectional=bidirectional, dropout=dropout)

    def forward(self, input, hidden):
        embedded = self.embedding(input).view(1, 1, -1)
        output = embedded
        output, hidden = self.gru(output, hidden)
        return output, hidden

    def initHidden(self):
        return torch.zeros(2*self.nb_layers if self.bidirectional else self.nb_layers,
                           1, self.hidden_size, device=Config.DEVICE)


class DecoderRNN(nn.Module):
    def __init__(self, hidden_size, output_size, nb_layers=1, dropout=0.25):
        super(DecoderRNN, self).__init__()
        self.hidden_size = hidden_size
        self.nb_layers = nb_layers

        self.embedding = nn.Embedding(output_size, hidden_size)
        self.gru = nn.GRU(hidden_size, hidden_size, num_layers=nb_layers, dropout=dropout)
        self.out = nn.Linear(hidden_size, output_size)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        output = self.embedding(input).view(1, 1, -1)
        output = F.relu(output)
        output, hidden = self.gru(output, hidden)
        output = self.softmax(self.out(output[0]))
        return output, hidden

    def initHidden(self):
        return torch.zeros(self.nb_layers, 1, self.hidden_size, device=Config.DEVICE)


class NNSyllSplitGRU(nn.Module):
    def __init__(self, nb_chars: int, start_token: int, stop_token: int, emb_dim: int, hid_dim=300, max_len=25,
                 b_bidirectional=False):
        super(NNSyllSplitGRU, self).__init__()
        self.nb_chars = nb_chars
        self.hid_dim = hid_dim
        self.start_token, self.stop_token = start_token, stop_token
        self.max_len = max_len
        self.num_layers = 1
        self.embeddings = nn.Embedding(num_embeddings=nb_chars, embedding_dim=emb_dim)
        self.b_bridge = (emb_dim != hid_dim)
        self.l_bridge = nn.Linear(emb_dim, hid_dim) if self.b_bridge else nn.Identity()

        self.nb_dirs = 2 if b_bidirectional else 1
        self.encoder = nn.GRU(input_size=hid_dim, hidden_size=hid_dim, num_layers=self.num_layers, batch_first=True,
                              bidirectional=b_bidirectional)
        self.decoder = nn.GRU(input_size=hid_dim, hidden_size=hid_dim, num_layers=self.num_layers, batch_first=True)
        self.l_out = nn.Linear(in_features=hid_dim, out_features=nb_chars)
        self.soft_max = nn.Softmax(dim=1)

        self.device = torch.device('cpu')

    def set_device(self):
        self.device = next(self.parameters()).device
        print(f"{self.__class__.__name__}: Set device to {self.device}")

    def forward(self, x, b_debug=False):
        if b_debug:
            print("*" * 60)
            print(f"x.shape = {x.shape}")
        embeds = self.embeddings(x)
        if b_debug:
            print(f"embeds_shape: {embeds.shape}")
        embeds = self.l_bridge(embeds)
        if b_debug:
            print(f"bridged_embeds_shape: {embeds.shape}")
        h0 = torch.zeros(self.num_layers, x.shape[0], self.hid_dim).to(self.device).float()
        out, hn = self.encoder(embeds, h0)
        if b_debug:
            print(f"out.shape: {out.shape}")
            print(f"hn.shape: {hn.shape}")

        # out_seq = F.one_hot(torch.as_tensor(self.start_token), self.nb_chars).float().to(self.device)
        out_seq = F.one_hot(torch.as_tensor(self.start_token), self.nb_chars).float().to(self.device).view(1, -1)
        t_in =\
            self.l_bridge(self.embeddings(torch.as_tensor(self.start_token).to(self.device))).view(1, 1, -1)
        if b_debug:
            print(f"out_seq.shape: {out_seq.shape}")
            print(f"t_in.shape: {t_in.shape}")

        out_char_idx = self.start_token
        while out_char_idx != self.stop_token and out_seq.shape[0] < self.max_len:
            out, hid = self.decoder.forward(t_in, hn)
            if b_debug:
                print(f"out.shape: {out.shape}")
                print(f"hid.shape: {hid.shape}")
            out = self.l_out(out[:, -1, :])
            out_char_idx = out.argmax(dim=1).item()
            if b_debug:
                print(f"projected out.shape: {out.shape}")
                print(f"out_char_idx: {out_char_idx}")
            # t_in = F.one_hot(torch.as_tensor(out_char_idx), self.nb_chars).float().view(1, -1).to(self.device)
            t_in = self.l_bridge(self.embeddings(torch.as_tensor(out_char_idx).to(self.device))).view(1, 1, -1)
            out_seq = torch.cat([out_seq, out])

            if b_debug:
                print(f"t_in.shape: {t_in.shape}")
                print(f"out_seq.shape: {out_seq.shape}")
                print("-"*60)

        return out_seq if self.training else self.soft_max(out_seq)


if __name__ == '__main__':
    nn = NNSyllSplitGRU(nb_chars=27, emb_dim=64, hid_dim=256, start_token=1, stop_token=10).to(Config.DEVICE)
    nn.set_device()
    a = torch.as_tensor(data=[[1, 0, 5, 1, 7]]).cuda()
    print(f"a = {a}")
    print(nn.forward(a, b_debug=True).shape)
