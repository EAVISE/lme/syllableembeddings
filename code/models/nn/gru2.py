"""
Same as GRU1, but with a bi-directional GRU.
"""
import torch
import torch.nn as nn
from config import Config


class GRU2(nn.Module):
    def __init__(self, nb_input: int, nb_output: int, expand_size=64, embed_size=300):
        super(GRU2, self).__init__()
        if nb_input % 4 != 0:
            raise ValueError(f"Input dimension is expected to be divisible by 4, which {nb_input} isn't.")

        self.token_dim = nb_input//4

        self.relu = nn.ReLU().to(Config.DEVICE)
        self.elu = nn.ELU().to(Config.DEVICE)
        # self.sig = nn.Sigmoid().to(RadixAIConfig.DEVICE)
        self.softmax = nn.Softmax(dim=1).to(Config.DEVICE)

        self.hidden_dim = expand_size

        self.l_tokens = [nn.Linear(self.token_dim, expand_size).to(Config.DEVICE) for _ in range(4)]
        self.gru = nn.GRU(input_size=expand_size, hidden_size=self.hidden_dim,
                          bidirectional=True, batch_first=True).to(Config.DEVICE)

        self.l_embed = nn.Linear(2*expand_size, embed_size).to(Config.DEVICE)
        self.out = nn.Linear(embed_size, nb_output).to(Config.DEVICE)

    def forward(self, input: torch.Tensor, b_train=False):
        output = self.decode(self.encode(input), b_train)

        return output

    def encode(self, input: torch.Tensor):
        dim = len(input.shape)-1
        ins = torch.chunk(input, chunks=4, dim=dim)

        outs = [self.l_tokens[i](ins[i]) for i in range(4)]

        # h0 -> shape: (num_layers * num_directions, batch, hidden_size)
        h0 = torch.zeros(2, input.shape[0], self.hidden_dim, dtype=torch.float).to(Config.DEVICE)

        t = torch.stack(outs, dim=dim)
        # out -> shape: (batch, seq_len, num_directions * hidden_size)
        # hn -> shape: (batch, num_layers * num_directions, hidden_size)
        out, hn = self.gru(t, h0)
        out = self.elu(self.l_embed(self.relu(out[:, -1, :])))

        return out

    def decode(self, input, b_train=False):
        out = self.out(input)

        return out if b_train else self.softmax(out)
