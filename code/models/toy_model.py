from datetime import datetime
import os
from collections import defaultdict, Counter
import dill

import numpy as np
import torch

from eval.wordsim353 import WordSim353
from models.nn.gru1 import GRU1
from models.nn.gru2 import GRU2
from models.nn.mlp2 import MLP2
from models.nn.mlp3 import MLP3
from config import Config

from models.nn.mlp import MLP
from models.scenarios.scenario_toy import ScenarioToy
from tools.tools import Tools


def embed(model: torch.nn.Module):
    # Use model to obtain embeddings
    print("Creating embeddings...")
    vecs = {}
    vec_item = Counter()

    files = os.listdir(Config.TEMP_DIR)
    nb_batches = len(files)
    at_batch = 0
    for file in files:
        at_batch += 1
        if at_batch % 50 == 0:
            print(f'\rAt batch: {at_batch}/{nb_epochs*nb_batches}', end='')
        batch = dill.load(open(os.path.join(Config.TEMP_DIR, file), 'rb'))

        input_t = torch.from_numpy(np.vstack([batch[x][0] for x in range(len(batch))]).astype(np.float)).float().to(
            Config.DEVICE)
        target = [scen.voc.voc[batch[x][1]] for x in range(len(batch))]

        with torch.no_grad():
            out = model.encode(input_t).detach().cpu().numpy()

            for i, word in enumerate(target):
                vec = out[i, :]
                vec_item[word] += 1
                if word in vecs:
                    vecs[word] = Tools.inc_avg(vecs[word], vec, vec_item[word])
                else:
                    vecs[word] = vec

        if at_batch == nb_epochs*nb_batches:
            break
    print(f'\rAt batch: {at_batch}/{nb_epochs*nb_batches}')

    print(f"Spearman Rank Correlation with WS353: {WordSim353().compute_score(vecs, b_ignore_case=True)}")

    return vec_item, vecs


date_start = datetime.now()

scen = ScenarioToy(size=50000, min_occ=50,
                   save_file=os.path.join(Config.DATA_DIR, "voc_full_top100k.dill"), b_overwrite=False)

# build the model
seq = MLP2(nb_input=4*scen.bits, nb_output=scen.size, expand_size=32, embed_size=200)
model_name = "mlp2_new"
# seq = MLP3(nb_input=4*scen.bits, nb_output=scen.size, expand_size=32, embed_size=200)
# model_name = "mlp3"
# seq = GRU2(nb_input=4*scen.bits, nb_output=scen.size, expand_size=192, embed_size=200)
# model_name = "gru2"

# criterion = nn.MSELoss()
# criterion = nn.BCELoss()
criterion = torch.nn.CrossEntropyLoss()

optimizer = torch.optim.Adam(seq.parameters())
batch_size = 2500
nb_epochs = 1
nb_batches = 100000
save_every = 2500
embed_every = 500

best_loss = np.PINF
since_best = 0
test_best_loss = np.PINF
test_since_best = 0
diff_loss = np.PINF
# clear temp dir
[os.remove(os.path.join(Config.TEMP_DIR, f)) for f in os.listdir(Config.TEMP_DIR)]

b_load_checkpoint = False
if b_load_checkpoint:
    load_model = "train_mlp3_2000.torch"
    checkpoint = torch.load(os.path.join(Config.MODELS_DIR, load_model))
    seq.load_state_dict(checkpoint['model_state_dict'])
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
    skip_batches = checkpoint['batch']
    loss = checkpoint['loss']
    del checkpoint

# begin to train
for i in range(1, nb_epochs+1):
    at_batch = 0
    for batch in scen.get_random_batch(size=batch_size):
        at_batch += 1

        # Write batch to binary file, if it doesn't already exist
        batch_file = os.path.join(Config.TEMP_DIR, f"batch_{at_batch}.dill")
        if not os.path.exists(batch_file):
            dill.dump(batch, open(batch_file, 'wb'))

        print(f'At epoch {i}/{nb_epochs} -- batch: {at_batch}/{nb_batches}')
        if b_load_checkpoint and at_batch <= skip_batches:
            continue

        input_t = torch.from_numpy(np.vstack([batch[x][0] for x in range(len(batch))]).astype(np.float)).float().to(Config.DEVICE)
        # target = nn.functional.one_hot(
        #     torch.tensor([batch[x][1] for x in range(len(batch))], dtype=int),
        #     num_classes=scen.size).float().to(RadixAIConfig.DEVICE)
        target = torch.tensor([batch[x][1] for x in range(len(batch))]).to(Config.DEVICE)

        def closure():
            optimizer.zero_grad()
            out = seq(input_t, b_train=True)
            loss = criterion(out, target)
            print('loss:', loss.item())
            loss.backward()

            return loss

        ep_loss = optimizer.step(closure).item()
        # # begin to predict, no need to track gradient here
        # with torch.no_grad():
        #     future = 0
        #     pred = seq(input_t)
        #     loss = criterion(pred, target)
        #     print('test loss:', loss.item())
        #     if loss.item() < test_best_loss:
        #         test_best_loss = loss.item()
        #         test_since_best = 0
        #     else:
        #         test_since_best += 1
        #     y = Tensor.cpu(pred).detach().numpy()

        if at_batch % save_every == 0:
            torch.save({
                'batch': at_batch,
                'model_state_dict': seq.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(),
                'loss': ep_loss,
            }, os.path.join(Config.MODELS_DIR, f"train_{model_name}_{at_batch}.torch"))
        if at_batch % embed_every == 0:
            embed(seq)
        if at_batch == nb_batches:
            break


date_lapse = datetime.now()
print(f"\nTime used: {date_lapse - date_start}\n")
vec_item, vecs = embed(seq)
print(f"\nTime used: {datetime.now() - date_lapse}\n")

ref_word = ""
while ref_word != "STOP":
    print("Enter word: ", end='')
    ref_word = input()
    if ref_word not in vecs:
        print("No vector known for word. Choose a different word.")
        continue

    distances = Counter()
    for word in vecs.keys():
        if word == ref_word:
            continue
        distances[word] = Tools.cosim(vecs[ref_word], vecs[word])

    for word, dist in distances.most_common(10):
        print(f"[{word:20s}] :: {vec_item[word]:3d} :: {dist}")
