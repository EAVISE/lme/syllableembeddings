import os

import dill
import torch

from config import Config


class SplitModelRNNLoader:
    def __init__(self, path_to_model: str):
        import warnings
        warnings.filterwarnings('ignore')
        self.encoder, self.decoder, self.train_data = dill.load(open(path_to_model, "rb"))

    def split_word(self, word: str):
        input_test = self.train_data.word_to_torch(word).long().to(Config.DEVICE)
        encoder_hidden = self.encoder.initHidden()
        for ei in range(input_test.shape[0]):
            encoder_output, encoder_hidden = self.encoder(
                input_test[ei], encoder_hidden)

        decoder_input = torch.tensor([[self.train_data.abc_map[self.train_data.START_CHAR]]],
                                     device=Config.DEVICE)
        decoder_hidden = encoder_hidden.view(self.decoder.nb_layers, 1, -1) if self.encoder.bidirectional \
            else encoder_hidden
        res = []
        while True:
            decoder_output, decoder_hidden = self.decoder(decoder_input, decoder_hidden)
            topv, topi = decoder_output.topk(1)
            decoder_input = topi.squeeze().detach()  # detach from history as input
            res.append(topi[0].item())
            if len(res) > 50:
                break
            if decoder_input.item() == self.train_data.abc_map[self.train_data.STOP_CHAR]:
                break

        # Last character is STOP character; cut off.
        res = ''.join([self.train_data.cba_map[x] for x in res])[:-1]

        parts = res.split('/')

        return parts


if __name__ == '__main__':
    sml = SplitModelRNNLoader(os.path.join(Config.MODELS_DIR, 'Paper',
                                           'nl_syll_splitter_transformer_em64_hid1x256_head8_epoch0.torch'),)
    print("Enter a word to get its syllabic decomposition: ", end='')
    tok = input()
    while tok != "EXIT":
        sylls = sml.split_word(tok)
        print(f"{tok}\t: {sylls}")

        print("============================================================")
        print("Enter a word to get its syllabic decomposition: ", end='')
        tok = input()
