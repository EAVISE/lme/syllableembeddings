import os

import dill
import torch

from config import Config, Lang


class SplitModelTFLoader:
    def __init__(self, base_path_to_model: str, model_version='_final', lang=Lang.EN, device=torch.device('cuda')):
        # import warnings
        # warnings.filterwarnings('ignore')
        self.device = device
        self.model = torch.load(open(base_path_to_model + f'_{model_version}.torch', 'rb'), map_location=device)
        if lang == Lang.EN:
            self.train_data = dill.load(open(os.path.join(Config.MODELS_DIR, 'Paper',
                                                          'en_syll_splitter_train_data_full.dill'), 'rb'))
        elif lang == Lang.NL:
            self.train_data = dill.load(open(os.path.join(Config.MODELS_DIR, 'Paper',
                                                          'nl_syll_splitter_train_data_full.dill'), 'rb'))
        # self.encoder, self.decoder, self.train_data = dill.load(open(path_to_model, "rb"))

    def split_word(self, word: str):
        eval_data = self.train_data.word_to_torch(word, b_as_one_hot=False)
        eval_data = eval_data.unsqueeze(0)
        greedy = self.greedy_decode(src=eval_data.long().to(self.device),
                                    max_len=len(word) + 8,
                                    start_symbol=self.train_data.abc_map[self.train_data.START_CHAR],
                                    stop_symbol=self.train_data.abc_map[self.train_data.STOP_CHAR])
        res = self.train_data.idxs_to_word(greedy)[1:-1]

        return res.split('/')

    def greedy_decode(self, src, max_len=4, start_symbol=0, stop_symbol=None):
        # print(f"src.shape: {src.shape}")
        with torch.no_grad():
            memory = self.model.enc_forward(src)
        # print(f"memory.shape: {memory.shape}")
        idxs = [start_symbol]
        next_char = start_symbol
        while next_char != stop_symbol:
        # for i in range(max_len-1):
            if len(idxs) == max_len:
                # print("Breaking due to max_len")
                break
            # print(idxs)
            ys = torch.LongTensor(idxs).unsqueeze(0).to(self.device)
            # print(f"ys.shape: {ys.shape}")
            with torch.no_grad():
                out = self.model.dec_forward(memory=memory, tgt=ys)
            # print(f"out.shape: {out.shape}")
            # print(f"out[-1,:,:]: {out[-1,:,:]}")
            # print(torch.sum(out[-1,:,:], dim=1))
            # print(f"torch.max(out[-1,:,:], dim=1): {torch.max(out[-1,:,:], dim=1)}")
            _, next_char = torch.max(out[-1, :, :], dim=1)
            next_char = next_char.item()
            # print(f"next_word: {next_word}")
            idxs.append(next_char)
            # print(f"ys: {ys}")

        return idxs


if __name__ == '__main__':
    # sml = SplitModelTFLoader(base_path_to_model=os.path.join(RadixAIConfig.MODELS_DIR,
    #                                                          'Paper', 'nl_syll_splitter_transformer_em64_hid1x256_head8'),
    #                          model_version='epoch0', device=torch.device('cuda'))
    sml = SplitModelTFLoader(base_path_to_model=os.path.join(Config.MODELS_DIR,
                                                             'Paper', 'en_syll_splitter_transformer_em64_hid1x256_head8'),
                             model_version='epoch4', device=torch.device('cuda'))

    print("Enter a word to get its syllabic decomposition: ", end='')
    tok = input()
    while tok != "EXIT":
        sylls = sml.split_word(tok)
        print(f"{tok}\t: {sylls}")

        print("============================================================")
        print("Enter a word to get its syllabic decomposition: ", end='')
        tok = input()
