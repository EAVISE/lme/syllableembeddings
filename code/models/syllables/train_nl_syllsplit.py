import math
import numpy as np

import torch
import torch.nn as nn
import torch.utils.data as data_utils

from models.nn.nn_syllsplit_transformer import NNSyllSplitTransformer
from models.nn.nn_syllsplit_lstm import NNSyllSplitGRU
from models.syllables.splitter_dataset import Seq2SeqSplitterDataset
from config import Config
from syllables.nlwiktionary_syllables import NLWiktionarySyllables


nlwikt_sylls = NLWiktionarySyllables()
nl_uq_words = sorted(nlwikt_sylls.data.keys())
idxs = np.random.choice(len(nl_uq_words), 100000, False)
train_words = {nl_uq_words[i]: nlwikt_sylls.data[nl_uq_words[i]] for i in idxs}

# Create data Loader
train_data = Seq2SeqSplitterDataset(data=train_words, max_len_out=100, b_return_one_hot=False)
train_loader = data_utils.DataLoader(train_data, batch_size=1, shuffle=True, drop_last=True)

ntokens = train_data.nb_chars  # the size of vocabulary
model = NNSyllSplitGRU(ntokens,
                       start_token=train_data.abc_map[train_data.START_CHAR],
                       stop_token=train_data.abc_map[train_data.STOP_CHAR],
                       emb_dim=64, hid_dim=128, max_len=100).to(Config.DEVICE)
model.set_device()

criterion = nn.CrossEntropyLoss()
lr = 1  # learning rate
optimizer = torch.optim.ASGD(model.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)


def train():
    model.train()  # Turn on the train mode
    total_loss = 0.

    for at_batch, data in enumerate(train_loader):
        batch_input, batch_target = data
        batch_input = batch_input.to(Config.DEVICE)
        batch_target = batch_target[0].to(Config.DEVICE)

        optimizer.zero_grad()
        output = model(batch_input, b_debug=False)[1:, :]
        # print(f"Train.output.shape: {output.shape}")

        d = min(batch_target.shape[0], output.shape[0])
        loss = criterion(output[:d, :], batch_target[:d])
        loss.backward()
        # torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
        optimizer.step()

        total_loss += loss.item()
        log_interval = 200
        if at_batch % log_interval == 0 and at_batch > 0:
            cur_loss = total_loss / log_interval
            print('| epoch {:3d} | {:5d}/{:5d} batches | '
                  'lr {:02.2f} | '
                  'loss {:5.2f} | ppl {:8.2f}'.format(
                    epoch, at_batch, len(train_data), scheduler.get_lr()[0],
                    cur_loss, math.exp(cur_loss)))
            total_loss = 0

            model.eval()
            input_test = train_data.word_to_torch('hallo', b_as_one_hot=False).long()
            print(input_test)
            # input_test = input_test.view(1, input_test.shape[0], -1)
            print(f"input_test.shape: {input_test.shape}")
            test = model.forward(input_test.view(1, -1).to(Config.DEVICE))
            print(f"test.shape: {test.shape}")
            # print(test[0])
            # print(test)
            print(train_data.torch_to_word(test))
            model.train()

# def evaluate(eval_model, data_source):
#     eval_model.eval() # Turn on the evaluation mode
#     total_loss = 0.
#     ntokens = len(TEXT.vocab.stoi)
#     with torch.no_grad():
#         for i in range(0, data_source.size(0) - 1, bptt):
#             data, targets = get_batch(data_source, i)
#             output = eval_model(data)
#             output_flat = output.view(-1, ntokens)
#             total_loss += len(data) * criterion(output_flat, targets).item()
#     return total_loss / (len(data_source) - 1)


best_val_loss = float("inf")
epochs = 5  # The number of epochs
best_model = None

for epoch in range(1, epochs + 1):
    train()
    # val_loss = evaluate(model, val_data)
    # print('-' * 89)
    # print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
    #       'valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
    #                                  val_loss, math.exp(val_loss)))
    # print('-' * 89)
    #
    # if val_loss < best_val_loss:
    #     best_val_loss = val_loss
    #     best_model = model

    scheduler.step()
