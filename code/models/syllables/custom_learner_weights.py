"""
This doesn't work.
The idea was to give a weight to each syllable, and let that weight be trained by the network, but in practice,
the weights don't change from their initial values.
"""
import torch

from config import Config


class CustomLearnerWeights(torch.nn.Module):
    def __init__(self, embeddings):
        super(CustomLearnerWeights, self).__init__()
        print(f"Embeddings shape: {embeddings.shape}")
        self.embeddings = torch.nn.Parameter(data=embeddings, requires_grad=True)
        self.weights = torch.nn.Parameter(data=1+0.1*torch.abs(torch.randn(1, embeddings.shape[0])), requires_grad=True)\
            .to(Config.DEVICE)

    def forward(self, input: torch.Tensor):
        """
        The input should be a NxD tensor, with D the dimension of the embeddings, with 1s in those places that
        correspond to the indices in self.embeddings of the embeddings to sum.
        """
        res = torch.matmul(torch.mul(input, self.weights), self.embeddings)
        print(f"Shape res: {res.shape}")
        res = torch.nn.functional.normalize(res, p=2, dim=1)

        return res
