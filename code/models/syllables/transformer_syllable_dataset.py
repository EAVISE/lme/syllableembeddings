"""
A pytorch Dataset for use with a Transformer Encoder model that aims to learn the syllable embeddings from
pretrained embeddings.
"""
from collections import Counter

import torch

from eval.semeval2017 import SemEval2017
from eval.wordsim353 import WordSim353
from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from config import Lang, Config
from syllables.en_syllables import ENSyllables
from syllables.en_syllables2 import ENSyllables2
from syllables.nlwiktionary_syllables import NLWiktionarySyllables
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2


class TransformerSyllableDataset(torch.utils.data.Dataset):
        def __init__(self, lang: Lang, nb_to_load=500000, max_len=10, b_pad=False, f_start=0., f_end=0.,
                     b_no_wordsim=False, b_no_semeval=False, thres_cts=3, cnet=None):
            """

            :param lang:
            :param nb_to_load:
            :param max_len: max number of syllables word can have
            :param b_pad: pad words
            :param f_start:
            :param f_end:
            :param b_no_wordsim: don't include words from WordSim evaluation set
            :param b_no_semeval: don't include words frmo SemEval evaluation set
            """
            super(TransformerSyllableDataset, self).__init__()
            if lang != Lang.EN and lang != Lang.NL:
                raise NotImplementedError(f"Language {lang.value} is not supported as yet.")

            self.max_len = max_len
            self.b_pad = b_pad

            # Load pretrained embeddings
            if cnet is None:
                cnet = ConceptNetVecs(lang=lang, nb_to_load=nb_to_load)
            # Load syllabic decompositions
            if lang == Lang.EN:
                self.sylls = ENSyllables2(overlap_with_vecs=cnet.data, f_start=f_start, f_end=f_end)
            elif lang == Lang.NL:
                self.sylls = NLWiktionarySyllables2(overlap_with_vecs=cnet.data, f_start=f_start, f_end=f_end)
            self.nb_sylls = len(self.sylls.uq_sylls)
            # Get overlap between the two
            # words = set(cnet.data.keys()).intersection(set(self.sylls.data.keys()))
            words = set(self.sylls.data.keys())
            if b_no_wordsim:
                # How many times does each syllable appear?
                ct_sylls = Counter()
                for k, v in self.sylls.data.items():
                    for e in v:
                        ct_sylls.update(e)

                ws = WordSim353(ws_file=Config.EN_WORDSIM353_FILE if lang == Lang.EN else
                                        Config.NL_WORDSIM353_FILE)
                removed = 0
                for w in ws.words:
                    if w.lower() in words:
                        # Get syllabic decomposition for word
                        w_sylls = self.sylls.get_syllabic_decomposition_for_word(w.lower())
                        b_skip = False
                        for e in w_sylls:
                            for s in e:
                                if ct_sylls[s] < thres_cts:
                                    # print(f"Syllable {s} appears only {ct_sylls[s]} times. Keeping word {w}.")
                                    b_skip = True
                                    break
                            if b_skip:
                                break
                        if b_skip:
                            continue

                        removed += 1
                        words.remove(w.lower())
                print(f"{self.__class__.__name__}: Removed {removed} WordSim words.")

            if b_no_semeval:
                # How many times does each syllable appear?
                ct_sylls = Counter()
                for k, v in self.sylls.data.items():
                    for e in v:
                        ct_sylls.update(e)

                se = SemEval2017(lang=lang)
                removed = 0
                for w in se.words:
                    if w.lower() in words:
                        # Get syllabic decomposition for word
                        w_sylls = self.sylls.get_syllabic_decomposition_for_word(w.lower())
                        b_skip = False
                        for e in w_sylls:
                            for s in e:
                                if ct_sylls[s] < thres_cts:
                                    # print(f"Syllable {s} appears only {ct_sylls[s]} times. Keeping word {w}.")
                                    b_skip = True
                                    break
                            if b_skip:
                                break
                        if b_skip:
                            continue

                        removed += 1
                        words.remove(w.lower())
                print(f"{self.__class__.__name__}: Removed {removed} SemEval2017 words.")

            self.words = list(words)
            self.nb_words = len(self.words)
            # Save some memory, and only keep pretrained embeddings for overlap
            self.embeddings = {word: cnet.get_vec_for_word(word) for word in self.words}
            del cnet  # Not really necessary, since we go out of scope, but still...

        def __getitem__(self, idx):
            word = self.words[idx]

            return self.get_word(word)

        def __len__(self):
            return self.nb_words

        def get_word(self, word: str):
            # Get syllabic decomposition as idxs
            idxs = self.sylls.decomp_to_idx(self.sylls.get_syllabic_decomposition_for_word(word)[0])
            # Pad if necessary; index with value self.nb_sylls is used as filter idx in PyTorch Embedding object.
            if self.b_pad and len(idxs) < self.max_len:
                idxs += [self.nb_sylls] * (self.max_len - len(idxs))
            src = torch.Tensor(idxs).long()
            # src = torch.zeros(self.nb_sylls).long()
            # src[idxs] = 1
            tgt = torch.from_numpy(self.embeddings[word])

            return src, tgt


if __name__ == '__main__':

    lang = Lang.EN
    max_len = 20
    batch_size = 250
    f_start_end = 0
    thres_cts = 3
    cnet = ConceptNetVecs(lang=lang, nb_to_load=-1)

    for f_start_end in [0, 10, 25, 50, 75, 100]:
        print(f"f_start_end: {f_start_end}")
        train_data = TransformerSyllableDataset(lang=lang, max_len=max_len, nb_to_load=-1,
                                                b_pad=(batch_size > 1), f_start=f_start_end / 100, f_end=f_start_end / 100,
                                                b_no_wordsim=True, b_no_semeval=False, thres_cts=thres_cts, cnet=cnet)
        print("*" * 60)
