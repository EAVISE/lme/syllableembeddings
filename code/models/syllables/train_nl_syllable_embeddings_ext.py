"""
Extend the train dataset by using all possible syllabic decompositions for known words, not just the ones extracted
from Wiktionary.
!!! This doesn't work !!! Was worth trying, though...
"""
import math
import os

import dill
import numpy as np
import torch
import torch.utils.data as data_utils

from models.syllables.custom_dataset import CustomDataset
from models.syllables.custom_learner import CustomLearner
from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from config import Config, Lang
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2
from tools.tools import Tools
from syllables.nlwiktionary_syllables import NLWiktionarySyllables


def numpy_to_dict(sylls, vecs):
    res = {}
    for i in range(len(sylls)):
        res[sylls[i]] = vecs[i, :]
    return res


class TrainNLSyllableEmbeddingsExt:
    def __init__(self):
        self.cnnl = ConceptNetVecs(lang=Lang.NL)
        self.wikt = NLWiktionarySyllables2(overlap_with_vecs=self.cnnl.data)

        # For which words do we have syllables?
        self.overlap = sorted(set(self.cnnl.get_words()).intersection(set(self.wikt.data.keys())))

    def train(self, output_dir=os.path.join(Config.PRETRAINED_VECS_DIR),
              model_name="torch_norm_ext_syll_vecs",
              save_every=5):
        """
        Train embeddings for those syllables that make up the words from the NL Wiktionary for which we could extract
        the syllabic decomposition.

        :param output_dir: directory into which model(s) will be saved
        :param model_name: generic model name (will be appended with some extra epoch information)
        :param save_every: number of epochs after which to save training in progress
        :return:
        """
        model = CustomLearner(embeddings=torch.rand((len(self.wikt.uq_sylls), 300), dtype=torch.float32)
                              .to(Config.DEVICE))

        #####################
        # Train model
        #####################
        num_epochs = 100
        hist, hist_test = [], []

        # Create n-hot representations of syllabic decompositions
        print("Creating training set...")
        words = []
        emb_idxs = []
        at_word = 0
        for word in self.overlap:
            at_word += 1
            if len(word) > 15:
                continue
            decomps = self.wikt.get_all_syllabic_decompositions_for_word(word)
            print(f"\r{at_word:5d} :: {word} --> {len(decomps)}", end='')
            idxs = []
            for decomp in decomps:
                for syll in decomp:
                    pos = Tools.binary_search(self.wikt.uq_sylls, syll)
                    if pos < 0:
                        raise ValueError("This is not gonna work...")
                    idxs.append(pos)
                words.append(word)
                emb_idxs.append(idxs)

        emb_words = torch.empty((len(words), 300), dtype=torch.float32)
        for i, word in enumerate(words):
            emb_words[i, :] = torch.from_numpy(self.cnnl.get_vec_for_word(word))

        print()

        # Create data Loader
        train_data = CustomDataset(emb_idxs, emb_words, len(self.wikt.uq_sylls))
        train_loader = data_utils.DataLoader(train_data, batch_size=2500, shuffle=True, drop_last=True)

        loss_fn = torch.nn.MSELoss()
        optimiser = torch.optim.Adam(model.parameters(), lr=1e-2)
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimiser, lr_lambda=[lambda epoch: 0.99 ** epoch])

        prev_loss, prev_test_loss = math.inf, math.inf
        for t in range(1, num_epochs+1):
            losses = []
            for at_batch, data in enumerate(train_loader):
                batch_input, batch_target = data
                batch_input = batch_input.to(Config.DEVICE)
                batch_target = batch_target.to(Config.DEVICE)

                def closure():
                    optimiser.zero_grad()
                    # Forward pass
                    output = model.forward(batch_input)
                    loss = loss_fn(output, batch_target)
                    print(f"Epoch {t}.{at_batch:02d}: Loss: {loss.item()}")
                    loss.backward()

                    # Normalize embeddings
                    model.embeddings.data = torch.nn.functional.normalize(model.embeddings.data, p=2, dim=1)

                    # Check maximum gradient value
                    # max = -math.inf
                    # for p in model.parameters():
                    #     m = torch.max(p.grad.data).item()
                    #     if m > max:
                    #         max = m
                    # print(f"Max parameter grad: {max}")

                    return loss.item()

                # Clip gradients
                # nn.utils.clip_grad_norm_(model.parameters(), 1.)
                # Update parameters
                losses.append(optimiser.step(closure))

            # Update learning rate
            scheduler.step(epoch=t)

            # Print losses
            avg_loss = np.average(losses)
            hist.append(avg_loss)
            # # Results on test set
            # model.eval()
            # temp = self._apply_model(model, val_feats)
            # val_output = torch.zeros([len(temp), 3], dtype=torch.float)
            # for i in range(len(temp)):
            #     val_output[i, :] = temp[i]
            # with torch.no_grad():
            #     val_loss = loss_fn(val_output.to(device), val_targets.to(device))
            # hist_test.append(val_loss)
            # model.train()
            print(f"Previous loss: {prev_loss}")
            print(f"Average loss : {avg_loss}")
            # print(f"Prev Val loss: {prev_test_loss}")
            # print(f"Val loss     : {val_loss}")
            print()
            if prev_loss - avg_loss < 0.000001:
                print(f"Difference with previous loss is less than threshold: {prev_loss - avg_loss}")
                break
            prev_loss = avg_loss
            # prev_test_loss = val_loss

            if model_name is not None and t % save_every == 0:
                syll_vecs = numpy_to_dict(self.wikt.uq_sylls, model.embeddings.clone().cpu().detach().numpy())
                dill.dump(syll_vecs, open(os.path.join(output_dir, f"{model_name}_{t}epochs.dill"), "wb"))

        if model_name is not None:
            syll_vecs = numpy_to_dict(self.wikt.uq_sylls, model.embeddings.clone().cpu().detach().numpy())
            dill.dump(syll_vecs, open(os.path.join(output_dir, f"{model_name}_final.dill"), "wb"))


if __name__ == '__main__':
    trainer = TrainNLSyllableEmbeddingsExt()
    trainer.train(model_name=None)
