import math
import os

import dill
import numpy as np
import torch
import torch.utils.data as data_utils

from models.syllables.custom_dataset import CustomDataset
from models.syllables.custom_learner import CustomLearner
from pretrained_vecs.base_embeddings import BaseEmbeddings
from pretrained_vecs.conceptnet_vecs import ConceptNetVecs
from pretrained_vecs.tulkens2016 import Tulkens2016, Embeddings
from config import Config, Lang
from syllables.nlwiktionary_syllables2 import NLWiktionarySyllables2
from tools.tools import Tools


def numpy_to_dict(sylls, vecs):
    res = {}
    for i in range(len(sylls)):
        res[sylls[i]] = vecs[i, :]
    return res


class TrainNLSyllableEmbeddings:
    def __init__(self, embeddings: BaseEmbeddings, f_start=0., f_end=0.):
        """

        :param embeddings: the pre-trained embeddings to use as basis from which to extract the syllable vecs
        """
        self.embeddings = embeddings
        self.embed_size = embeddings.embed_size
        self.wikt = NLWiktionarySyllables2(overlap_with_vecs=embeddings.data, f_start=f_start, f_end=f_end)
        exit()

        # For which words do we have syllables?
        self.overlap = sorted(set(self.embeddings.get_words()).intersection(set(self.wikt.data.keys())))
        # self.overlap = sorted(set(self.embeddings.get_words()).intersection(
        #     set([word.replace(' ', '_') for word in self.wikt.data.keys()])))
        self.nb_overlap = len(self.overlap)
        print(f"Overlapping words: {self.nb_overlap}")

    def train(self, output_dir=os.path.join(Config.PRETRAINED_VECS_DIR),
              model_name=None,
              num_epochs=100):
        """
        Train embeddings for those syllables that make up the words from the NL Wiktionary for which we could extract
        the syllabic decomposition.

        :return:
        """
        # Create embedding tensor for targets --> the full words
        emb_words = torch.empty((self.nb_overlap, self.embed_size), dtype=torch.float32)
        for i, word in enumerate(self.overlap):
            emb_words[i, :] = torch.from_numpy(self.embeddings.get_vec_for_word(word))

        # First, check how many unique syllables there are in the overlap of words
        syllables = set()
        for word in self.overlap:
            # Overlap only contains single words, no compositions
            syllables.update(self.wikt.get_syllabic_decomposition_for_word(word)[0])
        syllables = sorted(syllables)
        nb_syllables = len(syllables)
        print(f"Number of syllables: {nb_syllables}")

        model = CustomLearner(embeddings=torch.rand((nb_syllables, self.embed_size), dtype=torch.float32)
                              .to(Config.DEVICE))
        # model = CustomLearnerWeights(embeddings=torch.rand((nb_syllables, 300), dtype=torch.float32).to(RadixAIConfig.DEVICE))
        nb_params = sum(p.numel() for p in model.parameters())
        print(f"#Model parameters: {nb_params}")
        # exit()

        #####################
        # Train model
        #####################
        hist, hist_test = [], []

        # Create n-hot representations of syllables
        emb_idxs = []
        for word in self.overlap:
            sylls = self.wikt.get_syllabic_decomposition_for_word(word)[0]
            idxs = []
            for syll in sylls:
                pos = Tools.binary_search(syllables, syll)
                if pos < 0:
                    raise ValueError("This is not gonna work...")
                idxs.append(pos)
            emb_idxs.append(idxs)

        # Create data Loader
        train_data = CustomDataset(emb_idxs, emb_words, nb_syllables)
        train_loader = data_utils.DataLoader(train_data, batch_size=2000, shuffle=True, drop_last=True)

        loss_fn = torch.nn.MSELoss()
        lr = 0.05
        optimizer = torch.optim.Adam(model.parameters(), lr=lr, eps=1e-10, amsgrad=True)
        # optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.25, nesterov=True)
        # optimizer = torch.optim.ASGD(model.parameters(), lr=lr)
        scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=[lambda epoch: lr * (0.99 ** epoch)])
        # scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer=optimizer,
        #                                               lr_lambda=[lambda epoch: lr / (int(epoch / 2) + 1)])

        prev_loss, prev_test_loss = math.inf, math.inf
        for t in range(1, num_epochs+1):
            losses = []
            for at_batch, data in enumerate(train_loader):
                batch_input, batch_target = data
                batch_input = batch_input.to(Config.DEVICE)
                batch_target = batch_target.to(Config.DEVICE)

                def closure():
                    optimizer.zero_grad()
                    # Forward pass
                    output = model.forward(batch_input)
                    loss = loss_fn(output, batch_target)
                    print(f"Epoch {t}.{at_batch:02d}: Loss: {loss.item()}")
                    loss.backward()

                    # Normalize embeddings
                    model.embeddings.data = torch.nn.functional.normalize(model.embeddings.data, p=2, dim=1)

                    # Check maximum gradient value
                    # max = -math.inf
                    # for p in model.parameters():
                    #     m = torch.max(p.grad.data).item()
                    #     if m > max:
                    #         max = m
                    # print(f"Max parameter grad: {max}")

                    return loss.item()

                # Clip gradients
                # nn.utils.clip_grad_norm_(model.parameters(), 1.)
                # Update parameters
                losses.append(optimizer.step(closure))

            # Update learning rate
            scheduler.step(epoch=t)

            # Print losses
            avg_loss = np.average(losses)
            hist.append(avg_loss)
            # # Results on test set
            # model.eval()
            # temp = self._apply_model(model, val_feats)
            # val_output = torch.zeros([len(temp), 3], dtype=torch.float)
            # for i in range(len(temp)):
            #     val_output[i, :] = temp[i]
            # with torch.no_grad():
            #     val_loss = loss_fn(val_output.to(device), val_targets.to(device))
            # hist_test.append(val_loss)
            # model.train()
            diff = prev_loss - avg_loss
            print(f"Previous loss: {prev_loss:.8f}")
            print(f"Average loss : {avg_loss:.8f}")
            print(f"Difference   : {diff:.8f}")
            # print(f"Prev Val loss: {prev_test_loss}")
            # print(f"Val loss     : {val_loss}")
            print()
            if diff < 1e-7:
                print(f"Difference with previous loss is less than threshold: {prev_loss - avg_loss}")
                break
            prev_loss = avg_loss
            # prev_test_loss = val_loss

            if t % 25 == 0:
                syll_vecs = numpy_to_dict(syllables, model.embeddings.clone().cpu().detach().numpy())
                dill.dump(syll_vecs, open(os.path.join(output_dir, f"{model_name}_{t}epochs.dill"), "wb"))

        if model_name is not None:
            syll_vecs = numpy_to_dict(syllables, model.embeddings.clone().cpu().detach().numpy())
            dill.dump(syll_vecs, open(os.path.join(output_dir, f"{model_name}_final.dill"), "wb"))
            print(f"Written model to '{os.path.join(output_dir, f'{model_name}_final.dill')}'.")


if __name__ == '__main__':
    trainer = TrainNLSyllableEmbeddings(embeddings=ConceptNetVecs(lang=Lang.NL), f_start=0., f_end=0.)
    trainer.train(output_dir=os.path.join(Config.PRETRAINED_VECS_DIR, "Paper"),
                  model_name="nl2_vanilla_cptnet_start_end")
