"""
Train Transformer Syllable splitter.
But actually, don't use this one. Just kept for archival purposes.
"""
import math
import numpy as np

import torch
import torch.nn as nn
import torch.utils.data as data_utils

from models.nn.nn_syllsplit_transformer import NNSyllSplitTransformer
from models.syllables.splitter_dataset import Seq2SeqSplitterDataset
from config import Config
from syllables.nlwiktionary_syllables import NLWiktionarySyllables


nlwikt_sylls = NLWiktionarySyllables()
nl_uq_words = sorted(nlwikt_sylls.data.keys())
idxs = np.random.choice(len(nl_uq_words), 100000, False)
train_words = {nl_uq_words[i]: nlwikt_sylls.data[nl_uq_words[i]] for i in idxs}

# Create data Loader
train_data = Seq2SeqSplitterDataset(data=train_words)
train_loader = data_utils.DataLoader(train_data, batch_size=1, shuffle=True, drop_last=True)

ntokens = train_data.nb_chars  # the size of vocabulary
emsize = 200  # embedding dimension
nhid = 100  # the dimension of the feedforward network model in nn.TransformerEncoder
nlayers = 2  # the number of nn.TransformerEncoderLayer in nn.TransformerEncoder
nhead = 2  # the number of heads in the multiheadattention models
dropout = 0.2  # the dropout value
model = NNSyllSplitTransformer(ntokens, emsize, nhead, nhid, nlayers, dropout).to(Config.DEVICE)

criterion = nn.CrossEntropyLoss()
lr = 0.05  # learning rate
# optimizer = torch.optim.Adam(model.parameters(), lr=lr)
optimizer = torch.optim.SGD(model.parameters(), lr=lr)
scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1.0, gamma=0.95)

import time
def train():
    model.train()  # Turn on the train mode
    total_loss = 0.
    start_time = time.time()

    for at_batch, data in enumerate(train_loader):
        batch_input, batch_target = data
        batch_input = batch_input.to(Config.DEVICE)
        batch_target = batch_target.view(-1, 1).to(Config.DEVICE)

        optimizer.zero_grad()
        output = model(batch_input)
        # print(output.shape)
        # print(f"Target shape: {batch_target.shape}")
        # print(f"Output shape: {output.view(-1, ntokens).shape}")
        output = output.view(-1, ntokens)

        loss = 0
        for i in range(batch_target.shape[0]):
            if i >= output.shape[0]:
                break
            loss += criterion(output[i].view(1, -1), batch_target[i].long())

        # loss = criterion(output.view(-1, ntokens), batch_target)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)
        optimizer.step()

        total_loss += loss.item()
        log_interval = 200
        if at_batch % log_interval == 0 and at_batch > 0:
            cur_loss = total_loss / log_interval
            elapsed = time.time() - start_time
            print('| epoch {:3d} | {:5d}/{:5d} batches | '
                  'lr {:02.2f} | ms/batch {:5.2f} | '
                  'loss {:5.2f} | ppl {:8.2f}'.format(
                    epoch, at_batch, len(train_data), scheduler.get_lr()[0],
                    elapsed * 1000 / log_interval,
                    cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()

            model.eval()
            input_test = train_data.word_to_torch('hallo')
            input_test = input_test.view(1, input_test.shape[0], -1)
            print(f"input_test.shape: {input_test.shape}")
            test = model.forward(input_test.to(Config.DEVICE))
            print(test.shape)
            print(train_data.torch_to_word(test[-1]))
            model.train()

# def evaluate(eval_model, data_source):
#     eval_model.eval() # Turn on the evaluation mode
#     total_loss = 0.
#     ntokens = len(TEXT.vocab.stoi)
#     with torch.no_grad():
#         for i in range(0, data_source.size(0) - 1, bptt):
#             data, targets = get_batch(data_source, i)
#             output = eval_model(data)
#             output_flat = output.view(-1, ntokens)
#             total_loss += len(data) * criterion(output_flat, targets).item()
#     return total_loss / (len(data_source) - 1)


best_val_loss = float("inf")
epochs = 5  # The number of epochs
best_model = None

for epoch in range(1, epochs + 1):
    epoch_start_time = time.time()
    train()
    # val_loss = evaluate(model, val_data)
    # print('-' * 89)
    # print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
    #       'valid ppl {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
    #                                  val_loss, math.exp(val_loss)))
    # print('-' * 89)
    #
    # if val_loss < best_val_loss:
    #     best_val_loss = val_loss
    #     best_model = model

    scheduler.step()

# test_loss = evaluate(best_model, test_data)
# print('=' * 89)
# print('| End of training | test loss {:5.2f} | test ppl {:8.2f}'.format(
#     test_loss, math.exp(test_loss)))
# print('=' * 89)
