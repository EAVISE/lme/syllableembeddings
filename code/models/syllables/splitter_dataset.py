"""
PyTorch dataset object to load syllabic decompositions for Sequence 2 Sequence training.
The targets are a concatenation of all syllables in a word, separated with a '/'.
E.g.: the target for "airport" = "air" + "port" thus becomes "air/port".
"""

import numpy as np
import torch

from tools.tools import Tools


class Seq2SeqSplitterDataset(torch.utils.data.Dataset):
    SEP_CHAR = '/'
    START_CHAR = '♥'
    STOP_CHAR = '♦'
    PAD_CHAR = '♣'
    FILTER_CHARS = {' ', '.', ',', ';', ':', '!', '&', '’', 'ʼ', '<', '>', '{', '}', '[', ']', '(', ')',
                    '=', '+', '*', '€', 'ɡ', 'γ', '₂', '•', '·',
                    'å', 'č', 'ș', 'ø', 'ò', 'ɔ'}
    ALLOWED_CHARS = {c for c in "abcdefghijklmnopqrstuvwxyz0123456789äàáçëèéêïíîñöóüú'-"}

    def __init__(self, data: dict, max_len_out=50, b_pad=False, b_return_one_hot=True):
        """

        :param data: (k, v): k = word, v = list of syllables
        :param max_len_out: maximum length of output sequence; for input, this will be determined from the data
        :param b_return_one_hot: if True, dataset will return one-hot encodings, else will return long Tensor
         containing idxs
        """
        super(Seq2SeqSplitterDataset, self).__init__()
        self.b_pad = b_pad
        self.b_return_one_hot = b_return_one_hot
        self.data = {}
        self.max_len_in = 0  # Is used to determine padding, if b_pad = True
        for k, v in data.items():
            if self.check(k, v):
                if len(k) > self.max_len_in:
                    self.max_len_in = len(k)
                self.data[k.lower()] = [x.lower() for x in v[0]]
            else:
                print(f"Missed check: {k} -- {v}")
        exit()
        # Foresee start and end characters
        self.max_len_in += 2
        self.max_len_out = max_len_out
        # Get single words
        self.words = sorted(self.data.keys())
        self.nb_words = len(self.words)
        self.abc_map = {chr(97+i): i for i in range(26)}
        for c in "0123456789äàáçëèéêïíîñöóüú'-":
            self.abc_map[c] = len(self.abc_map)
        self.abc_map[self.SEP_CHAR] = len(self.abc_map)
        self.abc_map[self.START_CHAR] = len(self.abc_map)
        self.abc_map[self.STOP_CHAR] = len(self.abc_map)
        self.abc_map[self.PAD_CHAR] = len(self.abc_map)
        self.cba_map = {v: k for k, v in self.abc_map.items()}
        self.nb_chars = len(self.abc_map)
        print(f"Seq2SeqSplitterDataset: Words in dataset: {self.nb_words}")

    def get_padding_idx(self):
        return self.abc_map[self.PAD_CHAR]

    def check(self, word, decomps):
        if not {c for c in word}.difference(self.ALLOWED_CHARS):
            for decomp in decomps:
                for syll in decomp:
                    if {c for c in syll}.difference(self.ALLOWED_CHARS):
                        return False
        else:
            return False
        return True

    def __getitem__(self, idx):
        # Input
        word = self.words[idx]
        # Convert list of syllables into string; e.g., "format" would have decomp ['for', 'mat']
        # and would become 'for/mat'
        decomp = self.data[word]
        target = ''
        for i in range(len(decomp)-1):
            target += decomp[i]
            target += self.SEP_CHAR
        target += decomp[-1]

        # print(f"{word} --> {target}", flush=True)
        char_seq_in = [self.abc_map[c] for c in word] + [self.abc_map[self.STOP_CHAR]]
        if self.b_pad and len(char_seq_in) < self.max_len_in-1:
            char_seq_in += [self.abc_map[self.PAD_CHAR]] * (self.max_len_in-1 - len(char_seq_in))
        if len(char_seq_in) > self.max_len_in:
            raise ValueError(f"Input sequence too long: [{word}] -- {len(word)} > {self.max_len_in}")

        if self.b_return_one_hot:
            seq_in = np.zeros((len(char_seq_in), self.nb_chars))
            for i, n in enumerate(char_seq_in):
                seq_in[i, :] = Tools.to_one_hot(n, self.nb_chars)

            seq_in = torch.from_numpy(np.asarray(seq_in, dtype=np.float32))
        else:
            seq_in = np.asarray(char_seq_in)
            seq_in = torch.from_numpy(seq_in).long()

        # seq_out = np.zeros((len(target)+1, self.nb_chars))
        # for i, c in enumerate(target):
        #     seq_out[i, :] = Tools.to_one_hot(self.abc_map[c], self.nb_chars)
        # seq_out[-1, :] = Tools.to_one_hot(self.abc_map[self.STOP_CHAR], self.nb_chars)

        seq_out = [self.abc_map[self.START_CHAR]]\
                  + [self.abc_map[c] for c in target]\
                  + [self.abc_map[self.STOP_CHAR]]
        if self.b_pad and len(seq_out) < self.max_len_out:
            seq_out += [self.abc_map[self.PAD_CHAR]] * (self.max_len_out - len(seq_out))
        if len(seq_out) > self.max_len_out:
            trans = ''.join([self.cba_map[c] for c in seq_out])
            raise ValueError(f"Output sequence too long: [{trans}] -- {len(trans)} > {self.max_len_out}")

        seq_out = np.asarray(seq_out)
        seq_out = torch.from_numpy(seq_out).long()

        return seq_in, seq_out

    def word_to_torch(self, word, b_as_one_hot=True, pad_len=0):
        if b_as_one_hot:
            seq_in = np.zeros((len(word)+1, self.nb_chars))
            for i, c in enumerate(word):
                seq_in[i, :] = Tools.to_one_hot(self.abc_map[c], self.nb_chars)
            seq_in[-1, :] = Tools.to_one_hot(self.abc_map[self.STOP_CHAR], self.nb_chars)
            seq_in = torch.from_numpy(np.asarray(seq_in, dtype=np.float32))
        else:
            # seq_in = np.asarray([self.abc_map[c] for c in word] + [self.abc_map[self.STOP_CHAR]])
            seq_in = [self.abc_map[c] for c in word] + [self.abc_map[self.STOP_CHAR]]

            if len(seq_in) < pad_len:
                seq_in += [self.abc_map[self.PAD_CHAR]] * (pad_len - len(seq_in))
            elif pad_len and len(seq_in) > self.max_len_out:
                trans = ''.join([self.cba_map[c] for c in seq_in])
                raise ValueError(f"Input sequence too long: [{trans}] -- {len(trans)} > {pad_len}")

            seq_in = torch.Tensor(seq_in)

        return seq_in

    def idxs_to_word(self, idxs: list):
        res = ''
        for idx in idxs:
            res += self.cba_map[idx]

        return res

    def torch_to_word(self, tensor):
        """

        :param tensor: 2D tensor
        :return:
        """
        res = ''
        for row in tensor:
            res += self.cba_map[torch.argmax(row).item()]

        return res

    def __len__(self):
        return self.nb_words
