import torch


class CustomLearner(torch.nn.Module):
    def __init__(self, embeddings: torch.Tensor):
        super(CustomLearner, self).__init__()
        print(f"Embeddings shape: {embeddings.shape}")
        self.embeddings = torch.nn.Parameter(data=embeddings, requires_grad=True)

    def forward(self, input: torch.Tensor):
        """
        The input should be a NxD tensor, with D the dimension of the embeddings, with 1s in those places that
        correspond to the indices in self.embeddings of the embeddings to sum.
        """
        # Sum embeddings
        res = torch.matmul(input, self.embeddings)
        # print(f"Shape res: {res.shape}")
        res = torch.nn.functional.normalize(res, p=2, dim=1)

        return res
