import math
import os
from collections import defaultdict
from copy import deepcopy

import dill

import numpy as np
import numpy.linalg as la
from sklearn.metrics import mean_squared_error as mse

from pretrained_vecs.conceptnet_vecs import ConceptNetNLVecs
from config import Config
from syllables.nlwiktionary_syllables import NLWiktionarySyllables


def update(target, sum, victim, lr, b_norm=True):
    # print(f"Target: {target}")
    diff_sum = sum - victim
    # print(f"Diffsum: {diff_sum}")

    diff_tar = target - diff_sum
    # print(f"Difftar: {diff_tar}")

    direction = diff_tar - victim
    # print(f"Direction: {direction}")
    victim = victim + lr*direction

    return victim / la.norm(victim) if b_norm else victim


def get_vec():
    rand_vec = np.asarray([np.random.rand() for _ in range(300)])
    return rand_vec / la.norm(rand_vec)


def train_syllables():
    cnnl = ConceptNetNLVecs()
    wikt = NLWiktionarySyllables()

    overlap = sorted(set(cnnl.get_words()).intersection(set(wikt.data.keys())))
    print(len(overlap))

    # syll_vecs = defaultdict(get_vec)
    syll_vecs = dill.load(open(os.path.join(Config.PRETRAINED_VECS_DIR, "syll_vecs.dill"), "rb"))
    at_word = 0
    best_mse = math.inf
    prev_mse = math.inf
    since_best = 0
    best_vecs = deepcopy(syll_vecs)
    for epoch in range(101, 501):
        total_mse = 0
        lr = 0.99 ** epoch
        for i, word in enumerate(overlap):
            if i % 5000 == 0:
                print(f"\rAt epoch {epoch} -- word {i}...", end='', flush=True)
            target_vec = cnnl.get_vec_for_word(word)
            sylls = wikt.get_syllabic_decomposition_for_word(word)[0]

            sum_vec = np.sum([syll_vecs[syll] for syll in sylls], axis=0)

            for syll in sylls:
                syll_vecs[syll] = update(target_vec, sum_vec, syll_vecs[syll], lr, b_norm=False)

            sum_vec = np.sum([syll_vecs[syll] for syll in sylls], axis=0)
            total_mse += mse(target_vec, sum_vec)

        print(f"\rAt epoch {epoch} -- word {len(overlap)}...")
        print(f"MSE: {total_mse}")

        if total_mse < best_mse:
            best_mse = total_mse
            since_best = 0
            best_vecs = deepcopy(syll_vecs)
        else:
            since_best += 1
            if since_best == 1:
                dill.dump(best_vecs, open(os.path.join(Config.PRETRAINED_VECS_DIR, f"syll_vecs_best_{epoch - 1}epochs.dill"), "wb"))
            if since_best >= 5:
                print("Best MSE is at least three epochs behind us... Stopping.")
                break

        if epoch % 50 == 0:
            dill.dump(syll_vecs, open(os.path.join(Config.PRETRAINED_VECS_DIR, f"syll_vecs_{epoch}epochs.dill"), "wb"))


# Prototype
if True:
    bloembak = np.asarray([0.5, 0.5])
    bloembak /= la.norm(bloembak)
    bloemkool = np.asarray([-0.5, 0.5])
    bloemkool /= la.norm(bloemkool)
    koolbak = np.asarray([0.0, -1.0])

    bak = np.asarray([np.random.rand() for _ in range(2)])
    bloem = np.asarray([np.random.rand() for _ in range(2)])
    kool = np.asarray([np.random.rand() for _ in range(2)])
    bak /= la.norm(bak)
    bloem /= la.norm(bloem)
    kool /= la.norm(kool)

    print(f"bloembak : {bloembak}")
    print(f"bloemkool: {bloemkool}")
    print(f"koolbak  : {koolbak}")

    # lr = 0.1
    for epoch in range(1, 100):
        lr = 0.9**epoch
        # bloembak
        t_bloembak = bloem + bak
        bak = update(bloembak, t_bloembak, bak, lr)
        bloem = update(bloembak, t_bloembak, bloem, lr)

        # bloemkool
        t_bloemkool = bloem + kool
        bloem = update(bloemkool, t_bloemkool, bloem, lr)
        kool = update(bloemkool, t_bloemkool, kool, lr)

        # koolbak
        t_koolbak = kool + bak
        kool = update(koolbak, t_koolbak, kool, lr)
        bak = update(koolbak, t_koolbak, bak, lr)

    print(f"bak  : {bak}")
    print(f"bloem: {bloem}")
    print(f"kool : {kool}")
    print()
