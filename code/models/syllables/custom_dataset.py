import numpy as np
import torch

from tools.tools import Tools


class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, idxs, targets, nb_syllables):
        super(CustomDataset, self).__init__()
        self.idxs = idxs
        self.targets = targets
        self.nb_syllables = nb_syllables

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idxs = idx.tolist()
            del idx

            nhot = np.zeros((len(idxs), self.nb_syllables))
            for i, idx in enumerate(idxs):
                nhot[i, :] = Tools.to_n_hot(self.idxs[idx], self.nb_syllables)
            target = self.targets[idxs]

        else:
            nhot = torch.from_numpy(Tools.to_n_hot(self.idxs[idx], self.nb_syllables)).float()
            target = self.targets[idx, :]

        return nhot, target

    def __len__(self):
        return len(self.idxs)
