import torch


class CustomLoss(torch.nn.module):
    def __init__(self, cs: dict):
        """

        Args:
            cs: a dictionary containing the weights for all terms in the loss. By default, these are 1 for the
            diagonal terms (i.e., "class x examples that were predicted as being of class x", and 0 for the
            cross terms (i.e., "class x examples that were predicted as being of class y").
            This means that when no specific weights are provided, this loss reverts to regular Cross Entropy.
        """
        super(CustomLoss, self).__init__()
        # Default weight for diagonal terms = 1
        self.cs = {f'{i}{i}': 1 for i in range(3)}
        # Default weight for cross terms = 0
        self.cs.update({f'{i}{j}': 0 for i in range(3) for j in range(3) if i != j})
        # Update with used provided weights
        self.cs.update(cs)

    def forward(self, input: torch.Tensor, target: torch.Tensor):
        res = target - input

        # # Regular Cross Entropy
        # ce = torch.sum(target * torch.log(input), dim=1)
        # ce = ce * sum([self.cs[f"{i}{i}"] * target[:, i] for i in range(3)])
        #
        # # Cross terms
        # cts = []
        # for i in range(3):
        #     for j in range(3):
        #         if j == i:
        #             continue
        #         weight = self.cs[f"{i}{j}"]
        #         if weight == 0:
        #             continue
        #         cts.append(weight * target[:, i] * torch.log(1 - input[:, j]))
        #
        # res = -torch.mean(ce + sum(cts))

        return res