This folder contains all the files used for creating the Dutch translation of the English SemEval2017 (Task2) dataset.
The steps followed to create the translation are outlined in the paper "Compressing Word Embeddings Using Syllables".

The files are the following:

nl.csv                                            : the final result, in the same format used by the original dataset
SemEval2017_NL_Annotations_[firstname].csv        : the original annotations by [firstname], where [firstname] should be replaced by the firstname of one of the three annotators; Laurent, Matthias and Simon
SemEval2017_NL_Annotations_[firstname]_RECHECK.csv: the updated annotations by [firstname], afther the first pass
SemEval2017_NL_Wordpairs.csv                      : the Dutch wordpairs, without score(s)
Vertaling_EN_NL.ods                               : the translation from English to Dutch
